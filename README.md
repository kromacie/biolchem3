## Biolchem

### Assumptions

- Allow users to solve school exams for biology and chemistry
- Classify questions and exams for difficulty (based on users results)

### Functionalities

- Add exams and singular questions
- Full questions creation based on CMS
- Clasification of questions/exams based on users results
- Realtime (websockets) questions checking

### Todo

- NLP based answers checking (context)

### Status
Abadoned