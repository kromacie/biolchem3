<?php namespace App\Actions;

use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class Create implements ActionInterface
{

    private $data;

    /**
     * CreateValidator constructor.
     * @param $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }


    public function perform(AbstractRepository $repository)
    {
        return $repository->create($this->data);
    }
}