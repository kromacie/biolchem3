<?php namespace App\Actions\Inputs\Rewards;

use App\Scopes\Inputs\Rewards\ScopeInputID;
use App\Scopes\Inputs\Rewards\ScopeRewardID;
use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class Show implements ActionInterface
{
    private $data;

    /**
     * ShowBySlug constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }


    public function perform(AbstractRepository $repository)
    {
        return $repository
            ->scope(new ScopeInputID($this->data['input_id']))
            ->scope(new ScopeRewardID($this->data['reward_id']))
            ->first();
    }
}