<?php namespace App\Actions\Questions;

use App\Scopes\Questions\ScopeDates;
use App\Scopes\Questions\ScopeEnabled;
use App\Scopes\Questions\ScopeSelect;
use App\Scopes\Questions\ScopeSubjectID;
use App\Scopes\Questions\ScopeTags;
use App\Scopes\Questions\ScopeTiers;
use App\Scopes\Questions\ScopeTypes;
use App\Scopes\Questions\ScopeVersions;
use App\Scopes\Questions\ScopeWithResolvedQuestions;
use App\Scopes\Questions\SortByResolvedQuestions;
use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class Paginate implements ActionInterface
{
    private $page = 1;
    private $onPage = 5;
    private $years = [];
    private $months = [];
    private $url = 'page';
    private $categories = [];
    private $tags = [];
    private $subject_id;
    private $enabled;
    private $types = [];
    private $versions = [];
    private $tiers = [];
    private $popularity;
    private $difficulty;


    /**
     * @param mixed $enabled
     * @return Paginate
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }



    /**
     * @param mixed $subject_id
     * @return Paginate
     */
    public function setSubjectId($subject_id)
    {
        $this->subject_id = $subject_id;
        return $this;
    }



    /**
     * @param int $page
     * @return Paginate
     */
    public function setPage(int $page): Paginate
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @param array $years
     * @return Paginate
     */
    public function setYears(array $years): Paginate
    {
        $this->years = $years;
        return $this;
    }

    /**
     * @param array $months
     * @return Paginate
     */
    public function setMonths(array $months): Paginate
    {
        $this->months = $months;
        return $this;
    }

    /**
     * @param array $categories
     * @return Paginate
     */
    public function setCategories(array $categories): Paginate
    {
        $this->categories = $categories;
        return $this;
    }

    /**
     * @param array $tags
     * @return Paginate
     */
    public function setTags(array $tags): Paginate
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * @param array $types
     * @return Paginate
     */
    public function setTypes(array $types): Paginate
    {
        $this->types = $types;
        return $this;
    }

    /**
     * @param array $versions
     * @return Paginate
     */
    public function setVersions(array $versions): Paginate
    {
        $this->versions = $versions;
        return $this;
    }

    /**
     * @param array $tiers
     * @return Paginate
     */
    public function setTiers(array $tiers): Paginate
    {
        $this->tiers = $tiers;
        return $this;
    }

    /**
     * @param mixed $popularity
     * @return Paginate
     */
    public function setPopularity($popularity)
    {
        $this->popularity = $popularity;
        return $this;
    }

    /**
     * @param mixed $difficulty
     * @return Paginate
     */
    public function setDifficulty($difficulty)
    {
        $this->difficulty = $difficulty;
        return $this;
    }

    public function perform(AbstractRepository $repository)
    {
        if($this->tiers) {
            $repository->scope(new ScopeTiers($this->tiers));
        }

        if($this->types) {
            $repository->scope(new ScopeTypes($this->types));
        }

        if($this->versions) {
            $repository->scope(new ScopeVersions($this->versions));
        }

        if($this->years || $this->enabled) {
            $repository->scope(new ScopeDates($this->years, $this->months));
        }

        if($this->enabled !== null) {
            $repository->scope(new ScopeEnabled($this->enabled));
        }

        if($this->subject_id) {
            $repository->scope(new ScopeSubjectID($this->subject_id));
        }

        if($this->categories || $this->tags) {
            $repository->scope(new ScopeTags($this->tags, $this->categories));
        }

        if($this->popularity || $this->difficulty) {
            $repository->scope(new SortByResolvedQuestions($this->difficulty, $this->popularity));
        }

        $repository->scope(new ScopeWithResolvedQuestions());
        $repository->scope(new ScopeSelect());

        return $repository->paginate($this->onPage, ['*'], $this->url, $this->page);
    }
}