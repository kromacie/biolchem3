<?php namespace App\Actions\Questions;

use App\Scopes\Questions\ScopeEnabled;
use App\Scopes\Questions\ScopeQuestion;
use App\Scopes\Questions\ScopeWithResolvedQuestions;
use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class Show implements ActionInterface
{

    private $slug;
    private $enabled;

    /**
     * @param mixed $slug
     * @return Show
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @param mixed $enabled
     * @return Show
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }



    public function perform(AbstractRepository $repository)
    {
        if($this->slug) {
            $repository->scope(new ScopeQuestion($this->slug));
        }

        if($this->enabled !== null) {
            $repository->scope(new ScopeEnabled($this->enabled));
        }

        $repository->scope(new ScopeWithResolvedQuestions());

        return $repository->first();
    }
}