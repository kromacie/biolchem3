<?php namespace App\Actions\Questions;

use App\Scopes\Questions\ScopeSheetID;
use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class ShowAll implements ActionInterface
{

    private $sheet_id;

    /**
     * @param mixed $sheet_id
     * @return ShowAll
     */
    public function setSheetId($sheet_id)
    {
        $this->sheet_id = $sheet_id;
        return $this;
    }



    public function perform(AbstractRepository $repository)
    {
        if($this->sheet_id) {
            $repository->scope(new ScopeSheetID($this->sheet_id));
        }

        return $repository->get();
    }
}