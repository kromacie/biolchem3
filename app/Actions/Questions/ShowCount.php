<?php namespace App\Actions\Questions;

use App\Scopes\Questions\ScopeEnabled;
use App\Scopes\Questions\ScopeSubjectID;
use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class ShowCount implements ActionInterface
{

    private $enabled;

    private $subject_id;

    /**
     * @param mixed $enabled
     * @return ShowCount
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * @param mixed $subject_id
     * @return ShowCount
     */
    public function setSubjectId($subject_id)
    {
        $this->subject_id = $subject_id;
        return $this;
    }



    public function perform(AbstractRepository $repository)
    {
        if($this->enabled !== null) {
            $repository->scope(new ScopeEnabled($this->enabled));
        }

        if($this->subject_id) {
            $repository->scope(new ScopeSubjectID($this->subject_id));
        }

        return $repository->count();
    }
}