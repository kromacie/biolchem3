<?php namespace App\Actions\Questions\Tags;

use App\Scopes\Questions\Tags\ScopeQuestionID;
use App\Scopes\Questions\Tags\ScopeTagID;
use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class Delete implements ActionInterface
{

    private $question_id;
    private $tag_id;

    /**
     * Delete constructor.
     * @param $question_id
     * @param $tag_id
     */
    public function __construct($question_id, $tag_id)
    {
        $this->question_id = $question_id;
        $this->tag_id = $tag_id;
    }


    public function perform(AbstractRepository $repository)
    {
        $repository->scope(new ScopeQuestionID($this->question_id));
        $repository->scope(new ScopeTagID($this->tag_id));
        return $repository->delete();
    }
}