<?php namespace App\Actions\Resolved\Sheets;

use App\Scopes\Resolved\Sheets\ScopeSubjectID;
use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class ShowCount implements ActionInterface
{

    private $subject_id;

    /**
     * @param mixed $subject_id
     * @return ShowCount
     */
    public function setSubjectId($subject_id)
    {
        $this->subject_id = $subject_id;
        return $this;
    }



    public function perform(AbstractRepository $repository)
    {
        if($this->subject_id) {
            $repository->scope(new ScopeSubjectID($this->subject_id));
        }

        return $repository->count();
    }
}