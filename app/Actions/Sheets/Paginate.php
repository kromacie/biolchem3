<?php namespace App\Actions\Sheets;

use App\Scopes\Sheets\ScopeDates;
use App\Scopes\Sheets\ScopeEnabled;
use App\Scopes\Sheets\ScopeSelect;
use App\Scopes\Sheets\ScopeSubjects;
use App\Scopes\Sheets\ScopeTiers;
use App\Scopes\Sheets\ScopeTypes;
use App\Scopes\Sheets\ScopeVersions;
use App\Scopes\Sheets\ScopeWithResolvedQuestions;
use App\Scopes\Sheets\ScopeWithResolvedSheets;
use App\Scopes\Sheets\SortByResolvedQuestions;
use App\Scopes\Sheets\SortByResolvedSheets;
use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class Paginate implements ActionInterface
{

    private $page = 1;
    private $onPage = 5;
    private $years = [];
    private $months = [];
    private $url = 'page';
    private $enabled;
    private $subject;
    private $types;
    private $versions;
    private $tiers;
    private $popularity;
    private $difficulty;

    /**
     * @param mixed $difficulty
     * @return Paginate
     */
    public function setDifficulty($difficulty)
    {
        $this->difficulty = $difficulty;
        return $this;
    }


    /**
     * @param mixed $popularity
     * @return Paginate
     */
    public function setPopularity($popularity)
    {
        $this->popularity = $popularity;
        return $this;
    }

    /**
     * @param array $years
     * @return Paginate
     */
    public function setYears(array $years): Paginate
    {
        $this->years = $years;
        return $this;
    }

    /**
     * @param array $months
     * @return Paginate
     */
    public function setMonths(array $months): Paginate
    {
        $this->months = $months;
        return $this;
    }

    /**
     * @param string $url
     * @return Paginate
     */
    public function setUrl(string $url): Paginate
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @param mixed $enabled
     * @return Paginate
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * @param mixed $subject
     * @return Paginate
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @param mixed $types
     * @return Paginate
     */
    public function setTypes($types)
    {
        $this->types = $types;
        return $this;
    }

    /**
     * @param mixed $versions
     * @return Paginate
     */
    public function setVersions($versions)
    {
        $this->versions = $versions;
        return $this;
    }

    /**
     * @param mixed $tiers
     * @return Paginate
     */
    public function setTiers($tiers)
    {
        $this->tiers = $tiers;
        return $this;
    }

    public function perform(AbstractRepository $repository)
    {

        $repository->scope(new ScopeWithResolvedQuestions());
        $repository->scope(new ScopeWithResolvedSheets());

        if($this->subject) {
            $repository->scope(new ScopeSubjects($this->subject));
        }
        if($this->enabled !== null) {
            $repository->scope(new ScopeEnabled($this->enabled));
        }
        if(is_array($this->versions)) {
            $repository->scope(new ScopeVersions($this->versions));
        }
        if(is_array($this->types)) {
            $repository->scope(new ScopeTypes($this->types));
        }
        if($this->years || $this->months) {
            $repository->scope(new ScopeDates($this->years, $this->months));
        }
        if(is_array($this->tiers)) {
            $repository->scope(new ScopeTiers($this->tiers));
        }
        if($this->popularity == 'asc' || $this->popularity == 'desc') {
            $repository->scope(new SortByResolvedSheets($this->popularity));
        }
        if($this->difficulty == 'asc' || $this->difficulty == 'desc') {
            $repository->scope(new SortByResolvedQuestions($this->difficulty));
        }

        $repository->scope(new ScopeSelect());


        return $repository->paginate($this->onPage, ['*'], $this->url, $this->page);
    }

    /**
     * @param int $page
     * @return Paginate
     */
    public function setPage($page): Paginate
    {
        $this->page = $page;
        return $this;
    }
}