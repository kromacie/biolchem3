<?php namespace App\Actions\Sheets;

use App\Scopes\Sheets\ScopeEnabled;
use App\Scopes\Sheets\ScopeSubject;
use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class ShowAll implements ActionInterface
{

    private $subject;

    private $enabled;

    /**
     * @param mixed $subject
     * @return ShowAll
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @param mixed $enabled
     * @return ShowAll
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    public function perform(AbstractRepository $repository)
    {
        if($this->subject) {
            $repository->scope(new ScopeSubject($this->subject));
        }

        if($this->enabled !== null) {
            $repository->scope(new ScopeEnabled($this->enabled));
        }

        return $repository->get();
    }
}