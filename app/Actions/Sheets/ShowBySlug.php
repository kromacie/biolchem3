<?php namespace App\Actions\Sheets;

use App\Scopes\Sheets\ScopeEnabled;
use App\Scopes\Sheets\ScopeWithGroups;
use App\Scopes\Sheets\ScopeWithInputs;
use App\Scopes\Sheets\ScopeWithQuestions;
use App\Scopes\Sheets\ScopeWithResolvedQuestions;
use App\Scopes\Sheets\ScopeWithResolvedSheets;
use App\Scopes\Sheets\ScopeWithSubstitutes;
use App\Scopes\Universal\ScopeSlug;
use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class ShowBySlug implements ActionInterface
{

    private $slug;

    private $enabled;

    private $withQuestions;

    private $withSubstitutes;

    private $withGroups;

    private $withInputs;

    /**
     * @param mixed $slug
     * @return ShowBySlug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @param mixed $withQuestions
     * @return ShowBySlug
     */
    public function setWithQuestions($withQuestions)
    {
        $this->withQuestions = $withQuestions;
        return $this;
    }

    /**
     * @param mixed $withSubstitutes
     * @return ShowBySlug
     */
    public function setWithSubstitutes($withSubstitutes)
    {
        $this->withSubstitutes = $withSubstitutes;
        return $this;
    }

    /**
     * @param mixed $withGroups
     * @return ShowBySlug
     */
    public function setWithGroups($withGroups)
    {
        $this->withGroups = $withGroups;
        return $this;
    }

    /**
     * @param mixed $withInputs
     * @return ShowBySlug
     */
    public function setWithInputs($withInputs)
    {
        $this->withInputs = $withInputs;
        return $this;
    }


    /**
     * ShowBySlug constructor.
     * @param $slug
     */
    public function __construct($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @param mixed $enabled
     * @return ShowBySlug
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }




    public function perform(AbstractRepository $repository)
    {
        $repository->scope(new ScopeSlug($this->slug));

        if($this->enabled !== null) {
            $repository->scope(new ScopeEnabled($this->enabled));
        }

        if($this->withQuestions) {
            $repository->scope(new ScopeWithQuestions());
        }

        if($this->withSubstitutes) {
            $repository->scope(new ScopeWithSubstitutes());
        }

        if($this->withGroups) {
            $repository->scope(new ScopeWithGroups());
        }

        if($this->withInputs) {
            $repository->scope(new ScopeWithInputs());
        }

        $repository->scope(new ScopeWithResolvedSheets());
        $repository->scope(new ScopeWithResolvedQuestions());

        return $repository->first();
    }
}