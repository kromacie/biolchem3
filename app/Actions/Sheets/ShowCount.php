<?php namespace App\Actions\Sheets;

use App\Scopes\Sheets\ScopeEnabled;
use App\Scopes\Sheets\ScopeSubjects;
use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class ShowCount implements ActionInterface
{

    private $subject;

    private $enabled;

    /**
     * @param mixed $subject
     * @return ShowCount
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @param mixed $enabled
     * @return ShowCount
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }



    public function perform(AbstractRepository $repository)
    {
        if($this->subject) {
            $repository->scope(new ScopeSubjects($this->subject));
        }

        if($this->enabled !== null) {
            $repository->scope(new ScopeEnabled($this->enabled));
        }

        return $repository->count();
    }
}