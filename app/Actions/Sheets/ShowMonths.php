<?php namespace App\Actions\Sheets;

use App\Scopes\Sheets\GroupByMonth;
use App\Scopes\Sheets\MorphToMonth;
use App\Scopes\Sheets\ScopeEnabled;
use App\Scopes\Sheets\ScopeSubjects;
use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class ShowMonths implements ActionInterface
{

    private $subject_id;

    private $enabled;

    /**
     * @param mixed $subject_id
     * @return ShowMonths
     */
    public function setSubjectId($subject_id)
    {
        $this->subject_id = $subject_id;
        return $this;
    }

    /**
     * @param mixed $enabled
     * @return ShowMonths
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    public function perform(AbstractRepository $repository)
    {
        if($this->subject_id) {
            $repository->scope(new ScopeSubjects($this->subject_id));
        }

        if($this->enabled !== null) {
            $repository->scope(new ScopeEnabled($this->enabled));
        }

        return $repository->scope(new MorphToMonth())
                          ->scope(new GroupByMonth())
                          ->get();
    }
}