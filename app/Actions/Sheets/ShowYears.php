<?php namespace App\Actions\Sheets;

use App\Scopes\Sheets\GroupByYear;
use App\Scopes\Sheets\MorphToYear;
use App\Scopes\Sheets\ScopeEnabled;
use App\Scopes\Sheets\ScopeSubjects;
use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class ShowYears implements ActionInterface
{
    private $subject_id;

    private $enabled;

    /**
     * @param mixed $subject_id
     * @return ShowYears
     */
    public function setSubjectId($subject_id)
    {
        $this->subject_id = $subject_id;
        return $this;
    }

    /**
     * @param mixed $enabled
     * @return ShowYears
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    public function perform(AbstractRepository $repository)
    {
        if($this->subject_id !== null) {
            $repository->scope(new ScopeSubjects($this->subject_id));
        }
        if($this->enabled !== null) {
            $repository->scope(new ScopeEnabled($this->enabled));
        }

        return $repository->scope(new GroupByYear())
                          ->scope(new MorphToYear())
                          ->get();
    }
}