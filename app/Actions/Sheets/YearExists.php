<?php namespace App\Actions\Sheets;

use App\Scopes\Sheets\ScopeYear;
use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class YearExists implements ActionInterface
{

    private $year;

    /**
     * YearExists constructor.
     * @param $year
     */
    public function __construct($year)
    {
        $this->year = $year;
    }


    public function perform(AbstractRepository $repository)
    {
        $repository->scope(new ScopeYear($this->year));

        return $repository->exists();
    }
}