<?php namespace App\Actions;

use App\Scopes\Universal\ScopeID;
use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class Show implements ActionInterface
{
    private $id;

    /**
     * ShowBySlug constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }


    public function perform(AbstractRepository $repository)
    {
        return $repository->scope(new ScopeID($this->id))->first();
    }
}