<?php namespace App\Actions;

use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class ShowAll implements ActionInterface
{
    public function perform(AbstractRepository $repository)
    {
        return $repository->get();
    }
}