<?php namespace App\Actions\Subjects;

use App\Scopes\Subjects\ScopeEnabled;
use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class ShowAll implements ActionInterface
{

    private $enabled;

    /**
     * @param mixed $enabled
     * @return ShowAll
     */
    public function setEnabled($enabled): ShowAll
    {
        $this->enabled = $enabled;
        return $this;
    }



    public function perform(AbstractRepository $repository)
    {
        if($this->enabled !== null) {
            $repository->scope(new ScopeEnabled($this->enabled));
        }

        return $repository->get();
    }
}