<?php namespace App\Actions\Subjects;

use App\Scopes\Subjects\ScopeEnabled;
use App\Scopes\Universal\ScopeSlug;
use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class ShowBySlug implements ActionInterface
{

    private $slug;
    private $enabled;

    /**
     * ShowBySlug constructor.
     * @param $slug
     */
    public function __construct($slug)
    {
        $this->slug = $slug;
    }


    /**
     * @param mixed $enabled
     * @return ShowBySlug
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }



    public function perform(AbstractRepository $repository)
    {

        $repository->scope(new ScopeSlug($this->slug));

        if($this->enabled !== null) {
            $repository->scope(new ScopeEnabled($this->enabled));
        }

        return $repository->first();
    }
}