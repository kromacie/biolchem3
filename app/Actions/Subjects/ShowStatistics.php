<?php namespace App\Actions\Subjects;

use App\Scopes\Subjects\GroupBySheetYears;
use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class ShowStatistics implements ActionInterface
{

    private $enabled = null;

    /**
     * @param mixed $enabled
     * @return ShowStatistics
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }



    public function perform(AbstractRepository $repository)
    {
        $repository->scope(new GroupBySheetYears());

        return $repository->get();
    }
}