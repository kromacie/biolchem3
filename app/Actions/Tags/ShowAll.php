<?php namespace App\Actions\Tags;

use App\Scopes\Tags\ScopeExceptID;
use App\Scopes\Tags\ScopeHasTag;
use App\Scopes\Tags\ScopeHasTags;
use App\Scopes\Tags\ScopeDoesntHaveQuestion;
use App\Scopes\Tags\ScopeSubjectID;
use App\Scopes\Tags\ScopeWithoutTag;
use App\Scopes\Tags\ScopeWithoutTags;
use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class ShowAll implements ActionInterface
{

    private $without_tag = false;

    private $except_id = null;

    private $has_tags = false;

    private $has_tag = null;

    private $where_subject = null;

    private $doesnt_have_question = null;

    private $without_tags = null;

    /**
     * @param int $has_tag
     * @return ShowAll
     */
    public function setHasTag($has_tag)
    {
        $this->has_tag = $has_tag;
        return $this;
    }



    /**
     * @param bool $has_tags
     * @return ShowAll
     */
    public function setHasTags($has_tags): ShowAll
    {
        $this->has_tags = $has_tags;
        return $this;
    }


    /**
     * @param bool $without_tags
     * @return ShowAll
     */
    public function setWithoutTag($without_tags): ShowAll
    {
        $this->without_tag = $without_tags;
        return $this;
    }

    /**
     * @param int[] $except_id
     * @return ShowAll
     */
    public function setExcept(array $except_id): ShowAll
    {
        $this->except_id = $except_id;
        return $this;
    }

    /**
     * @param $question_id
     * @return ShowAll
     */
    public function setDoesntHaveQuestion($question_id)
    {
        $this->doesnt_have_question = $question_id;
        return $this;
    }

    /**
     * @param $without_tags
     * @return ShowAll
     */
    public function setWithoutTags($without_tags)
    {
        $this->without_tags = $without_tags;
        return $this;
    }

    public function perform(AbstractRepository $repository)
    {
        if($this->without_tag) {
            $repository->scope(new ScopeWithoutTag());
        }
        if($this->without_tags) {
            $repository->scope(new ScopeWithoutTags());
        }
        if($this->except_id) {
            $repository->scope(new ScopeExceptID($this->except_id));
        }
        if($this->has_tags) {
            $repository->scope(new ScopeHasTags());
        }
        if($this->has_tag) {
            $repository->scope(new ScopeHasTag($this->has_tag));
        }
        if($this->doesnt_have_question) {
            $repository->scope(new ScopeDoesntHaveQuestion($this->doesnt_have_question));
        }
        if($this->where_subject) {
            $repository->scope(new ScopeSubjectID($this->where_subject));
        }

        return $repository->get();
    }

    /**
     * @param null $where_subject
     * @return ShowAll
     */
    public function setWhereSubject($where_subject): ShowAll
    {
        $this->where_subject = $where_subject;

        return $this;
    }
}