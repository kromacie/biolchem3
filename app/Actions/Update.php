<?php namespace App\Actions;

use App\Scopes\Universal\ScopeID;
use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class Update implements ActionInterface
{
    private $id;
    private $data;

    /**
     * Update constructor.
     * @param $id
     * @param array $data
     */
    public function __construct($id, array $data)
    {
        $this->id = $id;
        $this->data = $data;
    }


    public function perform(AbstractRepository $repository)
    {
        return $repository->scope(new ScopeID($this->id))
            ->update($this->data);
    }
}