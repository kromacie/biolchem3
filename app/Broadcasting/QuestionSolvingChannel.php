<?php

namespace App\Broadcasting;

use App\Services\QuestionService;
use Illuminate\Contracts\Auth\Authenticatable;

class QuestionSolvingChannel
{

    private $questions;

    /**
     * Create a new channel instance.
     *
     * @param QuestionService $service
     */
    public function __construct(QuestionService $service)
    {
        $this->questions = $service;
    }

    /**
     * Authenticate the user's access to the channel.
     *
     * @param Authenticatable $user
     * @param $question
     * @param $uuid
     * @return bool
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function join(Authenticatable $user, $question, $uuid)
    {
        return $this->questions->adminShow($question) == true && $user->getAuthIdentifier() == $uuid;
    }
}
