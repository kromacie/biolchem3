<?php

namespace App\Broadcasting;

use App\Services\SheetService;
use Illuminate\Contracts\Auth\Authenticatable;

class SheetSolvingChannel
{

    private $sheets;

    /**
     * Create a new channel instance.
     *
     * @param SheetService $service
     */
    public function __construct(SheetService $service)
    {
        $this->sheets = $service;
    }

    /**
     * Authenticate the user's access to the channel.
     *
     * @param Authenticatable $user
     * @param $sheet
     * @param $uuid
     * @return bool
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function join(Authenticatable $user, $sheet, $uuid)
    {
        return $this->sheets->adminShow($sheet) == true && $user->getAuthIdentifier() == $uuid;
    }
}
