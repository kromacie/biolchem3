<?php

namespace App\Broadcasting;

use App\Services\SheetService;
use Illuminate\Contracts\Auth\Authenticatable;

class SheetYearChannel
{

    private $sheets;

    /**
     * Create a new channel instance.
     *
     * @param SheetService $service
     */
    public function __construct(SheetService $service)
    {
        $this->sheets = $service;
    }

    /**
     * Authenticate the user's access to the channel.
     *
     * @param Authenticatable $user
     * @param $year
     * @return Authenticatable
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function join(Authenticatable $user, $year)
    {
        if($this->sheets->yearExists($year)) {
            return $user;
        }
        return null;
    }
}
