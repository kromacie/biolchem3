<?php

namespace App\Broadcasting;

use App\Services\SubjectService;
use Illuminate\Contracts\Auth\Authenticatable;

class SubjectSheetChannel
{

    private $subjects;

    /**
     * Create a new channel instance.
     *
     * @param SubjectService $subjects
     */
    public function __construct(SubjectService $subjects)
    {
        $this->subjects = $subjects;
    }

    /**
     * Authenticate the user's access to the channel.
     *
     * @param Authenticatable $user
     * @param $subject
     * @return Authenticatable|null
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function join(Authenticatable $user, $subject)
    {
        if($this->subjects->show($subject, true)) {
            return $user;
        }
        return null;
    }
}
