<?php

namespace App\Console\Commands;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Console\Command;

class TestPerformance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:performance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * CreateValidator a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $time = microtime(true);

        $curl = curl_init('http://localhost:3000/');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        for ($i = 0; $i <= 1000; $i++) {
            curl_exec($curl);
        }

        curl_close($curl);

        $this->info('1000 requests in: ' . (microtime(true) - $time) . 's');
    }
}
