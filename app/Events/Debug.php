<?php

namespace App\Events;

use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\InteractsWithSockets;

class Debug implements ShouldBroadcastNow
{
    use InteractsWithSockets, SerializesModels;

    public $data = [];
    private $sheet_id;
    private $uuid;

    /**
     * Create a new event instance.
     *
     * @param $sheet_id
     * @param $uuid
     * @param $data
     */
    public function __construct($sheet_id, $uuid, $data)
    {
        $this->sheet_id = $sheet_id;
        $this->uuid = $uuid;
        $this->data = $data;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("sheet.{$this->sheet_id}.solving.{$this->uuid}");
    }


}
