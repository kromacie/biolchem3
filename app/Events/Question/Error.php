<?php

namespace App\Events\Question;

use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class Error implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $uuid;
    private $question_id;

    /**
     * SheetCheckingError constructor.
     * @param $uuid
     * @param $sheet_id
     */
    public function __construct($uuid, $sheet_id)
    {
        $this->uuid = $uuid;
        $this->question_id = $sheet_id;
    }


    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("question.{$this->question_id}.solving.{$this->uuid}");
    }
}
