<?php

namespace App\Events\Question;

use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\InteractsWithSockets;

class Finish implements ShouldBroadcastNow
{

    use InteractsWithSockets, SerializesModels;

    private $uuid;
    private $question_id;
    public $data;

    /**
     * Create a new event instance.
     *
     * @param $question_id
     * @param $uuid
     * @param $question
     * @param $answers
     * @param $result
     */
    public function __construct($question_id, $uuid, $question, $answers, $result)
    {
        $this->uuid = $uuid;
        $this->question_id = $question_id;
        $this->data['question'] = $question;
        $this->data['answers'] = $answers;
        $this->data['result'] = $result;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("question.{$this->question_id}.solving.{$this->uuid}");
    }

}
