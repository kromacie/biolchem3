<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class QuestionHasBeenChecked implements ShouldBroadcastNow
{
    use InteractsWithSockets, SerializesModels;

    private $sheet_id;
    private $uuid;

    public $data = [];

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($sheet_id, $uuid, $data)
    {
        $this->sheet_id = $sheet_id;
        $this->uuid = $uuid;
        $this->data = $data;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("sheet.{$this->sheet_id}.solving.{$this->uuid}");
    }


}
