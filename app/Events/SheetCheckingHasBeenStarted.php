<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SheetCheckingHasBeenStarted implements ShouldBroadcastNow
{

    use InteractsWithSockets, SerializesModels;

    private $uuid;
    private $sheet_id;

    /**
     * Create a new event instance.
     *
     * @param $uuid
     * @param $sheet_id
     */
    public function __construct($sheet_id, $uuid)
    {
        $this->uuid = $uuid;
        $this->sheet_id = $sheet_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("sheet.{$this->sheet_id}.solving.{$this->uuid}");
    }

}
