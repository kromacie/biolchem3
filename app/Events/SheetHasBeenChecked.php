<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SheetHasBeenChecked implements ShouldBroadcastNow
{

    use InteractsWithSockets, SerializesModels;

    private $uuid;
    private $sheet_id;
    public $data;

    /**
     * Create a new event instance.
     *
     * @param $sheet_id
     * @param $uuid
     * @param $sheet
     * @param $answers
     * @param $result
     */
    public function __construct($sheet_id, $uuid, $sheet, $answers, $result)
    {
        $this->uuid = $uuid;
        $this->sheet_id = $sheet_id;
        $this->data['sheet'] = $sheet;
        $this->data['answers'] = $answers;
        $this->data['result'] = $result;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("sheet.{$this->sheet_id}.solving.{$this->uuid}");
    }

}
