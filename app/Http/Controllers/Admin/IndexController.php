<?php namespace App\Http\Controllers\Admin;

use App\Events\NotifyUsers;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory;
use Illuminate\Validation\Validator;

class IndexController extends Controller
{
    public function index()
    {
        broadcast(new NotifyUsers("Hello everybody"));
        return view('admin.index');
    }

    public function hello()
    {
        return response("Hello World!");
    }
}