<?php

namespace App\Http\Controllers\Admin;

use App\Services\InputGroupTypeService;
use App\Services\InputTypeService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InputTypeController extends Controller
{

    private $types;

    /**
     * InputTypeController constructor.
     * @param $types
     */
    public function __construct(InputTypeService $types)
    {
        $this->types = $types;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function index()
    {
        return view('admin.inputs.index', [
            'types' => $this->types->adminAll()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.inputs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        $this->types->adminCreate($request->all());

        return redirect()->route('inputs.types.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function edit($id)
    {
        return view('admin.inputs.edit', [
            'type' => $this->types->adminShow($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function update(Request $request, $id)
    {
        $this->types->adminUpdate($id, $request->all());

        return redirect()->route('inputs.types.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function destroy($id)
    {
        $this->types->adminDelete($id);

        return redirect()->route('inputs.types.index');
    }
}
