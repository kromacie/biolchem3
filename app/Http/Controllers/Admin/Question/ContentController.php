<?php

namespace App\Http\Controllers\Admin\Question;

use App\Services\ContentService;
use App\Services\QuestionContentService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class ContentController extends Controller
{

    private $contents;

    private $question_contents;

    /**
     * ContentController constructor.
     * @param ContentService $contents
     * @param QuestionContentService $question_contents
     */
    public function __construct(ContentService $contents, QuestionContentService $question_contents)
    {
        $this->contents = $contents;
        $this->question_contents = $question_contents;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * ShowBySlug the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $sheet
     * @param $question
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store($sheet, $question, Request $request)
    {
        DB::beginTransaction();

        $this->question_contents->adminCreate($question, $request->all());

        try {
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            if($e instanceof ValidationException) {
                throw $e;
            }
        }

        return redirect()->intended(route('sheets.questions.editor.index', [
            $sheet, $question
        ]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * ShowBySlug the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $sheet
     * @param $question
     * @param $content
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $sheet, $question, $content)
    {
        $this->question_contents->adminUpdate($content, $request->all());

        return redirect()->route('sheets.questions.editor.index', [$sheet, $question]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $sheet
     * @param $question
     * @param $content
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function destroy($sheet, $question, $content)
    {
        $this->contents->adminDelete($content);

        return redirect()->intended(route('sheets.questions.editor.index', [$sheet, $question]));
    }
}
