<?php

namespace App\Http\Controllers\Admin\Question\Substitute;

use App\Services\QuestionSubstituteContentService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContentController extends Controller
{

    private $question_substitute_content;

    /**
     * ContentController constructor.
     * @param $question_substitute_content
     */
    public function __construct(QuestionSubstituteContentService $question_substitute_content)
    {
        $this->question_substitute_content = $question_substitute_content;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * ShowBySlug the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $sheet
     * @param $question
     * @param $substitute
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, $sheet, $question, $substitute)
    {
        $this->question_substitute_content->adminCreate($substitute, $request->all());

        return redirect()->route('sheets.questions.editor.index', [$sheet, $question]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * ShowBySlug the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $sheet
     * @param $question
     * @param $substitute
     * @param $content
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $sheet, $question, $substitute, $content)
    {
        $this->question_substitute_content->adminUpdate($content, $request->all());

        return redirect()->route('sheets.questions.editor.index', [$sheet, $question]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $sheet
     * @param $question
     * @param $substitute
     * @param $content
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function destroy($sheet, $question, $substitute, $content)
    {
        $this->question_substitute_content->adminDelete($content);

        return redirect()->route('sheets.questions.editor.index', [$sheet, $question]);
    }
}
