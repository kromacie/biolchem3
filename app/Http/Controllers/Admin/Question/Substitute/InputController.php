<?php

namespace App\Http\Controllers\Admin\Question\Substitute;

use App\Services\InputService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InputController extends Controller
{

    private $inputs;

    /**
     * InputController constructor.
     * @param $inputs
     */
    public function __construct(InputService $inputs)
    {
        $this->inputs = $inputs;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * ShowBySlug the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $sheet
     * @param $question
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, $sheet, $question)
    {
        $this->inputs->adminCreate($request->all());

        return redirect()->route('sheets.questions.editor.index', [$sheet, $question]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * ShowBySlug the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $sheet
     * @param $question
     * @param $substitute
     * @param $input
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $sheet, $question, $substitute, $input)
    {
        $this->inputs->adminUpdate($input, $request->all());

        return redirect()->route('sheets.questions.editor.index', [$sheet, $question]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $sheet
     * @param $question
     * @param $substitute
     * @param $input
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function destroy($sheet, $question, $substitute, $input)
    {
        $this->inputs->adminDelete($input);

        return redirect()->route('sheets.questions.editor.index', [$sheet, $question]);
    }
}
