<?php

namespace App\Http\Controllers\Admin\Question\Substitute;

use App\Services\InputCorrectService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InputCorrectController extends Controller
{

    private $corrects;

    /**
     * InputCorrectController constructor.
     * @param $corrects
     */
    public function __construct(InputCorrectService $corrects)
    {
        $this->corrects = $corrects;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * ShowBySlug the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $sheet
     * @param $question
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, $sheet, $question)
    {
        $this->corrects->adminCreate($request->all());

        return redirect()->route('sheets.questions.editor.index', [$sheet, $question]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * ShowBySlug the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $sheet
     * @param $question
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function destroy(Request $request, $sheet, $question)
    {
        $this->corrects->adminDelete($request->route('correct'));

        return redirect()->route('sheets.questions.editor.index', [$sheet, $question]);
    }
}
