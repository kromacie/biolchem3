<?php

namespace App\Http\Controllers\Admin\Question\Substitute;

use App\Services\QuestionSubstituteInputGroupService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InputGroupController extends Controller
{

    private $inputGroups;

    /**
     * InputGroupController constructor.
     * @param $inputGroups
     */
    public function __construct(QuestionSubstituteInputGroupService $inputGroups)
    {
        $this->inputGroups = $inputGroups;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * ShowBySlug the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $sheet
     * @param $question
     * @param $substitute
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, $sheet, $question, $substitute)
    {
        $this->inputGroups->adminCreate($substitute, $request->all());

        return redirect()->route('sheets.questions.editor.index', [$sheet, $question]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * ShowBySlug the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $sheet
     * @param $question
     * @param $substitute
     * @param $group
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $sheet, $question, $substitute, $group)
    {
        $this->inputGroups->adminUpdate($group, $request->all());

        return redirect()->route('sheets.questions.editor.index', [$sheet, $question]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $sheet
     * @param $question
     * @param $substitute
     * @param $group
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function destroy($sheet, $question, $substitute, $group)
    {
        $this->inputGroups->adminDelete($group);

        return redirect()->route('sheets.questions.editor.index', [$sheet, $question]);
    }
}
