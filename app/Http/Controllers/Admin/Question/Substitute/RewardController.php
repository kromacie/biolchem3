<?php

namespace App\Http\Controllers\Admin\Question\Substitute;

use App\Services\RewardService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RewardController extends Controller
{

    private $rewards;

    /**
     * RewardController constructor.
     * @param $rewards
     */
    public function __construct(RewardService $rewards)
    {
        $this->rewards = $rewards;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * ShowBySlug the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $sheet
     * @param $question
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, $sheet, $question)
    {
        $this->rewards->adminCreate($request->all());

        return redirect()->route('sheets.questions.editor.index', [$sheet, $question]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * ShowBySlug the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $sheet
     * @param $question
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function destroy(Request $request, $sheet, $question)
    {
        $this->rewards->adminDelete($request->route('reward'));

        return redirect()->route('sheets.questions.editor.index', [$sheet, $question]);
    }
}
