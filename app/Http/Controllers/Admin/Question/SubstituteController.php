<?php

namespace App\Http\Controllers\Admin\Question;

use App\Services\QuestionSubstituteService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubstituteController extends Controller
{

    private $substitutes;

    /**
     * SubstituteController constructor.
     * @param $substitutes
     */
    public function __construct(QuestionSubstituteService $substitutes)
    {
        $this->substitutes = $substitutes;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * ShowBySlug the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $sheet
     * @param $question
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store($sheet, $question, Request $request)
    {
        $this->substitutes->adminCreate($question, $request->all());

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * ShowBySlug the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $sheet
     * @param $question
     * @param $substitute
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function destroy($sheet, $question, $substitute)
    {
        $this->substitutes->adminDelete($substitute);

        return redirect()->route('sheets.questions.editor.index', [$sheet, $question]);
    }
}
