<?php

namespace App\Http\Controllers\Admin\Question;

use App\Services\QuestionTagService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TagController extends Controller
{

    private $tags;

    /**
     * TagController constructor.
     * @param $tags
     */
    public function __construct(QuestionTagService $tags)
    {
        $this->tags = $tags;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * ShowBySlug the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $sheet
     * @param $question
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $sheet, $question)
    {
        $this->tags->adminCreate($request->all());

        return redirect()->route('sheets.questions.editor.index', [$sheet, $question]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * ShowBySlug the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $sheet
     * @param $question
     * @param $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy($sheet, $question, $tag)
    {
        $this->tags->adminDelete($question, $tag);

        return redirect()->route('sheets.questions.editor.index', [$sheet, $question]);
    }
}
