<?php

namespace App\Http\Controllers\Admin;

use App\Services\QuestionService;
use App\Services\SheetService;
use App\Services\SubjectService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuestionController extends Controller
{

    private $questions;
    private $subjects;
    private $sheets;

    /**
     * QuestionController constructor.
     * @param QuestionService $questions
     * @param SubjectService $subjects
     * @param SheetService $sheets
     */
    public function __construct(QuestionService $questions, SubjectService $subjects, SheetService $sheets)
    {
        $this->questions = $questions;
        $this->subjects = $subjects;
        $this->sheets = $sheets;
    }


    /**
     * Display a listing of the resource.
     *
     * @param $sheet
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function index($sheet)
    {
        return view('admin.questions.index', [
            'questions' => $this->questions->adminAllBySheet($sheet),
            'sheet' => $this->sheets->adminShow($sheet)
        ]);
    }

    /**
     * ShowBySlug the form for creating a new resource.
     *
     * @param $sheet
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function create($sheet)
    {
        return view('admin.questions.create', [
            'subjects' => $this->subjects->adminAll(),
            'sheet' => $this->sheets->adminShow($sheet)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $sheet
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, $sheet)
    {
        $this->questions->adminCreate($request->all());

        return redirect()->route('sheets.questions.index', $sheet);
    }

    /**
     * Display the specified resource.
     *
     * @param $sheet
     * @param $question
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function show($sheet, $question)
    {
        return view('admin.questions.show', [
            'question' => $this->questions->adminShow($question),
            'sheet' => $this->questions->adminShow($sheet)
        ]);
    }

    /**
     * ShowBySlug the form for editing the specified resource.
     *
     * @param $sheet
     * @param $question
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function edit($sheet, $question)
    {
        return view('admin.questions.edit', [
            'question' => $this->questions->adminShow($question),
            'sheet' => $this->sheets->adminShow($sheet)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $question
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $question)
    {
        $this->questions->adminUpdate($question, $request->all());

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $sheet
     * @param $question
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function destroy($sheet, $question)
    {
        $this->questions->adminDelete($question);

        return redirect()->route('sheets.questions.index', $sheet);
    }
}
