<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\InputType;
use App\Services\InputGroupTypeService;
use App\Services\InputTypeService;
use App\Services\QuestionService;
use App\Services\SheetService;
use App\Services\TagService;

class QuestionEditorController extends Controller
{

    private $questions;
    private $sheets;
    private $tags;
    private $input_types;
    private $group_types;

    /**
     * QuestionEditorController constructor.
     * @param SheetService $sheets
     * @param QuestionService $questions
     * @param TagService $tags
     * @param InputGroupTypeService $group_types
     * @param InputTypeService $input_types
     */
    public function __construct(SheetService $sheets, QuestionService $questions, TagService $tags, InputGroupTypeService $group_types, InputTypeService $input_types)
    {
        $this->questions = $questions;
        $this->sheets = $sheets;
        $this->tags = $tags;
        $this->group_types = $group_types;
        $this->input_types = $input_types;
    }


    /**
     * @param $sheet
     * @param $question
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function index($sheet, $question)
    {

        $sheet = $this->sheets->adminShow($sheet);

        return view('admin.questions.editor', [
            'question' => $this->questions->adminShow($question),
            'tags' => $this->tags->adminAllDoesntHaveQuestionAndTagsForSubject($question, $sheet->id),
            'sheet' => $sheet,
            'input_types' => $this->input_types->adminAll(),
            'group_types' => $this->group_types->adminAll()
        ]);
    }
}