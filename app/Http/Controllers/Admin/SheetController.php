<?php

namespace App\Http\Controllers\Admin;

use App\Models\SheetVersion;
use App\Services\SheetService;
use App\Services\SheetTierService;
use App\Services\SheetTypeService;
use App\Services\SheetVersionService;
use App\Services\SubjectService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SheetController extends Controller
{

    private $sheets;
    private $subjects;
    private $versions;
    private $tiers;
    private $types;

    /**
     * SheetController constructor.
     * @param SheetService $sheets
     * @param SubjectService $subjects
     * @param SheetVersionService $versions
     * @param SheetTierService $tiers
     * @param SheetTypeService $types
     */
    public function __construct(SheetService $sheets, SubjectService $subjects, SheetVersionService $versions, SheetTierService $tiers, SheetTypeService $types)
    {
        $this->subjects = $subjects;
        $this->sheets = $sheets;
        $this->versions = $versions;
        $this->tiers = $tiers;
        $this->types = $types;
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $sheets = $this->sheets->adminAll($request->all());

        if($request->ajax()) {
            return response()->json($sheets);
        }

        return view('admin.sheets.index', [
            'sheets' => $sheets,
        ]);
    }

    /**
     * ShowBySlug the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function create()
    {
        return view('admin.sheets.create', [
            'subjects' => $this->subjects->adminAll(),
            'versions' => $this->versions->adminAll(),
            'tiers' => $this->tiers->adminAll(),
            'types' => $this->types->adminAll(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->sheets->adminCreate($request->all());

        return redirect()->route('sheets.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * ShowBySlug the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function edit($id)
    {
        return view('admin.sheets.edit', [
            'sheet' => $this->sheets->adminShow($id),
            'subjects' => $this->subjects->adminAll(),
            'versions' => $this->versions->adminAll(),
            'tiers' => $this->tiers->adminAll(),
            'types' => $this->types->adminAll()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function update(Request $request, $id)
    {
        $this->sheets->adminUpdate($id, $request->all());

        return redirect()->route('sheets.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function destroy($id)
    {
        $this->sheets->adminDelete($id);

        return redirect()->route('sheets.index');
    }
}
