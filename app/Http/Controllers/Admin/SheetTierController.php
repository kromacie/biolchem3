<?php

namespace App\Http\Controllers\Admin;

use App\Services\SheetTierService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SheetTierController extends Controller
{

    private $tiers;

    /**
     * SheetTierController constructor.
     * @param $tiers
     */
    public function __construct(SheetTierService $tiers)
    {
        $this->tiers = $tiers;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function index()
    {
        return view('admin.tiers.index', [
            'tiers' => $this->tiers->adminAll()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tiers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->tiers->adminCreate($request->all());

        return redirect()->route('tiers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function edit($id)
    {
        return view('admin.tiers.edit', [
            'tier' => $this->tiers->adminShow($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function update(Request $request, $id)
    {
        $this->tiers->adminUpdate($id, $request->all());

        return redirect()->route('tiers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function destroy($id)
    {
        $this->tiers->adminDelete($id);

        return redirect()->route('tiers.index');
    }
}
