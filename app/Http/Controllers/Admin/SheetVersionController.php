<?php

namespace App\Http\Controllers\Admin;

use App\Services\SheetVersionService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SheetVersionController extends Controller
{

    private $types;

    /**
     * SheetVersionController constructor.
     * @param $types
     */
    public function __construct(SheetVersionService $types)
    {
        $this->types = $types;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function index()
    {
        return view('admin.versions.index', [
            'versions' => $this->types->adminAll()
        ]);
    }

    /**
     * ShowBySlug the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.versions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->types->adminCreate($request->all());

        return redirect()->route('versions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * ShowBySlug the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function edit($id)
    {
        return view('admin.versions.edit', [
            'version' => $this->types->adminShow($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function update(Request $request, $id)
    {
        $this->types->adminUpdate($id, $request->all());

        return redirect()->route('versions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function destroy($id)
    {
        $this->types->adminDelete($id);

        return redirect()->route('versions.index');
    }
}
