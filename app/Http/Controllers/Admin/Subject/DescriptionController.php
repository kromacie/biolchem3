<?php

namespace App\Http\Controllers\Admin\Subject;

use App\Services\SubjectDescriptionService;
use App\Services\SubjectService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DescriptionController extends Controller
{
    private $subjects;
    private $descriptions;

    /**
     * DescriptionController constructor.
     * @param SubjectService $subjects
     * @param SubjectDescriptionService $descriptions
     */
    public function __construct(SubjectService $subjects, SubjectDescriptionService $descriptions)
    {
        $this->subjects = $subjects;
        $this->descriptions = $descriptions;
    }


    /**
     * Display a listing of the resource.
     *
     * @param $subject
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function index($subject)
    {
        $subject = $this->subjects->adminShow($subject);

        return view('admin.subjects.descriptions.index', [
            'subject' => $subject,
            'description' => $subject->description
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $subject
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function create($subject)
    {
        return view('admin.subjects.descriptions.create', [
            'subject' => $this->subjects->adminShow($subject)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $subject
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(Request $request, $subject)
    {
        $this->descriptions->adminCreate($request->all());

        return redirect()->route('subjects.descriptions.index', $subject);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $subject
     * @param $description
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function update(Request $request, $subject, $description)
    {
        $this->descriptions->adminUpdate($description, $request->all());

        return redirect()->route('subjects.descriptions.index', $subject);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
