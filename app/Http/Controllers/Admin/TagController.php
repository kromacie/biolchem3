<?php

namespace App\Http\Controllers\Admin;

use App\Services\SubjectService;
use App\Services\TagService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;

class TagController extends Controller
{
    private $tags;
    private $subjects;

    /**
     * TagController constructor.
     * @param TagService $tags
     * @param SubjectService $subjects
     */
    public function __construct(TagService $tags, SubjectService $subjects)
    {
        $this->tags = $tags;
        $this->subjects = $subjects;
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function index(Request $request)
    {
        $tags = $this->tags->adminSearchTagTags(
            $request->get('tag_id'),
            $request->get('subject_id'),
            $request->get('without_tags')
        );

        if($request->ajax()) {
            return $tags;
        }

        return view('admin.tags.index', [
            'tags' => $tags,
            'categories' => $this->tags->adminAllHasTags(),
            'subjects' => $this->subjects->adminAll(),
            'subject_id' => $request->get('subject_id'),
            'tag_id' => $request->get('tag_id'),
        ]);
    }

    /**
     * ShowBySlug the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function create()
    {
        return view('admin.tags.create', [
            'subjects' => $this->subjects->adminAll()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->tags->adminCreate($request->all());

        return redirect()->route('tags.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * ShowBySlug the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function edit($id)
    {
        return view('admin.tags.edit', [
            'tag' => $this->tags->adminShow($id),
            'subjects' => $this->subjects->adminAll(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->tags->adminUpdate($id, $request->all());

        return redirect()->route('tags.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function destroy($id)
    {
        $this->tags->adminDelete($id);

        return redirect()->route('tags.index');
    }
}
