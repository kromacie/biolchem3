<?php namespace App\Http\Controllers;

use App\Services\SubjectService;

class IndexController extends Controller
{

    private $subjects;

    /**
     * IndexController constructor.
     * @param $subjects
     */
    public function __construct(SubjectService $subjects)
    {
        $this->subjects = $subjects;
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function index()
    {
        return view('user.index', [
            'subjects' => $this->subjects->getStatistics()
        ]);
    }
}