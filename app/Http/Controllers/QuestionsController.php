<?php

namespace App\Http\Controllers;

use App\Models\Subject;
use App\Services\QuestionService;
use App\Services\SheetService;
use App\Services\SheetTierService;
use App\Services\SheetTypeService;
use App\Services\SheetVersionService;
use App\Services\TagService;
use App\Validators\Admin\Question\CheckValidator;
use Illuminate\Http\Request;

class QuestionsController extends Controller
{

    private $sheets;
    private $versions;
    private $types;
    private $tiers;
    private $tags;
    private $questions;

    public function __construct(SheetService $sheets, SheetVersionService $versions, SheetTypeService $types, SheetTierService $tiers, TagService $tags, QuestionService $questions)
    {
        $this->questions = $questions;
        $this->versions = $versions;
        $this->sheets = $sheets;
        $this->types = $types;
        $this->tiers = $tiers;
        $this->tags = $tags;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function index(Request $request)
    {
        $subject = $this->subject();
        \DB::enableQueryLog();
        return view('user.questions', [
            'subject' => $this->subject(),
            'years' => $this->sheets->showYears($subject->id),
            'versions' => $this->versions->all(),
            'types' => $this->types->all(),
            'tiers' => $this->tiers->all(),
            'tags' => $this->tags->getAllTagsLevelTop($subject->id),
            'months' => $this->sheets->showMonths($subject->id),
            'questions' => ($this->questions->paginate(
                $request->input('page', 1),
                $subject->id,
                true,
                $request->input('months', []),
                $request->input('years', []),
                $request->input('types', []),
                $request->input('versions', []),
                $request->input('tiers', []),
                $request->input('popularity'),
                $request->input('difficulty'),
                $request->input('categories', []),
                $request->input('tags', [])
            ))->appends($request->except('page')),
            'c_months' => array_flip($request->input('months', [])),
            'c_years' => array_flip($request->input('years', [])),
            'c_types' => array_flip($request->input('types', [])),
            'c_versions' => array_flip($request->input('versions', [])),
            'c_tiers' => array_flip($request->input('tiers', [])),
            'c_popularity' => $request->input('popularity'),
            'c_difficulty' => $request->input('difficulty'),
            'c_categories' => $request->input('categories', []),
            'c_tags' => array_flip($request->input('tags', [])),


        ]);
    }

    /**
     * @param Request $request
     * @param $subject
     * @param $question
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function show(Request $request, $subject, $question)
    {
        return view('user.question', [
            'question' => $this->questions->show($question, true),
            'subject' => $this->subject(),
            'uuid' => $this->uuid()
        ]);
    }

    /**
     * @param Request $request
     * @param $subject
     * @param $question
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function check(Request $request, $subject, $question)
    {
        $this->questions->startChecking(
            $question,
            auth('guest')->user()->getAuthIdentifier(),
            $request->input('answer')
        );

        return response($request->input('answer'));
    }

    public function uuid()
    {
        return \Auth::guard('guest')->user()->getAuthIdentifier();
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function subject(): Subject
    {
        return app()->make(Subject::class);
    }

}
