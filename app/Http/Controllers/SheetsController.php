<?php namespace App\Http\Controllers;

use App\Events\SheetCheckingHasBeenStarted;
use App\Jobs\CheckSheet;
use App\Models\Subject;
use App\Services\SheetService;
use App\Services\SheetTierService;
use App\Services\SheetTypeService;
use App\Services\SheetVersionService;
use DB;
use Illuminate\Http\Request;

class SheetsController extends Controller
{

    private $sheets;
    private $versions;
    private $types;
    private $tiers;

    public function __construct(SheetService $sheets, SheetVersionService $versions, SheetTypeService $types, SheetTierService $tiers)
    {
        $this->versions = $versions;
        $this->sheets = $sheets;
        $this->types = $types;
        $this->tiers = $tiers;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $subject = $this->subject();
        return view('user.sheets', [
            'subject' => $this->subject(),
            'years' => $this->sheets->showYears($subject->id),
            'versions' => $this->versions->all(),
            'types' => $this->types->all(),
            'tiers' => $this->tiers->all(),
            'months' => $this->sheets->showMonths($subject->id),
            'sheets' => ($this->sheets->paginate(
                $request->input('page', 1),
                $subject->id,
                true,
                $request->input('months', []),
                $request->input('years', []),
                $request->input('types', []),
                $request->input('versions', []),
                $request->input('tiers', []),
                $request->input('popularity'),
                $request->input('difficulty')
            ))->appends($request->except('page')),
            'c_months' => array_flip($request->input('months', [])),
            'c_years' => array_flip($request->input('years', [])),
            'c_types' => array_flip($request->input('types', [])),
            'c_versions' => array_flip($request->input('versions', [])),
            'c_tiers' => array_flip($request->input('tiers', [])),
            'c_popularity' => $request->input('popularity'),
            'c_difficulty' => $request->input('difficulty')

        ]);
    }

    /**
     * @param $subject
     * @param $release
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function show($subject, $release)
    {
        return view('user.sheet', [
            'subject' => $this->subject(),
            'sheet' => $this->sheets->show(
                $release,
                true,
                true,
                true,
                true,
                true
            ),
            'uuid' => auth('guest')->user()->getAuthIdentifier(),
        ]);
    }

    /**
     * @param $subject
     * @param $sheet
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function check($subject, $sheet, Request $request)
    {
        $this->sheets->startChecking(
            $this->sheets->show($sheet)->id,
            auth('guest')->user()->getAuthIdentifier(),
            $request->input('answer')
        );

        return response()->json($request->input('answer'));
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function subject(): Subject
    {
        return app()->make(Subject::class);
    }

}