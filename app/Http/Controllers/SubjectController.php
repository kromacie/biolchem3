<?php namespace App\Http\Controllers;

use App\Models\Subject;
use App\Services\EchoClientService;
use App\Services\QuestionService;
use App\Services\ResolvedQuestionService;
use App\Services\ResolvedSheetService;
use App\Services\SheetService;

class SubjectController extends Controller
{
    private $sheets;
    private $clients;
    private $resolvedSheet;
    private $resolvedQuestion;
    private $questions;

    /**
     * SubjectController constructor.
     * @param SheetService $sheets
     * @param EchoClientService $client
     * @param ResolvedSheetService $resolvedSheets
     * @param ResolvedQuestionService $resolvedQuestions
     */
    public function __construct(SheetService $sheets, EchoClientService $client, ResolvedSheetService $resolvedSheets, ResolvedQuestionService $resolvedQuestions, QuestionService $questions)
    {
        $this->sheets = $sheets;
        $this->clients = $client;
        $this->resolvedQuestion = $resolvedQuestions;
        $this->resolvedSheet = $resolvedSheets;
        $this->questions = $questions;
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Throwable
     */
    public function show()
    {
        $subject = $this->subject();

        return view('user.subject', [
            'subject' => $subject,
            'sheet_count' => $this->sheets->getCount($subject->id, true),
            'question_count' => $this->questions->getCount($subject->id, true),
            'sheet_users' => $this->clients->getSheetClients($subject->slug),
            'question_users' => $this->clients->getQuestionClients($subject->slug),
            'resolved_questions' => $this->resolvedQuestion->countBySubject($subject->id),
            'resolved_sheets' => $this->resolvedSheet->countBySubject($subject->id)
        ]);
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function subject()
    {
        return app()->make(Subject::class);
    }
}