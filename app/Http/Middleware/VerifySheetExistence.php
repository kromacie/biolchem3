<?php

namespace App\Http\Middleware;

use App\Models\Sheet;
use App\Services\SheetService;
use Closure;

class VerifySheetExistence
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function handle($request, Closure $next)
    {
        $id = $request->route('sheet');

        /** @var SheetService $sheets */
        $sheets = app()->make(SheetService::class);

        $sheet = $sheets->show($id, true);

        app()->instance(Sheet::class, $sheet);

        return $next($request);
    }
}
