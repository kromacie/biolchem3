<?php

namespace App\Http\Middleware;

use App\Models\Subject;
use App\Services\SubjectService;
use Closure;

class VerifySubjectExistence
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function handle($request, Closure $next)
    {
        $id = $request->route('subject');
        /** @var SubjectService $subjects */
        $subjects = app()->make(SubjectService::class);

        $subject = $subjects->show($id, true);

        app()->instance(Subject::class, $subject);

        return $next($request);
    }
}
