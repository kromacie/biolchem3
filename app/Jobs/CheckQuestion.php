<?php

namespace App\Jobs;

use App\Events\Question\Error;
use App\Models\Question;
use App\Services\QuestionService;
use App\Services\ResolvedQuestionService;
use App\Services\Validators\Question\SheetQuestionValidator;
use App\Services\Validators\Question\SingleQuestionEventLogger;
use App\Services\Validators\Question\SingleQuestionRewardCreator;
use App\Services\Validators\ResultSet;
use App\Services\Validators\ValidationFactory;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Collection;

class CheckQuestion implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $answers;

    public $question_id;

    public $user_id;

    /**
     * Create a new job instance.
     *
     * @param $question_id
     * @param $user_id
     * @param $answers
     */
    public function __construct($question_id, $user_id, $answers)
    {
        $this->answers = $answers;
        $this->question_id = $question_id;
        $this->user_id = $user_id;
    }

    /**
     * @return SheetQuestionValidator
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    private function buildQuestionValidatorInstance()
    {

        $validator = new ValidationFactory();
        $validator->setQuestionEventLogger(
            new SingleQuestionEventLogger($this->user_id)
        );
        $validator->setQuestionRewardCreator(
            new SingleQuestionRewardCreator(
                app()->make(ResolvedQuestionService::class)
            )
        );
        $validator->setUserId($this->user_id);

        $results = new ResultSet();
        $results->setCurrentQuestion($this->question_id);

        return $validator->buildSheetQuestionValidator($results);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            /** @var Question $question */
            $question = $this->questions()->adminShow($this->question_id);

            $this->buildQuestionValidatorInstance()->validate($question, Collection::wrap($this->answers)->first());
        } catch(\Throwable $exception) {
            try {
                \Log::alert($exception);
                event(new Error($this->user_id, $this->question_id));
            } catch (\Throwable $exception) {

            }
//            event(new Debug($this->sheet_id, $this->user_id, $exception->getMessage()));
//            event(new Debug($this->sheet_id, $this->user_id, $exception->getLine()));
//            event(new Debug($this->sheet_id, $this->user_id, $exception->getFile()));
//            event(new Debug($this->sheet_id, $this->user_id, $exception->getTraceAsString()));
        }
    }

    /**
     * @return QuestionService
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function questions(): QuestionService
    {
        return app()->make(QuestionService::class);
    }
}
