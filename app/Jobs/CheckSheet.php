<?php

namespace App\Jobs;

use App\Events\SheetCheckingError;
use App\Services\ResolvedQuestionService;
use App\Services\Validators\Question\SheetQuestionEventLogger;
use App\Services\Validators\Question\SheetQuestionRewardCreator;
use App\Services\SheetService;
use App\Services\Validators\Sheet\SheetValidator;
use App\Services\Validators\ValidationFactory;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CheckSheet implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $answers;

    public $sheet_id;

    public $user_id;

    /**
     * Create a new job instance.
     *
     * @param $sheet_id
     * @param $user_id
     * @param $answers
     */
    public function __construct($sheet_id, $user_id, $answers)
    {
        $this->answers = $answers;
        $this->sheet_id = $sheet_id;
        $this->user_id = $user_id;
    }

    /**
     * @return SheetValidator
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    private function buildSheetValidatorInstance()
    {
        $validator = new ValidationFactory();
        $validator->setQuestionEventLogger(
            new SheetQuestionEventLogger($this->user_id, $this->sheet_id)
        );
        $validator->setQuestionRewardCreator(
            new SheetQuestionRewardCreator(
                app()->make(ResolvedQuestionService::class), $this->sheet_id
            )
        );
        $validator->setSheetId($this->sheet_id);
        $validator->setUserId($this->user_id);

        return $validator->buildSheetValidator();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $sheet = $this->sheets()->adminShow($this->sheet_id);

            $this->buildSheetValidatorInstance()->validate($sheet, $this->answers ? $this->answers : []);
        } catch(\Throwable $exception) {
            try {
                \Log::alert($exception);
                event(new SheetCheckingError($this->user_id, $this->sheet_id));
            } catch (\Throwable $exception) {

            }
//            event(new Debug($this->sheet_id, $this->user_id, $exception->getMessage()));
//            event(new Debug($this->sheet_id, $this->user_id, $exception->getLine()));
//            event(new Debug($this->sheet_id, $this->user_id, $exception->getFile()));
//            event(new Debug($this->sheet_id, $this->user_id, $exception->getTraceAsString()));
        }
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    private function sheets(): SheetService
    {
        return app()->make(SheetService::class);
    }

}
