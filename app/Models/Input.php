<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $input_group_id
 * @property string $description
 * @property string $placeholder
 * @property boolean $disabled
 * @property boolean $checked
 * @property boolean $is_description
 * @property InputGroup $inputGroup
 * @property InputCorrect[]|Collection $corrects
 * @property InputType|null $type
 * @property int $type_id
 * @property Reward[] $rewards
 */
class Input extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['input_group_id', 'description', 'placeholder', 'disabled', 'checked', 'is_description', 'type_id'];

    protected $hidden = [
        'input_group_id', 'description', 'placeholder', 'disabled', 'checked', 'is_description', 'type_id'
    ];

    /**
     * Indicates if the model should be timestamped.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * The storage format of the model's date columns.
     * 
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * The connection name for the model.
     * 
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function inputGroup()
    {
        return $this->belongsTo('App\Models\InputGroup');
    }

    public function type()
    {
        return $this->belongsTo(InputType::class, 'type_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function corrects()
    {
        return $this->hasMany('App\Models\InputCorrect');
    }

    public function getClassesAttribute()
    {
        return $this->inputGroup->type->input_class . ($this->type_id ? ' ' . $this->type->class : '');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function rewards()
    {
        return $this->hasManyThrough(Reward::class, RewardHasInput::class, 'input_id', 'id', 'id', 'reward_id');
    }
}
