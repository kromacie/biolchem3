<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * @property integer $id
 * @property string $description
 * @property InputGroupType $type
 * @property Input[]|Collection $inputs
 * @property integer $type_id
 * @property string $class
 * @property QuestionSubstitute[] $questionSubstitutes
 */
class InputGroup extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['description', 'type_id'];

    protected $hidden = [
        'description', 'type_id', 'pivot'
    ];

    /**
     * Indicates if the model should be timestamped.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * The storage format of the model's date columns.
     * 
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * The connection name for the model.
     * 
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function inputs()
    {
        return $this->hasMany('App\Models\Input');
    }

    public function type()
    {
        return $this->belongsTo(InputGroupType::class, 'type_id', 'id');
    }

    public function getClassAttribute()
    {
        return $this->type->group_class;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function questionSubstitutes()
    {
        return $this->belongsToMany('App\Models\QuestionSubstitute', 'question_substitute_has_input_groups');
    }
}
