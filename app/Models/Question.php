<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

/**
 * @property integer $id
 * @property integer $sheet_id
 * @property boolean $enabled
 * @property string $title
 * @property integer $points
 * @property Sheet $sheet
 * @property QuestionHasContent[] $questionHasContents
 * @property Tag[]|Collection $tags
 * @property QuestionSubstitute[]|Collection $substitutes
 * @property ResolvedQuestion[]|Collection $resolved_questions
 * @property integer $total_got_result
 * @property integer $total_max_result
 * @property integer $percentage_result
 * @property integer $resolved_questions_count
 */
class Question extends Model
{
    use HasSlug;
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['sheet_id', 'enabled', 'title'];

    protected $hidden = [
        'enabled', 'sheet_id', 'slug'
    ];

    /**
     * Indicates if the model should be timestamped.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * The storage format of the model's date columns.
     * 
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * The connection name for the model.
     * 
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sheet()
    {
        return $this->belongsTo('App\Models\Sheet');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function contents()
    {
        return $this->hasManyThrough(Content::class, QuestionHasContent::class, 'question_id', 'id', 'id', 'content_id');
    }

    public function resolved_questions()
    {
        return $this->hasMany(ResolvedQuestion::class, 'question_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function tags()
    {
        return $this->hasManyThrough(Tag::class, QuestionHasTag::class, 'question_id', 'id', 'id', 'tag_id');
    }

    public function getTagGroupsAttribute()
    {
        return $this->tags->unique(function (Tag $tag){
            return $tag->tag_id;
        });
    }

    public function getPointsAttribute()
    {
        return $this->substitutes->sum('points');
    }

    public function getTotalGotResultAttribute()
    {
        return $this->resolved_questions->sum('got_result');
    }

    public function getTotalMaxResultAttribute()
    {
        return $this->resolved_questions->sum('max_result');
    }

    public function getPercentageResultAttribute()
    {
        return ($max = $this->total_max_result) ? round($this->total_got_result / $this->total_max_result, 2) * 100 : 0;
    }

    public function getResolvedQuestionsCountAttribute()
    {
        return $this->resolved_questions->sum('count');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function substitutes()
    {
        return $this->hasMany('App\Models\QuestionSubstitute');
    }

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }
}
