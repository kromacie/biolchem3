<?php

namespace App\Models;

use function foo\func;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $question_id
 * @property boolean $points
 * @property Question $question
 * @property Content[]|Collection $content
 * @property InputGroup[]|Collection $inputGroups
 * @property Reward[]|Collection $rewards
 */
class QuestionSubstitute extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['question_id', 'points'];

    protected $hidden = [
        'points', 'question_id'
    ];

    /**
     * Indicates if the model should be timestamped.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * The storage format of the model's date columns.
     * 
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * The connection name for the model.
     * 
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function question()
    {
        return $this->belongsTo('App\Models\Question');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function contents()
    {
        return $this->hasManyThrough(Content::class, QuestionSubstituteHasContent::class, 'question_substitute_id', 'id', 'id', 'content_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function inputGroups()
    {
        return $this->belongsToMany('App\Models\InputGroup', 'question_substitute_has_input_groups');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rewards()
    {
        return $this->hasMany('App\Models\Reward');
    }

    public function getPointsAttribute()
    {
        return $this->rewards->max(function (Reward $reward){
            return $reward->points;
        });
    }
}
