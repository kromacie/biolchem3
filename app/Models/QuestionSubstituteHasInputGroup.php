<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $question_substitute_id
 * @property integer $input_group_id
 * @property QuestionSubstitute $questionSubstitute
 * @property InputGroup $inputGroup
 */
class QuestionSubstituteHasInputGroup extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['question_substitute_id', 'input_group_id'];

    /**
     * Indicates if the model should be timestamped.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * The storage format of the model's date columns.
     * 
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * The connection name for the model.
     * 
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function questionSubstitute()
    {
        return $this->belongsTo('App\Models\QuestionSubstitute');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function inputGroup()
    {
        return $this->belongsTo('App\Models\InputGroup');
    }
}
