<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $question_substitute_id
 * @property boolean $points
 * @property string $name
 * @property QuestionSubstitute $questionSubstitute
 * @property Input[]|Collection $inputs
 */
class Reward extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['question_substitute_id', 'points', 'name'];

    /**
     * Indicates if the model should be timestamped.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * The storage format of the model's date columns.
     * 
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * The connection name for the model.
     * 
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function questionSubstitute()
    {
        return $this->belongsTo('App\Models\QuestionSubstitute');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function inputs()
    {
        return $this->hasManyThrough('App\Models\Input', RewardHasInput::class, 'reward_id', 'id', 'id', 'input_id');
    }
}
