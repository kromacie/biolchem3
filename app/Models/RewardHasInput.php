<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $reward_id
 * @property integer $input_id
 * @property Input $input
 * @property Reward $reward
 */
class RewardHasInput extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['reward_id', 'input_id'];

    /**
     * Indicates if the model should be timestamped.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * The storage format of the model's date columns.
     * 
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * The connection name for the model.
     * 
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function input()
    {
        return $this->belongsTo('App\Models\Input');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reward()
    {
        return $this->belongsTo('App\Models\Reward');
    }
}
