<?php

namespace App\Models;

use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

/**
 * @property integer $id
 * @property integer $subject_id
 * @property string $release
 * @property integer $version_id
 * @property Subject $subject
 * @property Question[]|Collection $questions
 * @property integer $counter
 * @property integer $year
 * @property integer $month
 * @property integer $tier_id
 * @property SheetTier $tier
 * @property SheetVersion $version
 * @property integer $count
 * @property SheetType $type
 * @property boolean $enabled
 * @property integer $points
 * @property ResolvedSheet[]|Collection $resolved_sheets
 * @property ResolvedQuestion[]|Collection $resolved_questions
 * @property integer $resolved_sheets_count
 * @property integer $total_got_result
 * @property integer $total_max_result
 * @property integer $percentage_result
 * @property integer $server_percentage_result
 */
class Sheet extends Model
{
    use HasSlug;
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['subject_id', 'release', 'version_id', 'enabled', 'tier_id', 'type_id'];

    protected $hidden = [
        'subject_id', 'version_id', 'enabled', 'tier_id', 'type_id', 'slug'
    ];


    /**
     * Indicates if the model should be timestamped.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * The storage format of the model's date columns.
     * 
     * @var string
     */
    protected $dateFormat = 'Y-m-d H:i:s';

    protected $casts = [
        'release' => 'datetime'
    ];


    /**
     * The connection name for the model.
     * 
     * @var string
     */
    protected $connection = 'mysql';

    public function getReleaseAttribute()
    {
        return Str::ucfirst(Carbon::createFromTimeString($this->attributes['release'])->isoFormat("MMMM YYYY"));
    }

    public function getMonthAttribute()
    {
        return Str::ucfirst(Carbon::createFromTimeString($this->attributes['release'])->isoFormat("MMMM"));
    }

    public function getYearAttribute()
    {
        return isset($this->attributes['year']) ? $this->attributes['year'] : Str::ucfirst(Carbon::createFromTimeString($this->attributes['release'])->isoFormat("YYYY"));
    }

    public function getIsoReleaseAttribute()
    {
        return Carbon::createFromTimeString($this->attributes['release'])->format('Y-m-d');
    }

    public function getPointsAttribute()
    {
        return $this->questions->sum('points');
    }

    public function getPercentageResultAttribute()
    {
        return ($max = $this->total_max_result) ? round($this->total_got_result / $this->total_max_result, 2) * 100 : 0;
    }

    public function getTotalGotResultAttribute()
    {
        return $this->resolved_questions->sum('got_result');
    }

    public function getTotalMaxResultAttribute()
    {
        return $this->resolved_questions->sum('max_result');
    }

    public function percentageOfTotalSheets($total)
    {
        return $total ? round($this->count / $total, 2) * 100 : 0;
    }

    public function getResolvedSheetsCountAttribute()
    {
        return $this->resolved_sheets->sum('count');
    }

    public function percentageOfTotalResolvedSheets($total)
    {
        return $total ? round($this->resolved_sheets_count / $total, 2) * 100 : 0;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subject()
    {
        return $this->belongsTo('App\Models\Subject');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questions()
    {
        return $this->hasMany('App\Models\Question');
    }

    public function version()
    {
        return $this->belongsTo(SheetVersion::class, 'version_id', 'id');
    }

    public function tier()
    {
        return $this->belongsTo(SheetTier::class, 'tier_id', 'id');
    }

    public function type()
    {
        return $this->belongsTo(SheetType::class, 'type_id', 'id');
    }

    public function resolved_sheets()
    {
        return $this->hasMany(ResolvedSheet::class, 'sheet_id', 'id');
    }

    public function resolved_questions()
    {
        return $this->hasMany(ResolvedQuestion::class, 'sheet_id', 'id');
    }


    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->saveSlugsTo('slug')
            ->generateSlugsFrom('release');
    }
}
