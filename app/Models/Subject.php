<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

/**
 * @property integer $id
 * @property string $name
 * @property string $icon
 * @property string $slug
 * @property string $theme
 * @property boolean $enabled
 * @property Sheet[]|Collection $sheets
 * @property TagGroup[] $tagGroups
 * @property SubjectDescription|null $description
 * @property integer $total_sheet_count
 */
class Subject extends Model
{
    use HasSlug;
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['name', 'icon', 'enabled', 'theme'];

    protected $hidden = [

    ];

    /**
     * Indicates if the model should be timestamped.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * The storage format of the model's date columns.
     * 
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * The connection name for the model.
     * 
     * @var string
     */
    protected $connection = 'mysql';

    public function getTotalSheetsCountAttribute()
    {
        return $this->sheets->sum('count');
    }

    public function getTotalResolvedSheetsCountAttribute()
    {
        return $this->sheets->sum('resolved_sheets_count');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sheets()
    {
        return $this->hasMany('App\Models\Sheet');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tagGroups()
    {
        return $this->hasMany('App\Models\TagGroup');
    }

    public function description()
    {
        return $this->hasOne(SubjectDescription::class, 'subject_id', 'id');
    }

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }
}
