<?php

namespace App\Providers;

use App\Services\Socket\AbstractConnector;
use App\Services\Socket\NonSecuredConnector;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Support\Carbon;
use Illuminate\Support\ServiceProvider;

EloquentUserProvider::class;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Carbon::setLocale('pl');

        $this->app->bind(AbstractConnector::class, NonSecuredConnector::class);
    }
}
