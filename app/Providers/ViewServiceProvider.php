<?php

namespace App\Providers;

use App\Services\SubjectService;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\View as ViewInstance;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...
        View::composer(
            'layouts.app', function (ViewInstance $view) {
                $view->with('subjects', $this->app->make(SubjectService::class)->search(true));
            }
        );
    }
}
