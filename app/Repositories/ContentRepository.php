<?php namespace App\Repositories;

use App\Models\Content;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class ContentRepository extends AbstractRepository
{
    public static function getClass(): String
    {
        return Content::class;
    }
}