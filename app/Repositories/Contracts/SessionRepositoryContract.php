<?php namespace App\Repositories\Contracts;

interface SessionRepositoryContract
{
    public function getById($id);
    public function getByCredentials($credentials);
}