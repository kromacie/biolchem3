<?php namespace App\Repositories;

use App\Models\Image;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class ImageRepository extends AbstractRepository
{

    public static function getClass(): String
    {
        return Image::class;
    }
}