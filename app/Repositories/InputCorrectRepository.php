<?php namespace App\Repositories;

use App\Models\InputCorrect;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class InputCorrectRepository extends AbstractRepository
{

    public static function getClass(): String
    {
        return InputCorrect::class;
    }
}