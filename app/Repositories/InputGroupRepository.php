<?php namespace App\Repositories;

use App\Models\InputGroup;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class InputGroupRepository extends AbstractRepository
{

    public static function getClass(): String
    {
        return InputGroup::class;
    }
}