<?php namespace App\Repositories;

use App\Models\InputGroupType;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class InputGroupTypeRepository extends AbstractRepository
{
    public static function getClass(): String
    {
        return InputGroupType::class;
    }
}