<?php namespace App\Repositories;

use App\Models\Input;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class InputRepository extends AbstractRepository
{

    public static function getClass(): String
    {
        return Input::class;
    }
}