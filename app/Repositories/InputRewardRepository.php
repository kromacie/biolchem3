<?php namespace App\Repositories;

use App\Models\RewardHasInput;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class InputRewardRepository extends AbstractRepository
{

    public static function getClass(): String
    {
        return RewardHasInput::class;
    }
}