<?php namespace App\Repositories;

use App\Models\InputType;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class InputTypeRepository extends AbstractRepository
{
    public static function getClass(): String
    {
        return InputType::class;
    }
}