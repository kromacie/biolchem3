<?php namespace App\Repositories;

use App\Models\QuestionHasContent;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class QuestionContentRepository extends AbstractRepository
{
    public static function getClass(): String
    {
        return QuestionHasContent::class;
    }
}