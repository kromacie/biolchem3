<?php namespace App\Repositories;

use App\Models\Question;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class QuestionRepository extends AbstractRepository
{
    public static function getClass(): String
    {
        return Question::class;
    }
}