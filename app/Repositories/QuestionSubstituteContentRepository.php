<?php namespace App\Repositories;

use App\Models\QuestionSubstituteHasContent;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class QuestionSubstituteContentRepository extends AbstractRepository
{

    public static function getClass(): String
    {
        return QuestionSubstituteHasContent::class;
    }
}