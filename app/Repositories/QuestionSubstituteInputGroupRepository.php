<?php namespace App\Repositories;

use App\Models\QuestionSubstituteHasInputGroup;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class QuestionSubstituteInputGroupRepository extends AbstractRepository
{

    public static function getClass(): String
    {
        return QuestionSubstituteHasInputGroup::class;
    }
}