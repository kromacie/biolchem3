<?php namespace App\Repositories;

use App\Models\QuestionSubstitute;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class QuestionSubstituteRepository extends AbstractRepository
{
    public static function getClass(): String
    {
        return QuestionSubstitute::class;
    }
}