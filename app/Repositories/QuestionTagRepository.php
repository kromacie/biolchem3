<?php namespace App\Repositories;

use App\Models\QuestionHasTag;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class QuestionTagRepository extends AbstractRepository
{

    public static function getClass(): String
    {
        return QuestionHasTag::class;
    }
}