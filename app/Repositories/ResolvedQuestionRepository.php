<?php namespace App\Repositories;

use App\Models\ResolvedQuestion;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class ResolvedQuestionRepository extends AbstractRepository
{
    public static function getClass(): String
    {
        return ResolvedQuestion::class;
    }
}