<?php namespace App\Repositories;

use App\Models\ResolvedSheet;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class ResolvedSheetRepository extends AbstractRepository
{
    public static function getClass(): String
    {
        return ResolvedSheet::class;
    }
}