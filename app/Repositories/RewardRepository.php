<?php namespace App\Repositories;

use App\Models\Reward;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class RewardRepository extends AbstractRepository
{

    public static function getClass(): String
    {
        return Reward::class;
    }
}