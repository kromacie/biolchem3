<?php namespace App\Repositories;

use App\Models\Session;
use App\Repositories\Contracts\SessionRepositoryContract;
use App\Scopes\Universal\ScopeID;
use Illuminate\Support\Arr;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class SessionRepository extends AbstractRepository implements SessionRepositoryContract
{

    public static function getClass(): String
    {
        return Session::class;
    }

    public function getById($id)
    {
        return $this->scope(new ScopeID($id))->first();
    }

    public function getByCredentials($credentials)
    {
        return $this->getById(Arr::get($credentials, 'id', null));
    }
}