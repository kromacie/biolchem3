<?php namespace App\Repositories;

use App\Models\Sheet;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class SheetRepository extends AbstractRepository
{

    public static function getClass(): String
    {
        return Sheet::class;
    }
}