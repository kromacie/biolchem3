<?php namespace App\Repositories;

use App\Models\SheetTier;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class SheetTierRepository extends AbstractRepository
{
    public static function getClass(): String
    {
        return SheetTier::class;
    }
}