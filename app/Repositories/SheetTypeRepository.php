<?php namespace App\Repositories;

use App\Models\SheetType;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class SheetTypeRepository extends AbstractRepository
{

    public static function getClass(): String
    {
        return SheetType::class;
    }
}