<?php namespace App\Repositories;

use App\Models\SheetVersion;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class SheetVersionRepository extends AbstractRepository
{

    public static function getClass(): String
    {
        return SheetVersion::class;
    }
}