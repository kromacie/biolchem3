<?php namespace App\Repositories;

use App\Models\SubjectDescription;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class SubjectDescriptionRepository extends AbstractRepository
{

    public static function getClass(): String
    {
        return SubjectDescription::class;
    }
}