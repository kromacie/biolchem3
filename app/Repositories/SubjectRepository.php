<?php namespace App\Repositories;

use App\Models\Subject;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class SubjectRepository extends AbstractRepository
{

    public static function getClass(): String
    {
        return Subject::class;
    }
}