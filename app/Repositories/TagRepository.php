<?php namespace App\Repositories;

use App\Models\Tag;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class TagRepository extends AbstractRepository
{

    public static function getClass(): String
    {
        return Tag::class;
    }
}