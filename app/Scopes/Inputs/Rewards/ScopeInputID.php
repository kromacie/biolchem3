<?php namespace App\Scopes\Inputs\Rewards;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeInputID implements ScopeInterface
{
    private $input_id;

    /**
     * ScopeInputID constructor.
     * @param $input_id
     */
    public function __construct($input_id)
    {
        $this->input_id = $input_id;
    }


    public function scope(Builder $builder)
    {
        $builder->where('input_id', '=', $this->input_id);
    }
}