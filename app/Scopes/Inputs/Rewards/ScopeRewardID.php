<?php namespace App\Scopes\Inputs\Rewards;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeRewardID implements ScopeInterface
{

    private $reward_id;

    /**
     * ScopeRewardID constructor.
     * @param $reward_id
     */
    public function __construct($reward_id)
    {
        $this->reward_id = $reward_id;
    }


    public function scope(Builder $builder)
    {
        $builder->where('reward_id', '=', $this->reward_id);
    }
}