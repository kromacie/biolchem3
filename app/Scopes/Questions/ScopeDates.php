<?php namespace App\Scopes\Questions;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeDates implements ScopeInterface
{
    private $years;
    private $months;

    /**
     * ScopeDates constructor.
     * @param $years
     * @param $months
     */
    public function __construct(array $years, array $months)
    {
        $this->years = $years;
        $this->months = $months;
    }


    public function scope(Builder $builder)
    {
        $builder->whereHas('sheet', function (Builder $builder){
            foreach ($this->years as $year) {
                $builder->orWhereYear('release', '=', $year);
            }
        });

        $builder->whereHas('sheet', function (Builder $builder){
            foreach ($this->months as $month) {
                $builder->orWhereMonth('release', '=', $month);
            }
        });
    }
}