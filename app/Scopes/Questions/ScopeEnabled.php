<?php namespace App\Scopes\Questions;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeEnabled implements ScopeInterface
{

    private $enabled;

    /**
     * ScopeEnabled constructor.
     * @param $enabled
     */
    public function __construct($enabled)
    {
        $this->enabled = $enabled;
    }


    public function scope(Builder $builder)
    {
        $builder->where('enabled', '=', $this->enabled);
    }
}