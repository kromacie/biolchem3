<?php namespace App\Scopes\Questions;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeQuestion implements ScopeInterface
{

    private $slug;

    /**
     * ScopeQuestion constructor.
     * @param $slug
     */
    public function __construct($slug)
    {
        $this->slug = $slug;
    }


    public function scope(Builder $builder)
    {
        $builder->where('slug', $this->slug);
    }
}