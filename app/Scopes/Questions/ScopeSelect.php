<?php namespace App\Scopes\Questions;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeSelect implements ScopeInterface
{

    public function scope(Builder $builder)
    {
        $builder->addSelect('questions.*');
    }
}