<?php namespace App\Scopes\Questions;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeSheetID implements ScopeInterface
{

    private $sheet_id;

    /**
     * ScopeSheetID constructor.
     * @param $sheet_id
     */
    public function __construct($sheet_id)
    {
        $this->sheet_id = $sheet_id;
    }


    public function scope(Builder $builder)
    {
        $builder->where('sheet_id', $this->sheet_id);
    }
}