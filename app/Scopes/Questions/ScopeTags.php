<?php namespace App\Scopes\Questions;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeTags implements ScopeInterface
{

    private $tags;
    private $categories;

    /**
     * ScopeTags constructor.
     * @param $tags
     * @param $categories
     */
    public function __construct($tags, $categories)
    {
        $this->tags = $tags;
        $this->categories = $categories;
    }


    public function scope(Builder $builder)
    {

        $__tags = $this->tags;
        $__categories = $this->categories;

        $builder->whereHas('tags', function (Builder $builder){
            $builder->where(function (Builder $builder){
                $builder->orWhere(function (Builder $builder) {

                    while ($tag = $this->takeTag()) {
                        if($this->hasCategories($tag)) {

                            $builder->where(function (Builder $builder) use ($tag){
                                while ($category = $this->takeCategory($tag)) {
                                    $builder->orWhere('slug', $category);
                                }
                            });

                        } else {
                            $builder->orWhere('slug', $tag);
                        }

                    }
                });


                if($this->categories) {
                    $builder->orWhere(function (Builder $builder) {
                        while ($category = $this->takeFirstCategory()) {
                            $builder->orWhere('slug', $category);
                        }
                    });
                }
            });
        });


        $this->categories = $__categories;
        $this->tags = $__tags;
    }

    public function takeTag()
    {
        foreach ($this->tags as $key => $tag) {
            unset($this->tags[$key]);
            return $tag;
        }
    }

    public function takeCategory($tag)
    {
        if(isset($this->categories[$tag])) {
            if(!is_array($this->categories[$tag])) {
                unset($this->categories[$tag]);
                return false;
            }

            foreach ($this->categories[$tag] as $key => $category) {
                unset($this->categories[$tag][$key]);
                return $category;
            }
        }

        return false;
    }

    public function takeFirstCategory()
    {
        foreach ($this->categories as $key => $category) {
            if(!is_array($category)) {
                unset($this->categories[$key]);
                continue;
            }
            foreach ($this->categories[$key] as $_key => $value) {
                unset($this->categories[$key][$_key]);
                return $value;
            }
        }

        return false;
    }

    public function hasCategories($tag)
    {
        return isset($this->categories[$tag]) && is_array($this->categories[$tag]);
    }
}