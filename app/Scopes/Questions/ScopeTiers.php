<?php namespace App\Scopes\Questions;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeTiers implements ScopeInterface
{

    private $tiers;

    /**
     * ScopeTiers constructor.
     * @param $tiers
     */
    public function __construct($tiers)
    {
        $this->tiers = $tiers;
    }

    public function scope(Builder $builder)
    {
        $builder->with('sheet.tier');

        $builder->whereHas('sheet.tier', function (Builder $builder){
            $builder->where(function (Builder $builder) {
                foreach ($this->tiers as $tier) {
                    $builder->orWhere('slug', $tier);
                }
            });
        });
    }
}