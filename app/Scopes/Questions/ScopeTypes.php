<?php namespace App\Scopes\Questions;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeTypes implements ScopeInterface
{
    private $types;

    /**
     * ScopeTypes constructor.
     * @param $types
     */
    public function __construct(array $types)
    {
        $this->types = $types;
    }

    public function scope(Builder $builder)
    {
        $builder->with('sheet.type');

        $builder->whereHas('sheet.type', function (Builder $builder){
            $builder->where(function (Builder $builder) {
                foreach ($this->types as $type) {
                    $builder->orWhere('slug', '=', $type);
                }
            });
        });
    }
}