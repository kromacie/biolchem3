<?php namespace App\Scopes\Questions;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeVersions implements ScopeInterface
{

    private $versions;

    /**
     * ScopeTypes constructor.
     * @param $types
     */
    public function __construct(array $types)
    {
        $this->versions = $types;
    }

    public function scope(Builder $builder)
    {
        $builder->with('sheet.version');

        $builder->whereHas('sheet.version', function (Builder $builder){
            $builder->where(function (Builder $builder) {
                foreach ($this->versions as $version) {
                    $builder->orWhere('slug', $version);
                }
            });
        });
    }
}