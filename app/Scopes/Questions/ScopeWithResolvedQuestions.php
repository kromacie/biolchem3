<?php namespace App\Scopes\Questions;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeWithResolvedQuestions implements ScopeInterface
{

    public function scope(Builder $builder)
    {
        $builder->with(['resolved_questions' => function(HasMany $many){
            $many->select(
                \DB::raw('COUNT(`id`) `count`'),
                \DB::raw('MIN(`question_id`) `question_id`'),
                \DB::raw('SUM(`got_result`) `got_result`'),
                \DB::raw('SUM(`max_result`) `max_result`')
            );

            $many->groupBy('question_id');
        }]);
    }
}