<?php namespace App\Scopes\Questions;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\JoinClause;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class SortByResolvedQuestions implements ScopeInterface
{

    private $difficulty;
    private $popularity;

    /**
     * SortByResolvedQuestions constructor.
     * @param $difficulty
     * @param $popularity
     */
    public function __construct($difficulty, $popularity)
    {
        $this->difficulty = $difficulty;
        $this->popularity = $popularity;
    }


    public function scope(Builder $builder)
    {
        $builder->leftJoin('resolved_questions', function (JoinClause $clause){
            $clause->on('resolved_questions.question_id', '=', 'questions.id');
        });

        if($this->difficulty == 'asc' || $this->difficulty == 'desc') {
            $builder->addSelect(\DB::raw('ROUND(SUM(`got_result`) / SUM(`max_result`), 2) * 100 `server_percentage_result`'));
            $builder->orderBy('server_percentage_result', $this->difficulty);

        }
        if($this->popularity == 'asc' || $this->popularity == 'desc') {
            $builder->addSelect(\DB::raw('COUNT(`questions`.`id`) `server_count_result`'));
            $builder->orderBy('server_count_result', $this->popularity);
        }

        $builder->groupBy('questions.id');
    }
}