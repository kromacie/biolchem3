<?php namespace App\Scopes\Questions\Tags;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeQuestionID implements ScopeInterface
{

    private $question_id;

    /**
     * ScopeQuestionID constructor.
     * @param $question_id
     */
    public function __construct($question_id)
    {
        $this->question_id = $question_id;
    }


    public function scope(Builder $builder)
    {
        $builder->where('question_id', '=', $this->question_id);
    }
}