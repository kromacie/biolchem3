<?php namespace App\Scopes\Questions\Tags;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeTagID implements ScopeInterface
{
    private $tag_id;

    /**
     * ScopeTagID constructor.
     * @param $tag_id
     */
    public function __construct($tag_id)
    {
        $this->tag_id = $tag_id;
    }


    public function scope(Builder $builder)
    {
        $builder->where('tag_id', '=', $this->tag_id);
    }
}