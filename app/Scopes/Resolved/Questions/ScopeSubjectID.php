<?php namespace App\Scopes\Resolved\Questions;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeSubjectID implements ScopeInterface
{

    private $subject;

    /**
     * ScopeSubject constructor.
     * @param $subject
     */
    public function __construct($subject)
    {
        $this->subject = $subject;
    }


    public function scope(Builder $builder)
    {
        $builder->whereHas('question.sheet.subject', function (Builder $builder){
            return $builder->where('id', '=', $this->subject);
        });
    }
}