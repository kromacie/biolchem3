<?php namespace App\Scopes\Resolved\Sheets;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeSubjectID implements ScopeInterface
{

    private $subject_id;

    /**
     * ScopeSubjectID constructor.
     * @param $subject_id
     */
    public function __construct($subject_id)
    {
        $this->subject_id = $subject_id;
    }


    public function scope(Builder $builder)
    {
        $builder->whereHas('sheet.subject', function (Builder $builder) {
            $builder->where('id', '=', $this->subject_id);
        });
    }
}