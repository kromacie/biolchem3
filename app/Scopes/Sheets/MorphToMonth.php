<?php namespace App\Scopes\Sheets;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class MorphToMonth implements ScopeInterface
{

    public function scope(Builder $builder)
    {
        $builder->select(
            DB::raw('MAX(`release`) `release`'), DB::raw('count(`release`) `counter`'), DB::raw('date_format(`release`, "%M") `month`')
        );
    }
}