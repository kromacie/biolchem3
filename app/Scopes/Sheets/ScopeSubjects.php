<?php namespace App\Scopes\Sheets;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeSubjects implements ScopeInterface
{
    private $subject_id;

    /**
     * ScopeSubjects constructor.
     * @param $subject_id
     */
    public function __construct($subject_id)
    {
        $this->subject_id = $subject_id;
    }


    public function scope(Builder $builder)
    {
        $builder->where('subject_id', $this->subject_id);
    }
}