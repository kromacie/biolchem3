<?php namespace App\Scopes\Sheets;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeVersions implements ScopeInterface
{

    private $versions;

    /**
     * ScopeTypes constructor.
     * @param $types
     */
    public function __construct(array $types)
    {
        $this->versions = $types;
    }

    public function scope(Builder $builder)
    {
        $builder->with('version');

        $builder->whereHas('version', function (Builder $builder){
            $builder->where(function (Builder $builder) {
                foreach ($this->versions as $version) {
                    $builder->orWhere('slug', $version);
                }
            });
        });
    }
}