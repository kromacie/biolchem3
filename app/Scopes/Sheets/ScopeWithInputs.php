<?php namespace App\Scopes\Sheets;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeWithInputs implements ScopeInterface
{

    public function scope(Builder $builder)
    {
        $builder->with('questions.substitutes.inputGroups.inputs');
    }
}