<?php namespace App\Scopes\Sheets;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeWithResolvedQuestions implements ScopeInterface
{

    public function scope(Builder $builder)
    {
        $builder->with(['resolved_questions' => function(HasMany $many){
            $many->select(
                \DB::raw('MIN(`sheet_id`) `sheet_id`'),
                \DB::raw('COUNT(`id`) `count`'),
                \DB::raw('SUM(`got_result`) `got_result`'),
                \DB::raw('SUM(`max_result`) `max_result`'),
                \DB::raw('ROUND(SUM(`got_result`) / SUM(`max_result`), 2) * 100 `server_percentage_result`')
            );

            $many->groupBy('sheet_id');
        }]);
    }
}