<?php namespace App\Scopes\Sheets;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeWithResolvedSheets implements ScopeInterface
{

    public function scope(Builder $builder)
    {
        $builder->with(['resolved_sheets' => function(HasMany $many) {

            $many->select(
                \DB::raw('MIN(`sheet_id`) `sheet_id`'),
                \DB::raw('COUNT(`id`) `count`')
            );

            $many->groupBy('sheet_id');

        }]);
    }
}