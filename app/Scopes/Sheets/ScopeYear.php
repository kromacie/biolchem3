<?php namespace App\Scopes\Sheets;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeYear implements ScopeInterface
{

    private $year;

    /**
     * ScopeYear constructor.
     * @param $year
     */
    public function __construct($year)
    {
        $this->year = $year;
    }


    public function scope(Builder $builder)
    {
        return $builder->whereYear('release', '=', $this->year);
    }
}