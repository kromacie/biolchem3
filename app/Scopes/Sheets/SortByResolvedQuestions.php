<?php namespace App\Scopes\Sheets;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\JoinClause;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class SortByResolvedQuestions implements ScopeInterface
{

    private $sort;

    /**
     * SortByResolvedQuestions constructor.
     * @param $sort
     */
    public function __construct($sort)
    {
        $this->sort = $sort;
    }


    public function scope(Builder $builder)
    {
        $builder->leftJoin('resolved_questions', function (JoinClause $clause){
            $clause->on('resolved_questions.sheet_id', '=', 'sheets.id');
        });

        $builder->addSelect(\DB::raw('ROUND(SUM(`got_result`) / SUM(`max_result`), 2) * 100 `server_percentage_result`'));

        $builder->groupBy('sheets.id');
        $builder->orderBy('server_percentage_result', $this->sort);
    }
}