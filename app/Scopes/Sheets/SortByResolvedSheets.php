<?php namespace App\Scopes\Sheets;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\JoinClause;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class SortByResolvedSheets implements ScopeInterface
{

    private $sort;

    public function __construct($sort)
    {
        $this->sort = $sort;
    }

    public function scope(Builder $builder)
    {
        $builder->leftJoin('resolved_sheets', function (JoinClause $clause){
            $clause->on('resolved_sheets.sheet_id', '=', 'sheets.id');
        });

        $builder->addSelect(\DB::raw('COUNT(`resolved_sheets`.`id`) `resolved_sheets_count`'));

        $builder->groupBy('sheets.id');
        $builder->orderBy('resolved_sheets_count', $this->sort);
    }
}