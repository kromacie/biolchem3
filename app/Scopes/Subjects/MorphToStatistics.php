<?php namespace App\Scopes\Subjects;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class MorphToStatistics implements ScopeInterface
{

    public function scope(Builder $builder)
    {
        $builder->with(['resolved_questions' => function(HasMany $many){
            $many->select(
                \DB::raw('MIN(`sheet_id`) `sheet_id`'),
                \DB::raw('COUNT(`id`) `count`'),
                \DB::raw('SUM(`got_result`) `got_result`'),
                \DB::raw('SUM(`max_result`) `max_result`')
            );

            $many->groupBy('sheet_id');
        }]);
    }
}