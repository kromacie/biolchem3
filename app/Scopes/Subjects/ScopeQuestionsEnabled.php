<?php namespace App\Scopes\Subjects;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeQuestionsEnabled implements ScopeInterface
{

    private $enabled;

    /**
     * ScopeQuestionsEnabled constructor.
     * @param $enabled
     */
    public function __construct($enabled)
    {
        $this->enabled = $enabled;
    }


    public function scope(Builder $builder)
    {
        $builder->whereHas('sheets.questions', function (Builder $builder) {
            $builder->where('enabled', '=', $this->enabled);
        });
    }
}