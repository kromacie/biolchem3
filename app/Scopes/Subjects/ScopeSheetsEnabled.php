<?php namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeSheetsEnabled implements ScopeInterface
{

    private $enabled;

    /**
     * ScopeSheetsEnabled constructor.
     * @param $enabled
     */
    public function __construct($enabled)
    {
        $this->enabled = $enabled;
    }


    public function scope(Builder $builder)
    {
        $builder->whereHas('sheets', function (Builder $builder) {
            $builder->where('enabled', '=', $this->enabled);
        });
    }
}