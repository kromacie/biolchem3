<?php namespace App\Scopes\Subjects;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeWithResolvedSheets implements ScopeInterface
{

    public function scope(Builder $builder)
    {
        $builder->with('sheets.resolved');
    }
}