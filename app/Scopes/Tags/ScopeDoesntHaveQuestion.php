<?php namespace App\Scopes\Tags;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeDoesntHaveQuestion implements ScopeInterface
{
    private $question_id;

    /**
     * ScopeDoesntHaveQuestion constructor.
     * @param $question_id
     */
    public function __construct($question_id)
    {
        $this->question_id = $question_id;
    }


    public function scope(Builder $builder)
    {
        $builder->whereDoesntHave('questionHasTags', function (Builder $builder){
            $builder->where('question_id', '=', $this->question_id);
        });
    }
}