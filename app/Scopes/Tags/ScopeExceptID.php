<?php namespace App\Scopes\Tags;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeExceptID implements ScopeInterface
{
    private $id;

    /**
     * ScopeExceptID constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }


    public function scope(Builder $builder)
    {
        $builder->whereNotIn('id', $this->id);
    }
}