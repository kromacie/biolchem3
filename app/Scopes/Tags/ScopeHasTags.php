<?php namespace App\Scopes\Tags;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeHasTags implements ScopeInterface
{
    public function scope(Builder $builder)
    {
        $builder->has('tags');
    }
}