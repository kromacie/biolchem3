<?php namespace App\Scopes\Tags;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeTags implements ScopeInterface
{

    public function scope(Builder $builder)
    {
        $builder->with('tags', 'tag');
    }
}