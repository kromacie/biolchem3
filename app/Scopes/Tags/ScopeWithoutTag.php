<?php namespace App\Scopes\Tags;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeWithoutTag implements ScopeInterface
{
    public function scope(Builder $builder)
    {
        $builder->doesntHave('tag');
    }
}