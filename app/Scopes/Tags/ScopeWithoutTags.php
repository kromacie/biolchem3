<?php namespace App\Scopes\Tags;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeWithoutTags implements ScopeInterface
{

    public function scope(Builder $builder)
    {
        $builder->doesntHave('tags');
    }
}