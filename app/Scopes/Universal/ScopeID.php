<?php namespace App\Scopes\Universal;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeID implements ScopeInterface
{

    private $id;

    /**
     * ScopeID constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }


    public function scope(Builder $builder)
    {
        $builder->where('id', '=', $this->id);
    }
}