<?php namespace App\Scopes\Universal;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopeSlug implements ScopeInterface
{
    private $slug;

    /**
     * ScopeSlug constructor.
     * @param $slug
     */
    public function __construct($slug)
    {
        $this->slug = $slug;
    }


    public function scope(Builder $builder)
    {
        $builder->where('slug', $this->slug);
    }
}