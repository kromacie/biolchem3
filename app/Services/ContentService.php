<?php namespace App\Services;

use App\Actions\Create;
use App\Actions\Delete;
use App\Actions\Show;
use App\Actions\Update;
use App\Models\Content;
use App\Repositories\ContentRepository;
use App\Validators\Admin\Content\CreateValidator;
use App\Validators\Admin\Content\UpdateValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Arr;
use Illuminate\Validation\ValidationException;

class ContentService extends Service
{

    /**
     * @param $data
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function adminCreate($data)
    {
        if(isset($data['image'])) {
            $data['image_id'] = $this->images()->adminCreate($data)->id;
        }

        $data = CreateValidator::make()->validate($data);

        if(!(Arr::has($data, 'image_id') || (Arr::has($data, 'description') && strlen($data['description'])) > 0)) {
            throw ValidationException::withMessages([
                'Description or Image must be set!'
            ]);
        }

        return $this->contents()->perform(
            new Create($data)
        );

    }

    /**
     * @param $id
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminDelete($id)
    {
        $content = $this->validateId($id);

        if($content->image_id) {
            $this->images()->adminDelete($content->image_id);
        }

        return $this->contents()->perform(new Delete($id));
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     * @throws ValidationException
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminUpdate($id, $data)
    {
        $this->validateId($id);

        return $this->contents()->perform(
            new Update($id, UpdateValidator::make()->validate($data))
        );
    }

    /**
     * @param $id
     * @return Content|mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminShow($id)
    {
        return $this->validateId($id);
    }

    /**
     * @return ImageService
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function images(): ImageService
    {
        return app()->make(ImageService::class);
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function validateId($id): Content
    {
        $model = $this->contents()->perform(new Show($id));

        if(!($model instanceof Content)) {
            throw new ModelNotFoundException("Dane pytanie nie istnieje!");
        }

        return $model;
    }

    /**
     * @return ContentRepository
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function contents(): ContentRepository
    {
        return app()->make(ContentRepository::class);
    }
}