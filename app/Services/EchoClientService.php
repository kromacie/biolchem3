<?php namespace App\Services;

use App\Services\Socket\AbstractConnector;
use GuzzleHttp\Exception\ConnectException;

class EchoClientService extends Service
{

    /**
     * @return AbstractConnector
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function connector(): AbstractConnector
    {
        return app()->make(AbstractConnector::class);
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function getSheetClients($slug)
    {
       return \Cache::remember('echo_subject_sheet_clients', 2, function () use ($slug){
           try {
               return $this->getSheetsFromResponse($this->response(), $slug)
                   ->subscription_count;
           } catch (ConnectException $e) {
               return '--';
           }
       });
    }

    public function getQuestionClients($slug)
    {
        return \Cache::remember('echo_subject_question_clients', 2, function () use ($slug){
            try {
                return $this->getSheetsFromResponse($this->response(), $slug)
                    ->subscription_count;
            } catch (ConnectException $e) {
                return '--';
            }
        });
    }

    private function getSheetsFromResponse($response, $subject)
    {
        $channel = $response->channels;

        if(property_exists($channel, $property = 'presence-subject.' . $subject . '.sheet')) {
            return $channel->$property;
        }

        return $this->fake();
    }

    private function getQuestionsFromResponse($response, $subject)
    {
        $channel = $response->channels;

        if(property_exists($channel, $property = 'presence-subject.' . $subject . '.question')) {
            return $channel->$property;
        }

        return $this->fake();
    }

    public function fake()
    {
        return new class {
            public $subscription_count = 0;
            public $occupied = false;
        };
    }

    /**
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function response()
    {
        return json_decode(
            $this->connector()->request()->getBody()
        );
    }

}