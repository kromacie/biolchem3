<?php namespace App\Services;

use App\Actions\Create;
use App\Actions\Delete;
use App\Actions\Show;

use App\Models\Image;
use App\Repositories\ImageRepository;
use App\Validators\Admin\Image\CreateValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Storage;

class ImageService extends Service
{
    /**
     * @param $data
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function adminCreate($data)
    {
        $data = CreateValidator::make()->validate($data);

        $path = $this->storage()->put('images', $data['image']);

        return $this->images()->perform(new Create([
            'path' => $path
        ]));
    }

    /**
     * @param $id
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminDelete($id)
    {
        $image = $this->validateId($id);

        $this->storage()->delete($image->path);

        return $this->images()->perform(new Delete($id));
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function validateId($id): Image
    {
        $model = $this->images()->perform(new Show($id));

        if(!($model instanceof Image)) {
            throw new ModelNotFoundException("Dane pytanie nie istnieje!");
        }

        return $model;
    }



    /**
     * @return ImageRepository
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function images(): ImageRepository
    {
        return app()->make(ImageRepository::class);
    }

    public function storage()
    {
        return Storage::disk('public');
    }
}