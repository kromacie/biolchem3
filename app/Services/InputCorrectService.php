<?php namespace App\Services;

use App\Actions\Create;
use App\Actions\Delete;
use App\Actions\Show;
use App\Models\InputCorrect;
use App\Repositories\InputCorrectRepository;
use App\Validators\Admin\Input\Correct\CreateValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class InputCorrectService extends Service
{
    /**
     * @param $data
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function adminCreate($data)
    {
        return $this->corrects()->perform(
            new Create(CreateValidator::make()->validate($data))
        );
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminDelete($id)
    {
        $this->validateId($id);

        return $this->corrects()->perform(new Delete($id));
    }

    /**
     * @param $id
     * @return InputCorrect
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function validateId($id) : InputCorrect
    {
        $model = $this->corrects()->perform(new Show($id));

        if(!($model instanceof InputCorrect)) {
            throw new ModelNotFoundException("Dane pytanie podrzędne nie istnieje!");
        }

        return $model;
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function corrects(): InputCorrectRepository
    {
        return app()->make(InputCorrectRepository::class);
    }
}