<?php namespace App\Services;

use App\Actions\Create;
use App\Actions\Delete;
use App\Actions\Show;
use App\Actions\Update;
use App\Models\InputGroup;
use App\Repositories\InputGroupRepository;
use App\Validators\Admin\Input\Group\CreateValidator;
use App\Validators\Admin\Input\Group\UpdateValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class InputGroupService extends Service
{

    /**
     * @param $data
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function adminCreate($data)
    {
        return $this->inputGroups()->perform(
            new Create(CreateValidator::make()->validate($data))
        );
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminDelete($id)
    {
        $this->validateId($id);

        return $this->inputGroups()->perform(new Delete($id));
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function adminUpdate($id, $data)
    {
        $this->validateId($id);

        return $this->inputGroups()->perform(
            new Update($id, UpdateValidator::make()->validate($data))
        );
    }

    /**
     * @return InputGroupRepository
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function inputGroups(): InputGroupRepository
    {
        return app()->make(InputGroupRepository::class);
    }

    /**
     * @param $id
     * @return InputGroup
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminGet($id)
    {
        return $this->validateId($id);
    }

    /**
     * @param $id
     * @return InputGroup
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function validateId($id) : InputGroup
    {
        $model = $this->inputGroups()->perform(new Show($id));

        if(!($model instanceof InputGroup)) {
            throw new ModelNotFoundException("Dana grupa nie istnieje!");
        }

        return $model;
    }
}