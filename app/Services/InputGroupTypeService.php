<?php namespace App\Services;

use App\Actions\Create;
use App\Actions\Delete;
use App\Actions\Show;
use App\Actions\ShowAll;
use App\Actions\Update;
use App\Models\InputGroupType;
use App\Repositories\InputGroupTypeRepository;
use App\Validators\Admin\Input\Group\Type\CreateValidator;
use App\Validators\Admin\Input\Group\Type\UpdateValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class InputGroupTypeService extends Service
{

    /**
     * @param $id
     * @param $data
     * @return mixed
     * @throws \Throwable
     */
    public function adminUpdate($id, $data)
    {
        return \DB::transaction(function () use ($id, $data){
            $this->validateId($id);

            return $this->types()->perform(new Update(
                $id, UpdateValidator::make()->except($id)->validate($data)
            ));
        });
    }

    /**
     * @param $data
     * @return mixed
     * @throws \Throwable
     */
    public function adminCreate($data)
    {
        return \DB::transaction(function () use ($data){
            return $this->types()->perform(new Create(
                CreateValidator::make()->validate($data)
            ));
        });
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Throwable
     */
    public function adminDelete($id)
    {
        return \DB::transaction(function () use ($id){
            $this->validateId($id);

            return $this->types()->perform(new Delete($id));
        });
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminAll()
    {
        return $this->types()->perform(new ShowAll());
    }

    /**
     * @param $id
     * @return InputGroupType
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminShow($id)
    {
        return $this->validateId($id);
    }

    /**
     * @param $id
     * @return InputGroupType
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function validateId($id) : InputGroupType
    {
        $model = $this->types()->perform(new Show($id));

        if(!($model instanceof InputGroupType)) {
            throw new ModelNotFoundException("Dany typ grupy inputa nie istnieje!");
        }

        return $model;
    }
    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function types(): InputGroupTypeRepository
    {
        return app()->make(InputGroupTypeRepository::class);
    }
}