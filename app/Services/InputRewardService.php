<?php namespace App\Services;

use App\Actions\Create;
use App\Actions\Inputs\Rewards\Delete;
use App\Actions\Inputs\Rewards\Show;
use App\Models\RewardHasInput;
use App\Repositories\InputRewardRepository;
use App\Validators\Admin\Input\Reward\CreateValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class InputRewardService extends Service
{
    /**
     * @param $data
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function adminCreate($data)
    {
        return $this->rewards()->perform(
            new Create(CreateValidator::make()->validate($data))
        );
    }

    /**
     * @param $reward_id
     * @param $input_id
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminDelete($reward_id, $input_id)
    {
        $data = [
            'reward_id' => $reward_id,
            'input_id' => $input_id
        ];
        $this->validateId($data);

        return $this->rewards()->perform(new Delete($data));
    }

    /**
     * @param $id
     * @return RewardHasInput
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function validateId($id) : RewardHasInput
    {
        $model = $this->rewards()->perform(new Show($id));

        if(!($model instanceof RewardHasInput)) {
            throw new ModelNotFoundException("Dane pytanie podrzędne nie istnieje!");
        }

        return $model;
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function rewards(): InputRewardRepository
    {
        return app()->make(InputRewardRepository::class);
    }
}