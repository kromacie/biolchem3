<?php namespace App\Services;

use App\Actions\Create;
use App\Actions\Delete;
use App\Actions\Show;
use App\Actions\Update;
use App\Models\Input;
use App\Repositories\InputRepository;
use App\Validators\Admin\Input\CreateValidator;
use App\Validators\Admin\Input\UpdateValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class InputService extends Service
{
    /**
     * @param $data
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function adminCreate($data)
    {
        $data = CreateValidator::make()->validate($data);

        $data = $this->modifyData($data);

        return $this->inputs()->perform(new Create($data));
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function adminUpdate($id, $data)
    {
        $this->validateId($id);

        $data = UpdateValidator::make()->validate($data);

        $data = $this->modifyData($data);

        return $this->inputs()->perform(new Update($id, $data));
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminDelete($id)
    {
        $this->validateId($id);

        return $this->inputs()->perform(new Delete($id));
    }

    public function modifyData($data)
    {
        $data['disabled'] = isset($data['disabled']);
        $data['checked'] = isset($data['checked']);
        $data['is_description'] = isset($data['is_description']);

        return $data;
    }

    /**
     * @param $id
     * @return Input
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function validateId($id) : Input
    {
        $model = $this->inputs()->perform(new Show($id));

        if(!($model instanceof Input)) {
            throw new ModelNotFoundException("Dane pytanie nie istnieje!");
        }

        return $model;
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function inputs(): InputRepository
    {
        return app()->make(InputRepository::class);
    }
}