<?php namespace App\Services;

use App\Actions\Create;
use App\Actions\Delete;
use App\Actions\Show;
use App\Actions\ShowAll;
use App\Actions\Update;
use App\Models\InputType;
use App\Repositories\InputTypeRepository;
use App\Validators\Admin\Input\Type\CreateValidator;
use App\Validators\Admin\Input\Type\UpdateValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class InputTypeService extends Service
{

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminAll()
    {
        return $this->types()->perform(new ShowAll());
    }

    /**
     * @param $id
     * @return InputType
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminShow($id)
    {
        return $this->validateId($id);
    }

    /**
     * @param $data
     * @return mixed
     * @throws \Throwable
     */
    public function adminCreate($data)
    {
        return \DB::transaction(function () use ($data){
            return $this->types()->perform(new Create(
                CreateValidator::make()->validate($data)
            ));
        });
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     * @throws \Throwable
     */
    public function adminUpdate($id, $data)
    {
        return \DB::transaction(function () use ($id, $data) {
            $this->validateId($id);

            return $this->types()->perform(new Update(
                $id, UpdateValidator::make()->except($id)->validate($data)
            ));
        });
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Throwable
     */
    public function adminDelete($id)
    {
        return \DB::transaction(function () use ($id) {
            $this->validateId($id);

            return $this->types()->perform(new Delete($id));
        });
    }

    /**
     * @param $id
     * @return InputType
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function validateId($id) : InputType
    {
        $model = $this->types()->perform(new Show($id));

        if(!($model instanceof InputType)) {
            throw new ModelNotFoundException("Dany typ inputa nie istnieje!");
        }

        return $model;
    }

    /**
     * @return InputTypeRepository
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function types(): InputTypeRepository
    {
        return app()->make(InputTypeRepository::class);
    }
}