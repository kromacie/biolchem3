<?php namespace App\Services;

use App\Actions\Create;
use App\Repositories\QuestionContentRepository;
use App\Validators\Admin\Question\Contents\CreateValidator;

class QuestionContentService extends Service
{

    /**
     * @param $question_id
     * @param $data
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function adminCreate($question_id, $data)
    {
        $content_id = $this->contents()->adminCreate($data)->id;

        return $this->questionContents()->perform(
            new Create(CreateValidator::make()->validate([
                'content_id' => $content_id,
                'question_id' => $question_id
            ]))
        );
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function adminUpdate($id, $data)
    {
        return $this->contents()->adminUpdate($id, $data);
    }

    /**
     * @return QuestionContentRepository
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function questionContents(): QuestionContentRepository
    {
        return app()->make(QuestionContentRepository::class);
    }

    /**
     * @return ContentService
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function contents(): ContentService
    {
        return app()->make(ContentService::class);
    }

}