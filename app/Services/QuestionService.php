<?php namespace App\Services;

use App\Actions\Create;
use App\Actions\Delete;
use App\Actions\Questions\Paginate;
use App\Actions\Questions\ShowAll;
use App\Actions\Questions\ShowCount;
use App\Actions\Show;
use App\Actions\Update;

use App\Jobs\CheckQuestion;
use App\Models\Question;
use App\Repositories\QuestionRepository;
use App\Validators\Admin\Question\CheckValidator;
use App\Validators\Admin\Question\CreateValidator;
use App\Validators\Admin\Question\UpdateValidator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class QuestionService extends Service
{

    /**
     * @param int $page
     * @param null $subject_id
     * @param bool $enabled
     * @param array $months
     * @param array $years
     * @param array $types
     * @param array $versions
     * @param array $tiers
     * @param array $popularity
     * @param array $difficulty
     * @param array $categories
     * @param array $tags
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function paginate($page = 1, $subject_id = null, $enabled = true, $months = [], $years = [], $types = [], $versions = [], $tiers = [], $popularity = [], $difficulty = [], $categories = [], $tags = [] )
    {
        return $this->questions()->perform(
            (new Paginate())
                ->setPage($page)
                ->setSubjectId($subject_id)
                ->setEnabled($enabled)
                ->setYears($years)
                ->setMonths($months)
                ->setCategories($categories)
                ->setTags($tags)
                ->setTypes($types)
                ->setVersions($versions)
                ->setTiers($tiers)
                ->setPopularity($popularity)
                ->setDifficulty($difficulty)
        );
    }

    /**
     * @param $slug
     * @param null $enabled
     * @return Question
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function show($slug, $enabled = null)
    {
        return $this->validateModel(
            $this->questions()->perform(
                (new \App\Actions\Questions\Show())
                    ->setSlug($slug)
                    ->setEnabled($enabled)
            )
        );
    }

    /**
     * @param array $filters
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminAll($filters = [])
    {
        return $this->questions()->perform(new ShowAll());
    }

    /**
     * @param $data
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function adminCreate($data)
    {
        $data = CreateValidator::make()->validate($data);

        if (isset($data['enabled'])) {
            $data['enabled'] = true;
        }

        return $this->questions()->perform(
            new Create($data)
        );
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminDelete($id)
    {
        $this->validateId($id);

        return $this->questions()->perform(
            new Delete($id)
        );
    }

    /**
     * @param $id
     * @param $data
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function adminUpdate($id, $data)
    {
        $this->validateId($id);

        $data = UpdateValidator::make()->validate($data);

        $data['enabled'] = isset($data['enabled']);

        return $this->questions()->perform(new Update(
            $id, $data
        ));
    }

    /**
     * @param $id
     * @return Model
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminShow($id)
    {
        return $this->validateId($id);
    }

    /**
     * @param $sheet_id
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminAllBySheet($sheet_id)
    {
        return $this->questions()->perform((new ShowAll())->setSheetId($sheet_id));
    }

    /**
     * @param bool $subject_id
     * @param bool $enabled
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function getCount($subject_id = false, $enabled = true)
    {
        return $this->questions()->perform(
            (new ShowCount())
                ->setEnabled($enabled)
                ->setSubjectId($subject_id)
        );
    }

    /**
     * @param $question
     * @param $uuid
     * @param $answers
     * @throws \Illuminate\Validation\ValidationException
     */
    public function startChecking($question, $uuid, $answers)
    {
        CheckValidator::make()->validate([
            'answers' => $answers,
            'question_id' => $question,
            'user_id' => $uuid
        ]);

        dispatch(new CheckQuestion($question, $uuid, $answers));
    }

    /**
     * @param $id
     * @return Question
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function validateId($id) : Question
    {
        $model = $this->questions()->perform(new Show($id));

        $this->validateModel($model);

        return $model;
    }

    public function validateModel($model): Question
    {
        if(!($model instanceof Question)) {
            throw new ModelNotFoundException("Dane pytanie nie istnieje!");
        }

        return $model;
    }

    /**
     * @return QuestionRepository
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function questions(): QuestionRepository {
        return app()->make(QuestionRepository::class);
    }
}