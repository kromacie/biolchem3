<?php namespace App\Services;

use App\Actions\Create;
use App\Repositories\QuestionSubstituteContentRepository;
use App\Validators\Admin\Question\Substitutes\Contents\CreateValidator;

class QuestionSubstituteContentService extends Service
{
    /**
     * @param $substitute_id
     * @param $data
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function adminCreate($substitute_id, $data)
    {
        $content = $this->contents()->adminCreate($data);

        $data['content_id'] = $content->id;
        $data['question_substitute_id'] = $substitute_id;

        return $this->substituteContents()->perform(
            new Create(CreateValidator::make()->validate($data))
        );
    }

    /**
     * @param $content_id
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminDelete($content_id)
    {
        return $this->contents()->adminDelete($content_id);
    }

    /**
     * @param $content_id
     * @param $data
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function adminUpdate($content_id, $data)
    {
        return $this->contents()->adminUpdate($content_id, $data);
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function contents(): ContentService
    {
        return app()->make(ContentService::class);
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function substituteContents(): QuestionSubstituteContentRepository
    {
        return app()->make(QuestionSubstituteContentRepository::class);
    }
}