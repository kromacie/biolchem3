<?php namespace App\Services;

use App\Actions\Create;
use App\Repositories\QuestionSubstituteInputGroupRepository;
use App\Validators\Admin\Question\Substitutes\Inputs\Groups\CreateValidator;

class QuestionSubstituteInputGroupService extends Service
{
    /**
     * @param $substitute_id
     * @param $data
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function adminCreate($substitute_id, $data)
    {
        $group = $this->inputGroups()->adminCreate($data);

        $data['input_group_id'] = $group->id;
        $data['question_substitute_id'] = $substitute_id;

        $this->inputGroupSubstitutes()->perform(
            new Create(CreateValidator::make()->validate($data))
        );
    }

    /**
     * @param $input_group_id
     * @param $data
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function adminUpdate($input_group_id, $data)
    {
        return $this->inputGroups()->adminUpdate($input_group_id, $data);
    }

    /**
     * @param $group_id
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminDelete($group_id)
    {
        return $this->inputGroups()->adminDelete($group_id);
    }
    /**
     * @return InputGroupService
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function inputGroups(): InputGroupService
    {
        return app()->make(InputGroupService::class);
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function inputGroupSubstitutes(): QuestionSubstituteInputGroupRepository
    {
        return app()->make(QuestionSubstituteInputGroupRepository::class);
    }
}