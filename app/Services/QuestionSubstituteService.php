<?php namespace App\Services;

use App\Actions\Create;
use App\Actions\Delete;
use App\Actions\Show;

use App\Models\QuestionSubstitute;
use App\Repositories\QuestionSubstituteRepository;
use App\Validators\Admin\Question\Substitutes\CreateValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class QuestionSubstituteService extends Service
{
    /**
     * @param $question_id
     * @param $data
     * @return QuestionSubstitute
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function adminCreate($question_id, $data): QuestionSubstitute
    {
        $data['question_id'] = $question_id;

        return $this->substitutes()->perform(
            new Create(CreateValidator::make()->validate($data))
        );
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminDelete($id)
    {
        $this->validateId($id);

        return $this->substitutes()->perform(new Delete($id));
    }

    /**
     * @param $id
     * @return QuestionSubstitute
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function validateId($id) : QuestionSubstitute
    {
        $model = $this->substitutes()->perform(new Show($id));

        if(!($model instanceof QuestionSubstitute)) {
            throw new ModelNotFoundException("Dane pytanie podrzędne nie istnieje!");
        }

        return $model;
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function substitutes(): QuestionSubstituteRepository
    {
        return app()->make(QuestionSubstituteRepository::class);
    }
}