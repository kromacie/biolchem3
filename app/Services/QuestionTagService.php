<?php namespace App\Services;

use App\Actions\Create;
use App\Actions\Questions\Tags\Delete;
use App\Actions\Questions\Tags\Show;
use App\Models\QuestionHasTag;
use App\Repositories\QuestionTagRepository;
use App\Validators\Admin\Question\Tags\CreateValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Fluent;

class QuestionTagService extends Service
{
    public function adminCreate($data)
    {
        return DB::transaction(function () use ($data){

            $data = new Fluent(CreateValidator::make()->validate($data));

            $tag = $this->tags()->adminShow($data->get('tag_id'));

            if($tag->tag_id && !$this->adminHasTag($data->get('question_id'), $tag->tag_id)) {
                $this->adminCreate([
                    'tag_id' => $tag->tag_id,
                    'question_id' => $data->get('question_id')
                ]);
            }

            return $this->questionTags()->perform(new Create(
                $data->toArray()
            ));
        });
    }

    /**
     * @param $question_id
     * @param $tag_id
     * @return bool
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminHasTag($question_id, $tag_id)
    {
        try {
            $this->validateId($question_id, $tag_id);
        } catch (ModelNotFoundException $e) {
            return false;
        }
        return true;
    }

    public function adminDelete($question_id, $tag_id)
    {
        return DB::transaction(function () use ($question_id, $tag_id){
            $this->validateId($question_id, $tag_id);

            return $this->questionTags()->perform(
                new Delete($question_id, $tag_id)
            );
        });
    }

    /**
     * @param $question_id
     * @param $tag_id
     * @return QuestionHasTag
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function validateId($question_id, $tag_id) : QuestionHasTag
    {
        $model = $this->questionTags()->perform(new Show($question_id, $tag_id));

        if(!($model instanceof QuestionHasTag)) {
            throw new ModelNotFoundException("Dana kategoria nie istnieje!");
        }

        return $model;
    }

    /**
     * @return QuestionTagRepository
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function questionTags(): QuestionTagRepository
    {
        return app()->make(QuestionTagRepository::class);
    }

    /**
     * @return TagService
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function tags(): TagService
    {
        return app()->make(TagService::class);
    }
}