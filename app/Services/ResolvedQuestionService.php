<?php namespace App\Services;

use App\Actions\Create;
use App\Actions\Resolved\Questions\ShowCount;
use App\Repositories\ResolvedQuestionRepository;
use App\Validators\Admin\Result\Question\CreateValidator;

class ResolvedQuestionService extends Service
{
    /**
     * @param $data
     * @return mixed
     * @throws \Throwable
     */
    public function create($data)
    {
        return \DB::transaction(function () use ($data){
            return $this->questions()->perform(
                new Create(CreateValidator::make()->validate($data))
            );
        });
    }

    /**
     * @param $subject_id
     * @return mixed
     * @throws \Throwable
     */
    public function countBySubject($subject_id)
    {
        return \DB::transaction(function () use ($subject_id) {
            return $this->questions()->perform(
                (new ShowCount())->setSubjectId($subject_id)
            );
        });
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function questions(): ResolvedQuestionRepository
    {
        return app()->make(ResolvedQuestionRepository::class);
    }
}