<?php namespace App\Services;

use App\Actions\Create;
use App\Actions\Resolved\Sheets\ShowCount;
use App\Repositories\ResolvedSheetRepository;
use App\Validators\Admin\Result\Sheet\CreateValidator;

class ResolvedSheetService extends Service
{

    /**
     * @param $data
     * @return mixed
     * @throws \Throwable
     */
    public function create($data)
    {
        return \DB::transaction(function () use ($data) {
            return $this->sheets()->perform(
                new Create(CreateValidator::make()->validate($data))
            );
        });
    }

    /**
     * @param $subject_id
     * @return mixed
     * @throws \Throwable
     */
    public function countBySubject($subject_id)
    {
        return \DB::transaction(function () use ($subject_id) {
            return $this->sheets()->perform(
                (new ShowCount())->setSubjectId($subject_id)
            );
        });
    }
    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function sheets(): ResolvedSheetRepository
    {
        return app()->make(ResolvedSheetRepository::class);
    }
}