<?php namespace App\Services;

use App\Actions\Create;
use App\Actions\Delete;
use App\Actions\Show;
use App\Models\Reward;
use App\Repositories\RewardRepository;
use App\Validators\Admin\Reward\CreateValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RewardService extends Service
{
    /**
     * @param $data
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function adminCreate($data)
    {
        return $this->rewards()->perform(
            new Create(CreateValidator::make()->validate($data))
        );
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminDelete($id)
    {
        $this->validateId($id);

        return $this->rewards()->perform(new Delete($id));
    }

    /**
     * @param $id
     * @return Reward
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function validateId($id) : Reward
    {
        $model = $this->rewards()->perform(new Show($id));

        if(!($model instanceof Reward)) {
            throw new ModelNotFoundException("Dane pytanie podrzędne nie istnieje!");
        }

        return $model;
    }

    /**
     * @return RewardRepository
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function rewards(): RewardRepository
    {
        return app()->make(RewardRepository::class);
    }
}