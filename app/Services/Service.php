<?php namespace App\Services;

use Illuminate\Http\Request;

abstract class Service
{
    protected $request;

    public function __construct(Request $request = null)
    {
        $this->request = $request;
    }
}