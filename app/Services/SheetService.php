<?php namespace App\Services;

use App\Actions\Create;
use App\Actions\Delete;
use App\Actions\Sheets\Paginate;
use App\Actions\Sheets\ShowBySlug;
use App\Actions\Sheets\ShowCount;
use App\Actions\Sheets\ShowMonths;
use App\Actions\Sheets\ShowYears;
use App\Actions\Sheets\YearExists;
use App\Actions\Show;
use App\Actions\Sheets\ShowAll;
use App\Actions\Update;
use App\Jobs\CheckSheet;
use App\Models\Sheet;
use App\Repositories\SheetRepository;
use App\Validators\Admin\Sheet\CheckValidator;
use App\Validators\Admin\Sheet\CreateValidator;
use App\Validators\Admin\Sheet\SearchValidator;
use App\Validators\Admin\Sheet\ShowAllValidator;
use App\Validators\Admin\Sheet\UpdateValidator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use DB;
use Illuminate\Support\Fluent;

class SheetService extends Service
{
    /**
     * @param array $data
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function adminAll($data = []): Collection
    {
        $data = new Fluent(ShowAllValidator::make()->validate($data));

        return $this->sheets()->perform(
            (new ShowAll())
                ->setSubject($data->get('subject_id', null))
        );
    }

    /**
     * @param $data
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function adminCreate($data)
    {
        $data = CreateValidator::make()->validate($data);

        return $this->sheets()->perform(new Create($data));
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     * @throws \Throwable
     */
    public function adminUpdate($id, $data)
    {
        return DB::transaction(function () use ($id, $data){
            $this->validateId($id);

            $this->sheets()->perform(new Update(
                $id, UpdateValidator::make()->validate($data)
            ));
        });
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Throwable
     */
    public function adminDelete($id)
    {
        return DB::transaction(function () use ($id){
            $this->validateId($id);

            return $this->sheets()->perform(new Delete($id));
        });
    }

    /**
     * @param $slug
     * @param bool $enabled
     * @param bool $with_questions
     * @param bool $with_substitutes
     * @param bool $with_groups
     * @param bool $with_inputs
     * @return Sheet
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function show($slug, $enabled = true, $with_questions = false, $with_substitutes = false, $with_groups = false, $with_inputs = false)
    {
        return $this->validateModel(
            $this->sheets()->perform(
                (new ShowBySlug($slug))->setEnabled($enabled)
                                       ->setWithQuestions($with_questions)
                                       ->setWithSubstitutes($with_substitutes)
                                       ->setWithGroups($with_groups)
                                       ->setWithInputs($with_inputs)
            )
        );
    }

    /**
     * @param null $subject
     * @param bool $enabled
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function all($subject = null, $enabled = true)
    {
        return $this->sheets()->perform(
            (new ShowAll())->setEnabled($enabled)
                           ->setSubject($subject)
        );
    }

    /**
     * @param int $page
     * @param null $subject
     * @param bool $enabled
     * @param array $months
     * @param array $years
     * @param array $types
     * @param array $versions
     * @param array $tiers
     * @param null $popularity
     * @param null $difficulty
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function paginate($page = 1, $subject = null, $enabled = true, $months = [], $years = [], $types = [], $versions = [], $tiers = [], $popularity = null, $difficulty = null)
    {
        $data = SearchValidator::make()->validate([
            'months' => $months,
            'years' => $years,
            'types' => $types,
            'tiers' => $tiers,
            'versions' => $versions,
            'page' => $page,
        ]);

        return $this->sheets()->perform(
            (new Paginate())->setSubject($subject)
                            ->setEnabled($enabled)
                            ->setMonths($data['months'])
                            ->setTypes($types)
                            ->setVersions($versions)
                            ->setYears($years)
                            ->setTiers($tiers)
                            ->setPage($page)
                            ->setPopularity($popularity)
                            ->setDifficulty($difficulty)

        );
    }

    /**
     * @param null $subject_id
     * @param bool $enabled
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function showYears($subject_id = null, $enabled = true): Collection
    {
        return $this->sheets()->perform(
            (new ShowYears())->setSubjectId($subject_id)
                             ->setEnabled($enabled)
        );
    }

    /**
     * @param null $subject_id
     * @param bool $enabled
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function getCount($subject_id = null, $enabled = true)
    {
        return $this->sheets()->perform(
            (new ShowCount())->setEnabled($enabled)
                             ->setSubject($subject_id)
        );
    }

    /**
     * @param null $subject_id
     * @param bool $enabled
     * @return Collection
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function showMonths($subject_id = null, $enabled = true): Collection
    {
        return $this->sheets()->perform(
            (new ShowMonths())->setSubjectId($subject_id)
                              ->setEnabled($enabled)
        );
    }

    /**
     * @param $id
     * @return Sheet
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminShow($id)
    {
        return $this->validateId($id);
    }

    /**
     * @param $id
     * @return Sheet
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function validateId($id) : Sheet
    {
        $model = $this->sheets()->perform(new Show($id));

        return $this->validateModel($model);
    }

    public function validateModel($model)
    {
        if(!($model instanceof Sheet)) {
            throw new ModelNotFoundException("Dana kategoria nie istnieje!");
        }
        return $model;
    }

    /**
     * @param $year
     * @return bool
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function yearExists($year) : bool
    {
        return $this->sheets()->perform(new YearExists($year));
    }

    public function startChecking($sheet, $uuid, $answers)
    {
        CheckValidator::make()->validate([
            'answers' => $answers,
            'sheet_id' => $sheet,
            'user_id' => $uuid
        ]);

        dispatch(new CheckSheet($sheet, $uuid, $answers));
    }

    /**
     * @return SheetRepository
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function sheets(): SheetRepository
    {
        return app()->make(SheetRepository::class);
    }
}