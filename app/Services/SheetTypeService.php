<?php namespace App\Services;

use App\Actions\Create;
use App\Actions\Delete;
use App\Actions\Show;
use App\Actions\ShowAll;
use App\Actions\Update;
use App\Models\SheetTier;
use App\Models\SheetType;
use App\Repositories\SheetTypeRepository;
use App\Validators\Admin\Sheet\Type\CreateValidator;
use App\Validators\Admin\Sheet\Type\UpdateValidator;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SheetTypeService extends Service
{
    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminAll()
    {
        return $this->types()->perform(new ShowAll());
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function all()
    {
        return $this->adminAll();
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminShow($id)
    {
        return $this->validateId($id);
    }

    /**
     * @param $data
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function adminCreate($data)
    {
        return $this->types()->perform(new Create(
            CreateValidator::make()->validate($data)
        ));
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     * @throws \Throwable
     */
    public function adminUpdate($id, $data)
    {
        return DB::transaction(function () use ($id, $data){
            $this->validateId($id);

            return $this->types()->perform(new Update(
                $id, UpdateValidator::make()->validate($data)
            ));
        });
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Throwable
     */
    public function adminDelete($id)
    {
        return DB::transaction(function () use ($id){
            $this->validateId($id);

            return $this->types()->perform(new Delete($id));
        });
    }

    public function validateModel($model)
    {
        if(!($model instanceof SheetType)) {
            throw new ModelNotFoundException("Dany typ arkusza nie istnieje");
        }
        return $model;
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function validateId($id)
    {
        return $this->validateModel(
            $this->types()->perform(new Show($id))
        );
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function types(): SheetTypeRepository
    {
        return app()->make(SheetTypeRepository::class);
    }
}