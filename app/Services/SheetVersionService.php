<?php namespace App\Services;

use App\Actions\Create;
use App\Actions\Delete;
use App\Actions\Show;
use App\Actions\ShowAll;
use App\Actions\Update;
use App\Models\SheetVersion;
use App\Repositories\SheetVersionRepository;
use App\Validators\Admin\Sheet\Version\CreateValidator;
use App\Validators\Admin\Sheet\Version\UpdateValidator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use DB;

class SheetVersionService extends Service
{

    /**
     * @param $data
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function adminCreate($data)
    {
        return $this->types()->perform(
            new Create(CreateValidator::make()->validate($data))
        );
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     * @throws \Throwable
     */
    public function adminUpdate($id, $data)
    {
        return DB::transaction(function () use ($id, $data){
            $this->validateId($id);

            return $this->types()->perform(
                new Update($id, UpdateValidator::make()->except($id)->validate($data))
            );
        });
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminAll(): Collection
    {
        return $this->types()->perform(new ShowAll());
    }

    /**
     * @return Collection|mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function all()
    {
        return $this->adminAll();
    }

    /**
     * @param $id
     * @return SheetVersion
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminShow($id): SheetVersion
    {
        return $this->validateId($id);
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Throwable
     */
    public function adminDelete($id)
    {
        return DB::transaction(function () use ($id){
           $this->validateId($id);

           return $this->types()->perform(new Delete($id));
        });
    }

    /**
     * @param $id
     * @return SheetVersion
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function validateId($id) : SheetVersion
    {
        $model = $this->types()->perform(new Show($id));

        if(!($model instanceof SheetVersion)) {
            throw new ModelNotFoundException("Dana wersja matury nie istnieje!");
        }

        return $model;
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function types(): SheetVersionRepository
    {
        return app()->make(SheetVersionRepository::class);
    }
}