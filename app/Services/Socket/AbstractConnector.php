<?php namespace App\Services\Socket;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

abstract class AbstractConnector implements ConnectorInterface
{
    /**
     * @return Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request(): Response
    {
        return $this->client()->send(
            new Request('GET', $this->host() . '/apps/Biolchem/channels?auth_key=' . $this->getAuth())
        );
    }

    public function getAuth()
    {
        return config('app.echo_auth');
    }

    public function host(): String
    {
        return config('app.url') . ':' . config('app.echo_port');
    }

    public function client()
    {
        return new Client([
            'timeout' => 1
        ]);
    }
}
