<?php namespace App\Services\Socket;

use GuzzleHttp\Psr7\Response;

interface ConnectorInterface
{
    public function request(): Response;
    public function host(): String;
}