<?php namespace App\Services\Socket;

class SecuredConnector extends AbstractConnector
{

    public function protocol(): String
    {
        return "https";
    }
}