<?php namespace App\Services;

use App\Actions\Create;
use App\Actions\Show;
use App\Actions\Update;
use App\Models\SubjectDescription;
use App\Repositories\SubjectDescriptionRepository;
use App\Validators\Admin\Subject\Description\CreateValidator;
use App\Validators\Admin\Subject\Description\UpdateValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SubjectDescriptionService extends Service
{
    /**
     * @param $data
     * @return mixed
     * @throws \Throwable
     */
    public function adminCreate($data)
    {
        return \DB::transaction(function () use ($data){
            return $this->descriptions()->perform(
                new Create(CreateValidator::make()->validate($data))
            );
        });
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     * @throws \Throwable
     */
    public function adminUpdate($id, $data)
    {
        return \DB::transaction(function () use ($id, $data){
            $this->validateId($id);

            return $this->descriptions()->perform(
                new Update($id, UpdateValidator::make()->validate($data))
            );
        });
    }

    /**
     * @param $id
     * @return SubjectDescription
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function validateId($id) : SubjectDescription
    {
        $model = $this->descriptions()->perform(new Show($id));

        if(!($model instanceof SubjectDescription)) {
            throw new ModelNotFoundException("Dany opis przedmiotu nie istnieje!");
        }

        return $model;
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function descriptions(): SubjectDescriptionRepository
    {
        return app()->make(SubjectDescriptionRepository::class);
    }
}