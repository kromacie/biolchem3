<?php namespace App\Services;

use App\Actions\Create;
use App\Actions\Delete;
use App\Actions\Show;
use App\Actions\Subjects\ShowAll;
use App\Actions\Subjects\ShowBySlug;
use App\Actions\Subjects\ShowStatistics;
use App\Actions\Update;
use App\Models\Subject;
use App\Models\SubjectDescription;
use App\Repositories\SubjectRepository;
use App\Validators\Admin\Subject\CreateValidator;
use App\Validators\Admin\Subject\UpdateValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

class SubjectService extends Service
{
    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminAll()
    {
        return $this->subjects()->perform(new ShowAll());
    }

    public function search($enabled = true)
    {
        return $this->subjects()->perform(
            (new ShowAll())->setEnabled($enabled)
        );
    }

    /**
     * @param $slug
     * @param bool $enabled
     * @return Subject
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function show($slug, $enabled = true)
    {
        return $this->validateModel(
            $this->subjects()->perform(
                (new ShowBySlug($slug))->setEnabled($enabled)
            )
        );
    }

    /**
     * @param $data
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function adminCreate($data)
    {
        return $this->subjects()->perform(
            new Create(CreateValidator::make()->validate($data))
        );
    }

    /**
     * @param $id
     * @return Subject
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminShow($id)
    {
        return $this->validateId($id);
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     * @throws \Throwable
     */
    public function adminUpdate($id, $data)
    {
        return DB::transaction(function () use ($id, $data) {
            $this->validateId($id);

            return $this->subjects()->perform(
                new Update($id, UpdateValidator::make()->except($id)->validate($data))
            );
        });
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Throwable
     */
    public function adminDelete($id)
    {
        return DB::transaction(function () use ($id) {
            $this->validateId($id);

            return $this->subjects()->perform(new Delete($id));
        });
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function getStatistics()
    {
        return $this->subjects()->perform(new ShowStatistics());
    }

    /**
     * @param $id
     * @return Subject
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function validateId($id) : Subject
    {
        $model = $this->subjects()->perform(new Show($id));

        $this->validateModel($model);

        return $model;
    }

    public function validateModel($model)
    {
        if(!($model instanceof Subject)) {
            throw new ModelNotFoundException("Dana kategoria nie istnieje!");
        }
        return $model;
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function subjects(): SubjectRepository
    {
        return app()->make(SubjectRepository::class);
    }
}