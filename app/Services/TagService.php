<?php namespace App\Services;

use App\Actions\Create;
use App\Actions\Delete;
use App\Actions\Show;
use App\Actions\Tags\ShowAll;
use App\Actions\Update;
use App\Models\Tag;
use App\Repositories\TagRepository;
use App\Validators\Admin\Tag\CreateValidator;
use App\Validators\Admin\Tag\UpdateValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TagService extends Service
{
    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminAll()
    {
        return $this->tags()->perform(new ShowAll());
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminAllWithoutTag()
    {
        return $this->tags()->perform(
            (new ShowAll())->setWithoutTag(true)
        );
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminAllHasTags()
    {
        return $this->tags()->perform(
            (new ShowAll())->setHasTags(true)
        );
    }

    /**
     * @param $question_id
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminAllDoesntHaveQuestionAndTagsForSubject($question_id, $subject_id)
    {
        //to change func definition
        return $this->tags()->perform(
            (new ShowAll())
                ->setDoesntHaveQuestion($question_id)
                ->setWithoutTags(true)
                ->setWhereSubject($subject_id)
        );
    }

    /**
     * @param $tag_id
     * @param $subject_id
     * @param null $without_tag
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminSearchTagTags($tag_id = null, $subject_id = null, $without_tag = null)
    {
        return $this->tags()->perform(
            (new ShowAll())
                ->setHasTag($tag_id)
                ->setWhereSubject($subject_id)
                ->setWithoutTag($without_tag)
        );
    }

    /**
     * @param null $subject_id
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function getAllTagsLevelTop($subject_id = null)
    {
        return $this->tags()->perform(
            (new ShowAll())
                ->setWhereSubject($subject_id)
                ->setWithoutTag(true)
        );
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminAllWithoutTagAndExcept($id)
    {
        return $this->tags()->perform(
            (new ShowAll())->setWithoutTag(true)
                           ->setExcept([$id])
        );
    }

    /**
     * @param $data
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function adminCreate($data)
    {
        return $this->tags()->perform(
            new Create(CreateValidator::make()->validate($data))
        );
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function adminUpdate($id, $data)
    {
        $this->validateId($id);

        return $this->tags()->perform(
            new Update($id, UpdateValidator::make()->except($id)->validate($data))
        );
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminDelete($id)
    {
        $this->validateId($id);

        return $this->tags()->perform(new Delete($id));
    }

    /**
     * @param $id
     * @return Tag
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function adminShow($id)
    {
        return $this->validateId($id);
    }

    /**
     * @param $id
     * @return Tag
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function validateId($id) : Tag
    {
        $model = $this->tags()->perform(new Show($id));

        if(!($model instanceof Tag)) {
            throw new ModelNotFoundException("Dana kategoria nie istnieje!");
        }

        return $model;
    }

    /**
     * @return TagRepository
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function tags(): TagRepository
    {
        return app()->make(TagRepository::class);
    }
}