<?php namespace App\Services\Validators\Answer;

use App\Services\Validators\AnswerValidatorInterface;
use App\Services\Validators\ResultSet;
use App\Services\Validators\Answer\Check\CheckMethodResolver;

/**
 * Class AbstractAnswerValidator
 * @package App\Services\Validators\Answer
 *
 * @property CheckMethodResolver $methodResolver
 * @property ResultSet $resultSet
 */
abstract class AbstractAnswerValidator implements AnswerValidatorInterface
{
    protected $methodResolver;
    protected $resultSet;

    public function setResolver(CheckMethodResolver $resolver)
    {
        $this->methodResolver = $resolver;
    }

    public function setResultSet(ResultSet $resultSet)
    {
        $this->resultSet = $resultSet;
    }


}