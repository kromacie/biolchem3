<?php namespace App\Services\Validators\Answer;

use App\Models\Input;
use App\Models\InputGroup;
use App\Services\Validators\Result;
use App\Services\Validators\Reward;
use App\Services\Validators\Answer\Check\CheckMethodInterface;
use App\Services\Validators\RewardSet;


class AnswerValidator extends AbstractAnswerValidator
{

    public function validate(Input $input, InputGroup $group, $answer, RewardSet $rewards)
    {

        $results = $this->results();

        if($correct = $this->resolve($group->type->name)->check($input->corrects, $answer)) {

            $results->add($input->id, new Result(true));
            $this->applyRewards($input, $rewards);
            return true;

        }

        $results->add($input->id, new Result(false));

        return false;
    }

    private function applyRewards(Input $input, RewardSet $rewards)
    {
        foreach ($input->rewards as $reward) {
            $rewards->add(new Reward($reward->id, $reward->points));
        }
    }

    private function results()
    {
        return $this->resultSet;
    }

    private function resolve($type): CheckMethodInterface
    {
        return $this->methodResolver->resolve($type);
    }

}