<?php namespace App\Services\Validators\Answer\Check;

use Illuminate\Support\Str;

trait CheckHelper
{
    public function withoutWhitespaces($text)
    {
        return preg_replace('/\s+/', '', $text);
    }

    public function textToLower($text)
    {
        return Str::lower($text);
    }

    public function convertSpecialChars($text)
    {
        return iconv('UTF-8', 'ISO-8859-1//TRANSLIT//IGNORE', $text);
    }

    public function normalized($text)
    {
        return $this->textToLower(
            $this->convertSpecialChars(
                $this->withoutWhitespaces($text)
            )
        );
    }
}