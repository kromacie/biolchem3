<?php namespace App\Services\Validators\Answer\Check;

use Illuminate\Database\Eloquent\Collection;

interface CheckMethodInterface
{
    public function check(Collection $corrects, $answer);
}