<?php namespace App\Services\Validators\Answer\Check;

class CheckMethodResolver
{
    private $methods = [];

    public function setMethod($type, $class)
    {
        $this->methods[$type] = $class;
    }

    public function resolve($type)
    {
        $method = $this->methods[$type];
        return new $method();
    }
}