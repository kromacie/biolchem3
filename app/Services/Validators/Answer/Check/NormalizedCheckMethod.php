<?php namespace App\Services\Validators\Answer\Check;

use App\Models\InputCorrect;
use App\Models\InputGroup;
use Illuminate\Database\Eloquent\Collection;

class NormalizedCheckMethod implements CheckMethodInterface
{
    use CheckHelper;

    public function check(Collection $corrects, $answer)
    {
        return $corrects->first(function (InputCorrect $correct) use ($answer){
            return $this->normalized($correct->value) == $this->normalized($answer);
        });
    }
}