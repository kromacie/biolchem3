<?php namespace App\Services\Validators\Answer\Check;

use App\Models\InputCorrect;
use Illuminate\Database\Eloquent\Collection;

class StrictCheckMethod implements CheckMethodInterface
{
    use CheckHelper;

    public function check(Collection $corrects, $answer)
    {
        return $corrects->first(function (InputCorrect $correct) use ($answer){
            return $this->withoutWhitespaces($correct->value) == $this->withoutWhitespaces($answer);
        });
    }
}