<?php namespace App\Services\Validators;

use App\Models\Input;
use App\Models\InputGroup;
use App\Services\Validators\Answer\Check\CheckMethodResolver;

interface AnswerValidatorInterface
{
    public function setResolver(CheckMethodResolver $resolver);
    public function setResultSet(ResultSet $resultSet);
    public function validate(Input $input, InputGroup $group, $answer, RewardSet $rewards);
}