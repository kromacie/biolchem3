<?php namespace App\Services\Validators\Group;

use App\Models\Input;
use App\Models\InputGroup;
use App\Services\Validators\AnswerValidatorInterface;
use App\Services\Validators\GroupValidatorInterface;
use App\Services\Validators\ResultSet;
use InvalidArgumentException;

/**
 * Class AbstractGroupValidator
 * @package App\Services\Validators\Group
 *
 * @property AnswerValidatorInterface $answerValidator
 * @property ResultSet $resultSet
 */
abstract class AbstractGroupValidator implements GroupValidatorInterface
{

    protected $answerValidator;
    protected $resultSet;

    public function setAnswerValidator(AnswerValidatorInterface $validator)
    {
        $this->answerValidator = $validator;
        return $this;
    }

    protected function take(InputGroup $group, $input_id)
    {
        return $group->inputs->pull(
            $group->inputs->search(function ($item) use ($input_id){
                return $item->id == $input_id;
            })
        );
    }

    protected function validator()
    {
        return $this->answerValidator;
    }

    /**
     * @param $input
     * @return Input
     * @throws InvalidArgumentException
     */
    protected function validateInput($input)
    {
        if(!($input instanceof Input)) {
            throw new InvalidArgumentException("Input cant be validated");
        }

        return $input;
    }

    protected function results()
    {
        return $this->resultSet;
    }

    public function setResultSet(ResultSet $resultSet)
    {
        $this->resultSet = $resultSet;
        return $this;
    }


}