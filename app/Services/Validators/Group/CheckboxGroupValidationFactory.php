<?php namespace App\Services\Validators\Group;

use App\Services\Validators\GroupValidatorInterface;

class CheckboxGroupValidationFactory extends GroupValidationFactory
{
    public function getValidator(): GroupValidatorInterface
    {
        return (new CheckboxGroupValidator())
            ->setAnswerValidator(
                $this->answerValidator
            )
            ->setResultSet(
                $this->resultSet
            );
    }
}