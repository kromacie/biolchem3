<?php namespace App\Services\Validators\Group;

use App\Models\InputGroup;
use App\Services\Validators\Result;
use App\Services\Validators\RewardSet;

class CheckboxGroupValidator extends AbstractGroupValidator
{

    public function validate(InputGroup $group, array $answers, RewardSet $rewards)
    {

        $in_rewards = new RewardSet();

        foreach ($answers as $input_id => $answer) {
            $input = $this->take($group, $input_id);

            $this->validator()->validate($input, $group, $answer, $in_rewards);
        }

        if(empty($answers)) {
            foreach ($group->inputs as $input) {
                $this->validator()->validate($this->take($group, $input->id), $group, '', $in_rewards);
            }
        }

        $this->verify($in_rewards);

        foreach ($in_rewards->all() as $r) {
            $rewards->add($r);
        }
    }

    private function verify(RewardSet $rewards)
    {
        $hasFailures = false;

        /** @var Result $result */
        foreach ($this->results()->getCurrentGroup() as $result) {
            if($result->getStatus() == false) {
                $hasFailures = true;
            }
        }

        if($hasFailures) {
            $rewards->invalidate();
        }
    }
}