<?php namespace App\Services\Validators\Group;

use App\Services\Validators\GroupValidatorInterface;

class ClassicGroupValidationFactory extends GroupValidationFactory
{

    public function getValidator(): GroupValidatorInterface
    {
        return (new ClassicGroupValidator())
            ->setAnswerValidator(
                $this->answerValidator
            )
            ->setResultSet(
                $this->resultSet
            );
    }
}