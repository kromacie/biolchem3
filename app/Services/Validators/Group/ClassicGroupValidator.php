<?php namespace App\Services\Validators\Group;

use App\Models\InputGroup;
use App\Services\Validators\RewardSet;

class ClassicGroupValidator extends AbstractGroupValidator
{

    public function validate(InputGroup $group, array $answers, RewardSet $rewards)
    {
        foreach ($answers as $input_id => $answer) {
            $input = $this->take($group, $input_id);

            $this->validator()->validate($input, $group, $answer, $rewards);
        }
    }
}