<?php namespace App\Services\Validators\Group;

use App\Services\Validators\AnswerValidatorInterface;
use App\Services\Validators\GroupValidatorInterface;
use App\Services\Validators\ResultSet;

abstract class GroupValidationFactory
{
    protected $answerValidator;
    protected $resultSet;

    public function setResultSet(ResultSet $resultSet)
    {
        $this->resultSet = $resultSet;
    }

    public function setAnswerValidator(AnswerValidatorInterface $answerValidator)
    {
        $this->answerValidator = $answerValidator;
    }

    abstract public function getValidator(): GroupValidatorInterface;
}