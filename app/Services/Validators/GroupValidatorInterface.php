<?php namespace App\Services\Validators;

use App\Models\InputGroup;

interface GroupValidatorInterface
{
    public function setAnswerValidator(AnswerValidatorInterface $validator);
    public function setResultSet(ResultSet $resultSet);
    public function validate(InputGroup $group, array $answers, RewardSet $rewards);
}