<?php namespace App\Services\Validators\Question;

use App\Models\Question;
use App\Models\QuestionSubstitute;
use App\Services\Validators\QuestionValidatorInterface;
use App\Services\Validators\ResultSet;
use App\Services\Validators\SubstituteValidatorInterface;
use App\Services\Validators\RewardSet;
use InvalidArgumentException;

/**
 * Class AbstractQuestionValidator
 * @package App\Services\Question
 *
 * @property QuestionEventLoggerInterface $logger
 * @property ResultSet $resultSet
 * @property SubstituteValidatorInterface $substituteValidator
 * @property QuestionRewardCreatorInterface $creator
 */
abstract class AbstractQuestionValidator implements QuestionValidatorInterface
{

    protected $logger;
    protected $substituteValidator;
    protected $shouldEventStart;
    protected $shouldEventFinish;
    protected $shouldCreateStatistics;
    protected $resultSet;
    protected $creator;

    public function shouldEventQuestionValidationStart(bool $status)
    {
        $this->shouldEventStart = $status;
    }

    public function shouldEventQuestionValidationFinish(bool $status)
    {
        $this->shouldEventFinish = $status;
    }

    public function shouldCreateQuestionValidationStatistics(bool $status)
    {
        $this->shouldCreateStatistics = $status;
    }

    public function setQuestionEventLogger(QuestionEventLoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function setSubstituteValidator(SubstituteValidatorInterface $validator)
    {
        $this->substituteValidator = $validator;
    }

    public function setResultSet(ResultSet $resultSet)
    {
        $this->resultSet = $resultSet;
    }

    protected function validator()
    {
        return $this->substituteValidator;
    }

    protected function results()
    {
        return $this->resultSet;
    }

    protected function take(Question $question, $substitute_id)
    {
        return $question->substitutes->pull(
            $question->substitutes->search(function ($item) use ($substitute_id){
                return $item->id == $substitute_id;
            })
        );
    }

    protected function eventValidationFinish(Question $question, RewardSet $rewards)
    {
        $this->logger->eventValidationFinish(
            $question, $rewards->sum(), $question->points, $this->results()
        );
    }

    protected function eventValidationStart(Question $question)
    {
        $this->logger->eventValidationStart($question->id);
    }

    protected function getFreshQuestionInstance(Question $question): Question
    {
        return $question->fresh([
            'substitutes.inputGroups.inputs.corrects',
            'substitutes.inputGroups.type',
        ]);
    }

    protected function createStatistics($question_id, $got_points, $max_points)
    {
        return $this->creator->create($got_points, $max_points, $question_id);
    }
    /**
     * @param $substitute
     * @return QuestionSubstitute
     * @throws InvalidArgumentException
     */
    protected function validateSubstitute($substitute)
    {
        if(!($substitute instanceof QuestionSubstitute)) {
            throw new InvalidArgumentException("Substitute cant be validated");
        }
        return $substitute;
    }

    public function setRewardCreator(QuestionRewardCreatorInterface $creator)
    {
        $this->creator = $creator;
    }


}