<?php namespace App\Services\Validators\Question;

use App\Models\Question;

interface QuestionEventLoggerInterface
{
    public function eventValidationStart($question_id);
    public function eventValidationFinish(Question $question, $got_points, $max_points, $answers = []);
}