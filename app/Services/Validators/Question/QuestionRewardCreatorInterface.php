<?php namespace App\Services\Validators\Question;

interface QuestionRewardCreatorInterface
{
    public function create($got_points, $max_points, $question_id);
}