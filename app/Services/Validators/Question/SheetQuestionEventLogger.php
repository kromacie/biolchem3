<?php namespace App\Services\Validators\Question;

use App\Events\QuestionHasBeenChecked;
use App\Models\Question;

class SheetQuestionEventLogger implements QuestionEventLoggerInterface
{
    private $user_id;
    private $sheet_id;

    /**
     * SheetQuestionEventLogger constructor.
     * @param $user_id
     * @param $sheet_id
     */
    public function __construct($user_id, $sheet_id)
    {
        $this->user_id = $user_id;
        $this->sheet_id = $sheet_id;
    }


    public function eventValidationStart($answer_data = [])
    {

    }

    public function eventValidationFinish(Question $question, $got_points, $max_points, $answers = [])
    {
        event(new QuestionHasBeenChecked($this->sheet_id, $this->user_id, [
            'got_points' => $got_points,
            'max_points' => $max_points,
            'question_id' => $question->id
        ]));
    }
}