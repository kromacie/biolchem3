<?php namespace App\Services\Validators\Question;

use App\Repositories\ResolvedQuestionRepository;
use App\Services\ResolvedQuestionService;

class SheetQuestionRewardCreator implements QuestionRewardCreatorInterface
{

    private $sheet_id;

    private $service;

    /**
     * SheetQuestionRewardCreator constructor.
     * @param ResolvedQuestionService $service
     * @param $sheet_id
     */
    public function __construct(ResolvedQuestionService $service, $sheet_id)
    {
        $this->service = $service;
        $this->sheet_id = $sheet_id;
    }


    /**
     * @param $got_points
     * @param $max_points
     * @param $question_id
     * @return mixed
     * @throws \Throwable
     */
    public function create($got_points, $max_points, $question_id)
    {
        return $this->service->create([
            'got_result' => $got_points,
            'max_result' => $max_points,
            'question_id' => $question_id,
            'sheet_id' => $this->sheet_id
        ]);
    }
}