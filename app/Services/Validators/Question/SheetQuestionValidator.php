<?php namespace App\Services\Validators\Question;

use App\Models\Question;
use App\Services\Validators\Reward;
use App\Services\Validators\RewardSet;

class SheetQuestionValidator extends AbstractQuestionValidator
{

    public function validate(Question $question, array $answers)
    {
        $rewards = new RewardSet();

        if($this->shouldEventStart) {
            $this->eventValidationStart($question);
        }

        foreach ($answers as $substitute_id => $substitute_answers)
        {
            $substitute = $this->validateSubstitute(
                $this->take($question, $substitute_id)
            );

            $this->results()->setCurrentSubstitute($substitute_id);

            /** @var Reward $reward */
            $reward = $this->validator()->validate($substitute, $substitute_answers);

            $rewards->add(
                $reward
            );
        }

        foreach ($question->substitutes as $substitute)
        {
            $this->results()->setCurrentSubstitute($substitute->id);

            /** @var Reward $reward */
            $reward = $this->validator()->validate($substitute, []);

            $rewards->add(
                $reward
            );
        }

        $question = $this->getFreshQuestionInstance($question);

        if($this->shouldEventFinish) {
            $this->eventValidationFinish($question, $rewards);
        }

        if($this->shouldCreateStatistics) {
            $this->createStatistics($question->id, $rewards->sum(), $question->points);
        }

        return $rewards;
    }
}