<?php namespace App\Services\Validators\Question;

use App\Events\Question\Finish;
use App\Events\Question\Start;
use App\Models\Question;

class SingleQuestionEventLogger implements  QuestionEventLoggerInterface
{

    public $uuid;

    /**
     * SingleQuestionEventLogger constructor.
     * @param $uuid
     */
    public function __construct($uuid)
    {
        $this->uuid = $uuid;
    }


    public function eventValidationStart($question_id)
    {
        event(new Start($question_id, $this->uuid));
    }

    public function eventValidationFinish(Question $question, $got_points, $max_points, $answers = [])
    {
        event(new Finish($question->id, $this->uuid, $question, $answers, [
            'max_points' => $max_points,
            'got_points' => $got_points
        ]));
    }
}