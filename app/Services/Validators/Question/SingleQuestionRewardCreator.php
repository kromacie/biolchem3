<?php namespace App\Services\Validators\Question;

use App\Services\ResolvedQuestionService;

class SingleQuestionRewardCreator implements QuestionRewardCreatorInterface
{

    private $service;

    public function __construct(ResolvedQuestionService $service)
    {
        $this->service = $service;
    }

    /**
     * @param $got_points
     * @param $max_points
     * @param $question_id
     * @return mixed
     * @throws \Throwable
     */
    public function create($got_points, $max_points, $question_id)
    {
        return $this->service->create([
            'got_result' => $got_points,
            'max_result' => $max_points,
            'question_id' => $question_id
        ]);
    }
}