<?php namespace App\Services\Validators;

use App\Models\Question;
use App\Services\Validators\Question\QuestionEventLoggerInterface;
use App\Services\Validators\Question\QuestionRewardCreatorInterface;

interface QuestionValidatorInterface
{
    public function shouldEventQuestionValidationStart(bool $status);
    public function shouldEventQuestionValidationFinish(bool $status);
    public function shouldCreateQuestionValidationStatistics(bool $status);
    public function setQuestionEventLogger(QuestionEventLoggerInterface $logger);
    public function setSubstituteValidator(SubstituteValidatorInterface $validator);
    public function setRewardCreator(QuestionRewardCreatorInterface $creator);
    public function setResultSet(ResultSet $resultSet);
    public function validate(Question $question, array $answers);
}