<?php namespace App\Services\Validators;

use Illuminate\Contracts\Support\Jsonable;

class ResultSet implements Jsonable, \JsonSerializable
{
    private $results = [];

    private $currentQuestion;
    private $currentSubstitute;
    private $currentGroup;

    /**
     * @param mixed $id
     */
    public function setCurrentQuestion($id): void
    {
        $this->currentQuestion = $id;
    }

    /**
     * @param mixed $id
     */
    public function setCurrentSubstitute($id): void
    {
        $this->currentSubstitute = $id;
    }

    /**
     * @param mixed $id
     */
    public function setCurrentGroup($id): void
    {
        $this->currentGroup = $id;
    }

    public function add($input_id, Result $result)
    {
        $this->initialize();

        $this->results[$this->currentQuestion][$this->currentSubstitute][$this->currentGroup][$input_id] = $result;
    }

    private function initialize()
    {
        if(!key_exists($this->currentQuestion, $this->results)) {
            $this->results[$this->currentQuestion] = [];
        }

        if(!key_exists($this->currentSubstitute, $this->results[$this->currentQuestion])) {
            $this->results[$this->currentQuestion][$this->currentSubstitute] = [];
        }

        if(!key_exists($this->currentGroup, $this->results[$this->currentQuestion][$this->currentSubstitute])) {
            $this->results[$this->currentQuestion][$this->currentSubstitute][$this->currentGroup] = [];
        }
    }

    public function optimize()
    {
        $this->results = $this->filter($this->results);
        return $this;
    }

    private function filter($results)
    {
        if(!is_array($results)) {
            return $results;
        }

        foreach ($results as $key => $el) {
            $answers[$key] = $this->filter($el);
        }

        foreach ($results as $key => $el) {
            if($this->count($results) == 0) {
                unset($results[$key]);
            }
        }

        return $results;
    }

    private function count($array)
    {
        if(!is_array($array)) {
            return 1;
        }

        $total = 0;

        foreach ($array as $index => $value) {
            $total += $this->count($value);
        }

        return $total;
    }

    public function getCurrentGroup()
    {
        return $this->results[$this->currentQuestion][$this->currentSubstitute][$this->currentGroup];
    }

    /**
     * Convert the object to its JSON representation.
     *
     * @param  int $options
     * @return string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->optimize());
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return $this->results;
    }
}