<?php namespace App\Services\Validators;

class Reward
{
    private $id;
    private $points;

    /**
     * Reward constructor.
     * @param $id
     * @param $points
     */
    public function __construct($id, $points)
    {
        $this->id = $id;
        $this->points = $points;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPoints()
    {
        return $this->points;
    }


}