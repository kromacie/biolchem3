<?php namespace App\Services\Validators;

use Illuminate\Support\Collection;

class RewardSet
{
    private $rewards = [];

    public function add(Reward $reward)
    {
        $this->rewards[] = $reward;
    }

    public function all()
    {
        return $this->rewards;
    }

    public function count()
    {
        $counted = [];
        /** @var Reward $reward */
        foreach ($this->rewards as $reward)
        {
            key_exists($reward->getId(), $counted) ? $counted[$reward->getId()] += 1 : $counted[$reward->getId()] = 1;
        }

        $new = new RewardSet();

        foreach ($counted as $id => $count) {
            $new->add(new Reward($id, $count));
        }

        return $new;
    }

    public function sum()
    {
        $points = 0;

        /** @var Reward $reward */
        foreach ($this->rewards as $reward)
        {
            $points += $reward->getPoints();
        }

        return $points;
    }

    public function invalidate()
    {
        $this->rewards = [];
    }

    public function highest(Collection $rewards)
    {
        /** @var Reward $current */
        foreach ($this->count()->all() as $current) {
            /** @var \App\Models\Reward $reward */
            foreach ($rewards as $reward) {
                if($current->getId() == $reward->id && $current->getPoints() >= $reward->inputs->count()) {
                    return $reward;
                }
            }
        }

        return null;

    }
}