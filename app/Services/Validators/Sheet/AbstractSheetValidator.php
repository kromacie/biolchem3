<?php namespace App\Services\Validators\Sheet;

use App\Services\Validators\QuestionValidatorInterface;
use App\Services\Validators\ResultSet;
use App\Services\Validators\SheetValidatorInterface;

/**
 * Class AbstractSheetValidator
 * @package App\Validators\Admin\Sheet
 *
 * @property SheetEventLogger $logger
 * @property QuestionValidatorInterface $questionValidator
 * @property ResultSet $resultSet
 * @property SheetRewardCreatorInterface $creator
 */
abstract class AbstractSheetValidator implements SheetValidatorInterface
{
    protected $eventValidationStart;
    protected $eventValidationFinish;
    protected $createValidationStatistics;
    protected $questionValidator;
    protected $resultSet;
    protected $logger;
    protected $creator;

    public function setQuestionValidator(QuestionValidatorInterface $validator)
    {
        $this->questionValidator = $validator;
    }

    public function shouldEventValidationStart(bool $status)
    {
        $this->eventValidationStart = $status;
    }

    public function shouldEventValidationFinish(bool $status)
    {
        $this->eventValidationFinish = $status;
    }

    public function shouldCreateValidationStatistics(bool $status)
    {
        $this->createValidationStatistics = $status;
    }

    public function setEventLogger(SheetEventLoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function setResultSet(ResultSet $resultSet)
    {
        $this->resultSet = $resultSet;
    }

    public function setRewardCreator(SheetRewardCreatorInterface $creator)
    {
        $this->creator = $creator;
    }

}