<?php namespace App\Services\Validators\Sheet;

use App\Events\SheetCheckingHasBeenStarted;
use App\Events\SheetHasBeenChecked;
use App\Models\Sheet;

class SheetEventLogger implements SheetEventLoggerInterface
{
    private $sheet_id;
    private $user_id;

    /**
     * SheetEventLogger constructor.
     * @param $sheet_id
     * @param $user_id
     */
    public function __construct($sheet_id, $user_id)
    {
        $this->sheet_id = $sheet_id;
        $this->user_id = $user_id;
    }


    public function eventValidationStart()
    {
        event(new SheetCheckingHasBeenStarted($this->sheet_id, $this->user_id));
    }

    public function eventValidationFinish(Sheet $sheet, $got_points, $max_points, $answers)
    {
        event(new SheetHasBeenChecked(
            $this->sheet_id,
            $this->user_id,
            $sheet,
            $answers, [
                'max_points' => $max_points,
                'got_points' => $got_points
            ]));
    }
}