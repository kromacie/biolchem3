<?php namespace App\Services\Validators\Sheet;

use App\Models\Sheet;

interface SheetEventLoggerInterface
{
    public function eventValidationStart();
    public function eventValidationFinish(Sheet $sheet, $got_points, $max_points, $answers);
}