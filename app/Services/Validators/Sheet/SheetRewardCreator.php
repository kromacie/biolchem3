<?php namespace App\Services\Validators\Sheet;

use App\Services\ResolvedSheetService;

class SheetRewardCreator implements SheetRewardCreatorInterface
{

    private $sheet_id;
    private $service;

    /**
     * SheetRewardCreate constructor.
     * @param ResolvedSheetService $service
     * @param $sheet_id
     */
    public function __construct(ResolvedSheetService $service, $sheet_id)
    {
        $this->sheet_id = $sheet_id;
        $this->service = $service;
    }


    /**
     * @return mixed
     * @throws \Throwable
     */
    public function create()
    {
        return $this->service->create([
            'sheet_id' => $this->sheet_id
        ]);
    }
}