<?php namespace App\Services\Validators\Sheet;

interface SheetRewardCreatorInterface
{
    public function create();
}