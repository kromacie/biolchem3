<?php namespace App\Services\Validators\Sheet;

use App\Models\Question;
use App\Models\Sheet;
use App\Services\Validators\Reward;
use App\Services\Validators\RewardSet;
use InvalidArgumentException;

class SheetValidator extends AbstractSheetValidator
{

    public function validate(Sheet $sheet, array $answers)
    {

        if($this->eventValidationStart) {
            $this->eventValidationStart();
        }

        $rewards = new RewardSet();

        foreach ($answers as $question_id => $question_answers) {
            $question = $this->validateQuestion(
                $this->take($sheet, $question_id)
            );

            $this->results()->setCurrentQuestion($question_id);

            /** @var RewardSet $reward */
            $reward = $this->validator()->validate(
                $question, $question_answers
            );

            $rewards->add(new Reward($question_id, $reward->sum()));
        }

        foreach ($sheet->questions as $question) {
            $this->results()->setCurrentQuestion($question->id);

            /** @var RewardSet $reward */
            $reward = $this->validator()->validate(
                $question, []
            );

            $rewards->add(new Reward($question->id, $reward->sum()));
        }

        if($this->eventValidationFinish) {
            $this->eventValidationFinish($sheet, $rewards);
        }

        if($this->createValidationStatistics) {
            $this->createStatistics();
        }

    }

    private function eventValidationStart()
    {
        $this->logger->eventValidationStart();
    }

    private function eventValidationFinish(Sheet $sheet, RewardSet $rewards)
    {
        $sheet = $this->getFreshSheetInstance($sheet);
        $this->logger->eventValidationFinish(
            $sheet,
            $rewards->sum(),
            $sheet->points,
            $this->results()
        );
    }

    private function getFreshSheetInstance(Sheet $sheet)
    {
        return $sheet->fresh([
            'questions.substitutes.inputGroups.inputs.corrects',
            'questions.substitutes.inputGroups.type',
            ]);
    }

    private function createStatistics()
    {
        return $this->creator->create();
    }

    /**
     * @param Sheet $sheet
     * @param $id
     * @return Question
     */
    private function take(Sheet $sheet, $id)
    {
        return $sheet->questions->pull(
            $sheet->questions->search(function ($item) use ($id){
                return $item->id == $id;
            })
        );
    }

    private function results()
    {
        return $this->resultSet;
    }

    private function validator()
    {
        return $this->questionValidator;
    }

    /**
     * @param $question
     * @return Question
     * @throws InvalidArgumentException
     */
    private function validateQuestion($question)
    {
        if(!($question instanceof Question)) {
            throw new InvalidArgumentException("Question cant be validated");
        }

        return $question;
    }
}