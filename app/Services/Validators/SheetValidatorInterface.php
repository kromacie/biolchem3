<?php namespace App\Services\Validators;

use App\Models\Sheet;
use App\Services\Validators\Sheet\SheetEventLoggerInterface;
use App\Services\Validators\Sheet\SheetRewardCreatorInterface;

interface SheetValidatorInterface {
    public function setResultSet(ResultSet $resultSet);
    public function setQuestionValidator(QuestionValidatorInterface $validator);
    public function setRewardCreator(SheetRewardCreatorInterface $creator);
    public function setEventLogger(SheetEventLoggerInterface $logger);
    public function shouldEventValidationStart(bool $status);
    public function shouldEventValidationFinish(bool $status);
    public function shouldCreateValidationStatistics(bool $status);
    public function validate(Sheet $sheet, array $answers);
}