<?php namespace App\Services\Validators\Substitute;

use App\Services\Validators\Group\GroupValidationFactory;
use App\Services\Validators\ResultSet;
use App\Services\Validators\SubstituteValidatorInterface;

/**
 * Class AbstractSubstituteValidator
 * @package App\Services\Validators\Substitute
 *
 * @property GroupValidationFactory[] $groupValidators
 * @property ResultSet $resultSet
 */
abstract class AbstractSubstituteValidator implements SubstituteValidatorInterface
{

    protected $groupValidators = [];
    protected $resultSet;

    public function setGroupValidator(GroupValidationFactory $factory, $type)
    {
        $this->groupValidators[$type] = $factory;
    }

    public function setResultSet(ResultSet $resultSet)
    {
        $this->resultSet = $resultSet;
    }


}