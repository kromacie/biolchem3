<?php namespace App\Services\Validators\Substitute;

use App\Models\InputGroup;
use App\Models\QuestionSubstitute;
use App\Services\Validators\Reward;
use App\Services\Validators\RewardSet;
use InvalidArgumentException;

class SubstituteValidator extends AbstractSubstituteValidator {

    public function validate(QuestionSubstitute $substitute, array $answers)
    {
        $rewards = new RewardSet();

        foreach ($answers as $group_id => $answers_group)
        {
            $group = $this->validateGroup(
                $this->take($substitute, $group_id)
            );

            $this->results()->setCurrentGroup($group_id);

            $this->resolve($group->type->name)->validate($group, $answers_group, $rewards);
        }

        foreach ($substitute->inputGroups as $group) {
            $this->results()->setCurrentGroup($group->id);

            $this->resolve($group->type->name)->validate($group, [], $rewards);
        }

        return $this->reward($substitute, $rewards);
    }

    /**
     * @param $group
     * @return InputGroup
     * @throws InvalidArgumentException
     */
    private function validateGroup($group)
    {
        if(!($group instanceof InputGroup)) {
            throw new InvalidArgumentException("Group cant be validated");
        }
        return $group;
    }

    private function take(QuestionSubstitute $substitute, $group_id)
    {
        return $substitute->inputGroups->pull(
            $substitute->inputGroups->search(function ($item) use ($group_id){
                return $item->id == $group_id;
            })
        );
    }

    private function results()
    {
        return $this->resultSet;
    }

    /**
     * @param QuestionSubstitute $substitute
     * @param RewardSet $rewards
     * @return \App\Services\Validators\Reward
     */
    private function reward(QuestionSubstitute $substitute, RewardSet $rewards)
    {
        if($reward = $rewards->highest($substitute->rewards)) {
            return new Reward($reward->id, $reward->points);
        }
        return new Reward(0, 0);

    }

    private function resolve($type)
    {
        return $this->groupValidators[$type]->getValidator();
    }
}