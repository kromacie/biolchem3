<?php namespace App\Services\Validators;

use App\Models\QuestionSubstitute;
use App\Services\Validators\Group\GroupValidationFactory;

interface SubstituteValidatorInterface
{
    public function setResultSet(ResultSet $resultSet);
    public function setGroupValidator(GroupValidationFactory $validator, $type);
    public function validate(QuestionSubstitute $substitute, array $answers);
}