<?php namespace App\Services\Validators;

use App\Services\ResolvedSheetService;
use App\Services\Validators\Answer\AnswerValidator;
use App\Services\Validators\Answer\Check\CheckMethodResolver;
use App\Services\Validators\Answer\Check\NormalizedCheckMethod;
use App\Services\Validators\Answer\Check\StrictCheckMethod;
use App\Services\Validators\Group\CheckboxGroupValidationFactory;
use App\Services\Validators\Group\ClassicGroupValidationFactory;
use App\Services\Validators\Group\GroupValidationFactory;
use App\Services\Validators\Question\QuestionEventLoggerInterface;
use App\Services\Validators\Question\QuestionRewardCreatorInterface;
use App\Services\Validators\Question\SheetQuestionValidator;
use App\Services\Validators\Sheet\SheetEventLogger;
use App\Services\Validators\Sheet\SheetRewardCreator;
use App\Services\Validators\Sheet\SheetValidator;
use App\Services\Validators\Substitute\SubstituteValidator;

class ValidationFactory
{

    private $event_logger;
    private $question_reward_creator;
    private $sheet_id;
    private $user_id;
    private $question_id;

    /**
     * @param mixed $event_logger
     * @return ValidationFactory
     */
    public function setQuestionEventLogger(QuestionEventLoggerInterface $event_logger)
    {
        $this->event_logger = $event_logger;
        return $this;
    }

    /**
     * @param mixed $question_reward_creator
     * @return ValidationFactory
     */
    public function setQuestionRewardCreator(QuestionRewardCreatorInterface $question_reward_creator)
    {
        $this->question_reward_creator = $question_reward_creator;
        return $this;
    }

    /**
     * @param mixed $sheet_id
     * @return ValidationFactory
     */
    public function setSheetId($sheet_id)
    {
        $this->sheet_id = $sheet_id;
        return $this;
    }

    /**
     * @param mixed $user_id
     * @return ValidationFactory
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * @param mixed $question_id
     * @return ValidationFactory
     */
    public function setQuestionId($question_id)
    {
        $this->question_id = $question_id;
        return $this;
    }


    public function buildCheckMethodResolver(): CheckMethodResolver
    {
        $resolver = new CheckMethodResolver();
        $resolver->setMethod('SINGULAR_TEXT', NormalizedCheckMethod::class);
        $resolver->setMethod('MULTIPLE_TEXT', NormalizedCheckMethod::class);
        $resolver->setMethod('SINGULAR_LETTER', NormalizedCheckMethod::class);
        $resolver->setMethod('MULTIPLE_LETTER', StrictCheckMethod::class);
        $resolver->setMethod('SINGULAR_CHECKBOX', NormalizedCheckMethod::class);
        $resolver->setMethod('MULTIPLE_RADIO', NormalizedCheckMethod::class);
        return $resolver;
    }

    public function buildAnswerValidator(ResultSet $resultSet): AnswerValidatorInterface
    {
        $answerValidator = new AnswerValidator();
        $answerValidator->setResultSet($resultSet);
        $answerValidator->setResolver(
            $this->buildCheckMethodResolver()
        );

        return $answerValidator;
    }

    public function buildClassicGroupValidator(ResultSet $resultSet, AnswerValidatorInterface $validator): GroupValidationFactory
    {
        $classicGroupFactory = new ClassicGroupValidationFactory();
        $classicGroupFactory->setAnswerValidator(
            $validator
        );
        $classicGroupFactory->setResultSet($resultSet);

        return $classicGroupFactory;
    }

    public function buildCheckboxGroupValidator(ResultSet $resultSet, AnswerValidatorInterface $validator): GroupValidationFactory
    {
        $checkboxGroupFactory = new CheckboxGroupValidationFactory();
        $checkboxGroupFactory->setAnswerValidator(
            $validator
        );
        $checkboxGroupFactory->setResultSet($resultSet);

        return $checkboxGroupFactory;
    }

    public function buildSubstituteValidator(ResultSet $resultSet)
    {

        $answerValidator = $this->buildAnswerValidator($resultSet);
        $classicGroupFactory = $this->buildClassicGroupValidator($resultSet, $answerValidator);
        $checkboxGroupFactory = $this->buildCheckboxGroupValidator($resultSet, $answerValidator);

        $substituteValidator = new SubstituteValidator();
        $substituteValidator->setGroupValidator($classicGroupFactory, 'SINGULAR_TEXT');
        $substituteValidator->setGroupValidator($classicGroupFactory, 'MULTIPLE_TEXT');
        $substituteValidator->setGroupValidator($classicGroupFactory, 'SINGULAR_LETTER');
        $substituteValidator->setGroupValidator($classicGroupFactory, 'MULTIPLE_LETTER');
        $substituteValidator->setGroupValidator($checkboxGroupFactory, 'SINGULAR_CHECKBOX');
        $substituteValidator->setGroupValidator($classicGroupFactory, 'MULTIPLE_RADIO');
        $substituteValidator->setResultSet($resultSet);

        return $substituteValidator;

    }

    public function buildSheetQuestionValidator(ResultSet $resultSet)
    {

        $questionValidator = new SheetQuestionValidator();
        $questionValidator->shouldEventQuestionValidationFinish(true);
        $questionValidator->shouldEventQuestionValidationStart(true);
        $questionValidator->shouldCreateQuestionValidationStatistics(true);
        $questionValidator->setResultSet($resultSet);
        $questionValidator->setRewardCreator(
            $this->question_reward_creator
        );

        $questionValidator->setQuestionEventLogger(
            $this->event_logger
        );
        $questionValidator->setSubstituteValidator(
            $this->buildSubstituteValidator($resultSet)
        );

        return $questionValidator;
    }


    /**
     * @return SheetValidator
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function buildSheetValidator()
    {
        $resultSet = new ResultSet();



        $validator = new SheetValidator();
        $validator->shouldEventValidationStart(true);
        $validator->shouldEventValidationFinish(true);
        $validator->shouldCreateValidationStatistics(true);
        $validator->setResultSet($resultSet);
        $validator->setRewardCreator(
            new SheetRewardCreator(
                app()->make(ResolvedSheetService::class), $this->sheet_id
            )
        );
        $validator->setEventLogger(
            new SheetEventLogger($this->sheet_id, $this->user_id)
        );
        $validator->setQuestionValidator(
            $this->buildSheetQuestionValidator($resultSet)
        );

        return $validator;
    }
}