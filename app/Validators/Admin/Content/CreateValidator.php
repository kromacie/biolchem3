<?php namespace App\Validators\Admin\Content;

use App\Validators\Validator;
use Illuminate\Validation\Validator as ValidatorInstance;

class CreateValidator extends Validator
{

    public function messages(): array
    {
        return [];
    }

    public function rules(): array
    {
        return [
            'image_id' => 'sometimes|exists:images,id',
            'description' => 'sometimes'
        ];
    }

    public function attributes(): array
    {
        return [];
    }
}