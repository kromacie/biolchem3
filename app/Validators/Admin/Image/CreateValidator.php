<?php namespace App\Validators\Admin\Image;

use App\Validators\Validator;

class CreateValidator extends Validator
{

    public function messages(): array
    {
        return [];
    }

    public function rules(): array
    {
        return [
            'image' => 'required|image:2048'
        ];
    }

    public function attributes(): array
    {
        return [];
    }
}