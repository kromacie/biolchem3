<?php namespace App\Validators\Admin\Input\Correct;

use App\Validators\Validator;

class CreateValidator extends Validator
{

    public function messages(): array
    {
        return [];
    }

    public function rules(): array
    {
        return [
            'value' => 'required|string|min:1|max:255',
            'input_id' => 'required|exists:inputs,id'
        ];
    }

    public function attributes(): array
    {
        return [];
    }
}