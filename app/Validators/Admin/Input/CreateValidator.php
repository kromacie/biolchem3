<?php namespace App\Validators\Admin\Input;

use App\Validators\Validator;

class CreateValidator extends Validator
{

    public function messages(): array
    {
        return [];
    }

    public function rules(): array
    {
        return [
            'input_group_id' => 'required|exists:input_groups,id',
            'description' => 'required|nullable|string',
            'checked' => 'sometimes',
            'disabled' => 'sometimes',
            'is_description' => 'sometimes',
            'type_id' => 'nullable|exists:input_types,id',
            'placeholder' => 'nullable|string|min:1'
        ];
    }

    public function attributes(): array
    {
        return [];
    }
}