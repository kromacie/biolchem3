<?php namespace App\Validators\Admin\Input\Group;

use App\Validators\Validator;

class CreateValidator extends Validator
{

    public function messages(): array
    {
        return [];
    }

    public function rules(): array
    {
        return [
            'type_id' => 'required|exists:input_group_types,id',
            'description' => 'nullable|string'
        ];
    }

    public function attributes(): array
    {
        return [];
    }
}