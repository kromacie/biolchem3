<?php namespace App\Validators\Admin\Input\Group\Type;

use App\Validators\Validator;

class CreateValidator extends Validator
{

    private $except = 'null';

    /**
     * @param mixed $except
     * @return CreateValidator
     */
    public function except($except)
    {
        $this->except = $except;
        return $this;
    }

    public function messages(): array
    {
        return [];
    }

    public function rules(): array
    {
        return [
            'group_class' => 'nullable|string|min:1',
            'input_class' => 'required|string|min:1',
            'name' => 'required|unique:input_group_types,name,' . $this->except . ',id'
        ];
    }

    public function attributes(): array
    {
        return [];
    }
}