<?php namespace App\Validators\Admin\Input\Reward;

use App\Validators\Validator;

class CreateValidator extends Validator
{

    public function messages(): array
    {
        return [];
    }

    public function rules(): array
    {
        return [
            'reward_id' => 'required|exists:rewards,id',
            'input_id' => 'required|exists:inputs,id'
        ];
    }

    public function attributes(): array
    {
        return [];
    }
}