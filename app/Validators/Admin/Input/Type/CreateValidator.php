<?php namespace App\Validators\Admin\Input\Type;

use App\Validators\Validator;

class CreateValidator extends Validator
{

    private $except = 'null';

    /**
     * @param mixed $id
     * @return CreateValidator
     */
    public function except($id)
    {
        $this->except = $id;
        return $this;
    }



    public function messages(): array
    {
        return [];
    }

    public function rules(): array
    {
        return [
            'name' => 'required|string|min:1|unique:input_types,name,' . $this->except . ',id',
            'class' => 'required|string|min:1'
        ];
    }

    public function attributes(): array
    {
        return [];
    }
}