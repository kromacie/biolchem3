<?php namespace App\Validators\Admin\Question;

use App\Validators\Validator;

class CheckValidator extends Validator
{
    public function messages(): array
    {
        return [];
    }

    public function rules(): array
    {
        return [
            'answers.*.*.*.*' => 'nullable',
            'question_id' => 'required|exists:questions,id',
            'user_id' => 'required|string'
        ];
    }

    public function attributes(): array
    {
        return [];
    }
}