<?php namespace App\Validators\Admin\Question\Contents;

use App\Validators\Validator;

class CreateValidator extends Validator
{

    public function messages(): array
    {
        return [];
    }

    public function rules(): array
    {
        return [
            'content_id' => 'required|exists:contents,id',
            'question_id' => 'required|exists:questions,id'
        ];
    }

    public function attributes(): array
    {
        return [];
    }
}