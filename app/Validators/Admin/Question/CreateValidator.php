<?php namespace App\Validators\Admin\Question;

use App\Validators\Validator;

class CreateValidator extends Validator
{

    public function messages(): array
    {
        return [];
    }

    public function rules(): array
    {
        return [
            'title' => 'required|string|min:1|max:255',
            'sheet_id' => 'required|exists:sheets,id',
            'enabled' => 'sometimes'
        ];
    }

    public function attributes(): array
    {
        return [];
    }
}