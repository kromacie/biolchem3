<?php namespace App\Validators\Admin\Question\Substitutes\Contents;

use App\Validators\Validator;

class CreateValidator extends Validator
{

    public function messages(): array
    {
        return [];
    }

    public function rules(): array
    {
        return [
            'content_id' => 'required|exists:contents,id',
            'question_substitute_id' => 'required|exists:question_substitutes,id'
        ];
    }

    public function attributes(): array
    {
        return [];
    }
}