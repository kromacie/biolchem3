<?php namespace App\Validators\Admin\Question\Substitutes;

use App\Validators\Validator;

class CreateValidator extends Validator
{

    public function messages(): array
    {
        return [];
    }

    public function rules(): array
    {
        return [
            'question_id' => 'required|exists:questions,id'
        ];
    }

    public function attributes(): array
    {
        return [];
    }
}