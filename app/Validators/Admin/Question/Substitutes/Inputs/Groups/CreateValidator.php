<?php namespace App\Validators\Admin\Question\Substitutes\Inputs\Groups;

use App\Validators\Validator;

class CreateValidator extends Validator
{

    public function messages(): array
    {
        return [];
    }

    public function rules(): array
    {
        return [
            'question_substitute_id' => 'required|exists:question_substitutes,id',
            'input_group_id' => 'required|exists:input_groups,id',
        ];
    }

    public function attributes(): array
    {
        return [];
    }
}