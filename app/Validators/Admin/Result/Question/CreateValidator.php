<?php namespace App\Validators\Admin\Result\Question;

use App\Validators\Validator;

class CreateValidator extends Validator
{
    public function messages(): array
    {
        return [];
    }

    public function rules(): array
    {
        return [
            'got_result' => 'required|numeric',
            'max_result' => 'required|numeric',
            'question_id' => 'required|exists:questions,id',
            'sheet_id' => 'nullable|exists:sheets,id'
        ];
    }

    public function attributes(): array
    {
        return [];
    }
}