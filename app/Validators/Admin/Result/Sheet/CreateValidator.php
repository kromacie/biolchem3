<?php namespace App\Validators\Admin\Result\Sheet;

use App\Validators\Validator;

class CreateValidator extends Validator
{

    public function messages(): array
    {
        return [];
    }

    public function rules(): array
    {
        return [
            'sheet_id' => 'required|exists:sheets,id'
        ];
    }

    public function attributes(): array
    {
        return [];
    }
}