<?php namespace App\Validators\Admin\Reward;

use App\Validators\Validator;

class CreateValidator extends Validator
{

    public function messages(): array
    {
        return [];
    }

    public function rules(): array
    {
        return [
            'points' => 'required|numeric',
            'name' => 'required|string',
            'question_substitute_id' => 'required|exists:question_substitutes,id'
        ];
    }

    public function attributes(): array
    {
        return [];
    }
}