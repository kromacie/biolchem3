<?php namespace App\Validators\Admin\Sheet;

use App\Validators\Validator;

class CheckValidator extends Validator
{
    public function messages(): array
    {
        return [];
    }

    public function rules(): array
    {
        return [
            'answers.*.*.*.*' => 'nullable',
            'sheet_id' => 'required|exists:sheets,id',
            'user_id' => 'required|string'
        ];
    }

    public function attributes(): array
    {
        return [];
    }
}