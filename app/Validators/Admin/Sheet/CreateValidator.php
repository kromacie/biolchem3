<?php namespace App\Validators\Admin\Sheet;

use App\Validators\Validator;

class CreateValidator extends Validator
{

    public function messages(): array
    {
        return [];
    }

    public function rules(): array
    {
        return [
            'subject_id' => 'required|exists:subjects,id',
            'version_id' => 'required|exists:sheet_versions,id',
            'type_id' => 'required|exists:sheet_types,id',
            'tier_id' => 'required|exists:sheet_tiers,id',
            'release' => 'required|date',
            'enabled' => 'sometimes'
        ];
    }

    public function attributes(): array
    {
        return [];
    }

    public function mutate(array $validated)
    {
        $validated['enabled'] = isset($validated['enabled']);

        return $validated;
    }
}