<?php namespace App\Validators\Admin\Sheet;

use App\Validators\Validator;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class SearchValidator extends Validator
{
    public function messages(): array
    {
        return [];
    }

    public function rules(): array
    {
        return [
            'months' => 'sometimes|array',
            'years' => 'sometimes|array',
            'versions' => 'sometimes|array',
            'types' => 'sometimes|array',
            'tiers' => 'sometimes|array',
            'page' => 'required|numeric|min:1'
        ];
    }

    public function attributes(): array
    {
        return [];
    }

    public function mutate(array $validated)
    {
        $validated['months'] = isset($validated['months']) ? $validated['months'] : [];
        $validated['years'] = isset($validated['years']) ? $validated['years'] : [];
        $validated['versions'] = isset($validated['versions']) ? $validated['versions'] : [];
        $validated['types'] = isset($validated['types']) ? $validated['types'] : [];
        $validated['tiers'] = isset($validated['tiers']) ? $validated['tiers'] : [];

        $validated['months'] = (new Collection($validated['months']))->map(function ($month) {
            $month = Str::lower($month);

            if($month == 'styczen')
                return '01';
            if($month == 'luty')
                return '02';
            if($month == 'marzec')
                return '03';
            if($month == 'kwiecień')
                return '04';
            if($month == 'maj')
                return '05';
            if($month == 'czerwiec')
                return '06';
            if($month == 'lipiec')
                return '07';
            if($month == 'sierpień')
                return '08';
            if($month == 'wrzesień')
                return '09';
            if($month == 'październik')
                return '10';
            if($month == 'listopad')
                return '11';
            else
                return '12';
        })->toArray();

        return $validated;
    }
}