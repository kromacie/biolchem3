<?php namespace App\Validators\Admin\Sheet;

use App\Validators\Validator;

class ShowAllValidator extends Validator
{

    public function messages(): array
    {
        return [];
    }

    public function rules(): array
    {
        return [
            'subject_id' => 'sometimes|exists:subjects,id'
        ];
    }

    public function attributes(): array
    {
        return [];
    }
}