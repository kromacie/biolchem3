<?php namespace App\Validators\Admin\Sheet\Tier;

use App\Validators\Validator;

class CreateValidator extends Validator
{

    public function messages(): array
    {
        return [];
    }

    public function rules(): array
    {
        return [
            'name' => 'required|string|min:1'
        ];
    }

    public function attributes(): array
    {
        return [];
    }
}