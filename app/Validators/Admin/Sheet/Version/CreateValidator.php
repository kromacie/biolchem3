<?php namespace App\Validators\Admin\Sheet\Version;

use App\Validators\Validator;

class CreateValidator extends Validator
{

    private $except = 'null';

    /**
     * @param int $except
     * @return CreateValidator
     */
    public function except(int $except): CreateValidator
    {
        $this->except = $except;
        return $this;
    }

    public function messages(): array
    {
        return [];
    }

    public function rules(): array
    {
        return [
            'description' => 'required|string|min:1|unique:sheet_versions,description,' . $this->except,
            'short_description' => 'required|string|min:1',
            'display_stat' => 'sometimes',
            'display_name' => 'sometimes'
        ];
    }

    public function mutate(array $validated)
    {
        $validated['display_stat'] = isset($validated['display_stat']);
        $validated['display_name'] = isset($validated['display_name']);

        return $validated;
    }

    public function attributes(): array
    {
        return [];
    }
}