<?php namespace App\Validators\Admin\Subject;

use App\Validators\Validator;

class CreateValidator extends Validator
{

    private $id = 'null';

    /**
     * @param int $id
     * @return CreateValidator
     */
    public function except(int $id): self
    {
        $this->id = $id;

        return $this;
    }



    public function messages(): array
    {
        return [];
    }

    public function rules(): array
    {
        return [
            'name' => 'required|min:1|string|unique:subjects,name,' . $this->id . ',id',
            'icon' => 'nullable|string',
            'theme' => 'required|string',
            'enabled' => 'sometimes'
        ];
    }

    public function attributes(): array
    {
        return [];
    }

    public function mutate(array $validated)
    {
        $validated['enabled'] = isset($validated['enabled']);

        return $validated;
    }
}