<?php namespace App\Validators\Admin\Subject\Description;

use App\Validators\Validator;

class CreateValidator extends Validator
{

    public function messages(): array
    {
        return [];
    }

    public function rules(): array
    {
        return [
            'subject_id' => 'required|exists:subjects,id',
            'content' => 'required|string|min:1'
        ];
    }

    public function attributes(): array
    {
        return [];
    }
}