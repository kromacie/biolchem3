<?php namespace App\Validators\Admin\Tag;

use App\Validators\Validator;

class CreateValidator extends Validator
{

    private $id = null;

    public function except($id) {
        $this->id = $id; return $this;
    }

    public function messages(): array
    {
        return [];
    }

    public function rules(): array
    {
        return [
            'name' => 'required|unique:tags,name,' . $this->id ?? 'null' . ',id,tag_id,' . $this->input('tag_id', 'null'),
            'subject_id' => 'required|exists:subjects,id',
            'tag_id' => 'nullable|exists:tags,id',
        ];
    }

    public function attributes(): array
    {
        return [];
    }
}