<?php namespace App\Validators;



use Illuminate\Support\Fluent;
use Illuminate\Validation\Factory;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator as ValidatorInstance;

/**
 * Class Validator
 * @package App\Validators
 */
abstract class Validator
{

    protected $redirectTo;

    private $input;

    abstract public function messages(): array;
    abstract public function rules(): array;
    abstract public function attributes(): array;

    public function redirectTo()
    {
        return $this->redirectTo;
    }

    /**
     * @param array $data
     * @return array
     * @throws ValidationException
     */
    public function validate(array $data = [])
    {
        $validator = $this->getValidatorInstance()
            ->make($data, [], $this->messages(), $this->attributes());

        $this->setInput($validator->getData());

        $validator->setRules($this->rules());

        $this->withValidator($validator);

        try {
            if($validator->fails()) {
                $this->throwValidationException($validator);
            }

            return $this->mutate($validator->validated());
        } catch (ValidationException $exception) {
            $exception->redirectTo($this->redirectTo());

            throw $exception;
        }

    }

    private function setInput($data) {
        $this->input = new Fluent($data);
    }

    /**
     * @return Fluent
     */
    private function getInput() {
        return $this->input;
    }

    public function input($key, $default = null) {
        return $this->getInput()->get($key, $default);
    }

    public function mutate(array $validated) {
        return $validated;
    }

    public function throwValidationException(ValidatorInstance $validator)
    {
        throw ValidationException::withMessages($validator->errors()->messages());
    }

    public function withValidator(ValidatorInstance $validator) {}

    /**
     * @return Factory
     */
    public function getValidatorInstance()
    {
        return app()->make(Factory::class);
    }

    public static function make()
    {
        return new static();
    }
}