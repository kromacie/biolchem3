<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSheetVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sheet_versions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description');
            $table->string('short_description');
            $table->string('slug');
            $table->boolean('display_stat')->default(false);
            $table->boolean('display_name')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sheet_versions');
    }
}
