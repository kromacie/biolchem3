<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sheets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('release');
            $table->string('slug');
            $table->boolean('enabled')->default(false);
            $table->unsignedBigInteger('type_id');
            $table->unsignedBigInteger('version_id');
            $table->unsignedBigInteger('tier_id');
            $table->unsignedBigInteger('subject_id');

            $table->foreign('version_id')->references('id')->on('sheet_versions')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('type_id')->references('id')->on('sheet_types')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('subject_id')->references('id')->on('subjects')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('tier_id')->references('id')->on('sheet_tiers')
                ->onUpdate('restrict')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sheets');
    }
}
