<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInputsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inputs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('input_group_id');
            $table->unsignedBigInteger('type_id')->nullable();
            $table->text('description')->nullable();
            $table->text('placeholder')->nullable();
            $table->boolean('disabled')->default(false);
            $table->boolean('checked')->default(false);
            $table->boolean('is_description')->default(false);

            $table->foreign('input_group_id')->references('id')->on('input_groups')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('type_id')->references('id')->on('input_types')
                ->onDelete('set null')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inputs');
    }
}
