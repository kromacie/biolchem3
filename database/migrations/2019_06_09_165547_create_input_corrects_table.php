<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInputCorrectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('input_corrects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('input_id');
            $table->string('value', 255);

            $table->foreign('input_id')->references('id')->on('inputs')
                ->onUpdate('cascade')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('input_corrects');
    }
}
