<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionSubstituteHasInputGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_substitute_has_input_groups', function (Blueprint $table) {
            $table->unsignedBigInteger('question_substitute_id');
            $table->unsignedBigInteger('input_group_id');

            $table->foreign('question_substitute_id', 'qsa_id_foreign')->references('id')->on('question_substitutes')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('input_group_id')->references('id')->on('input_groups')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_substitute_has_input_groups');
    }
}
