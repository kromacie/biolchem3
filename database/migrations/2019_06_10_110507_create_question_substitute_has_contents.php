<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionSubstituteHasContents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_substitute_has_contents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('question_substitute_id');
            $table->unsignedBigInteger('content_id');

            $table->foreign('question_substitute_id')->references('id')->on('question_substitutes')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('content_id')->references('id')->on('contents')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_substitute_has_contents');
    }
}
