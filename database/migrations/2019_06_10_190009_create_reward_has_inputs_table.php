<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRewardHasInputsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reward_has_inputs', function (Blueprint $table) {
            $table->unsignedBigInteger('reward_id');
            $table->unsignedBigInteger('input_id');

            $table->unique(['reward_id', 'input_id']);

            $table->foreign('reward_id')->references('id')->on('rewards')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('input_id')->references('id')->on('inputs')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rewards_has_inputs');
    }
}
