<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResolvedQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resolved_questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('got_result');
            $table->unsignedInteger('max_result');
            $table->unsignedBigInteger('question_id');
            $table->unsignedBigInteger('sheet_id')->nullable();

            $table->foreign('question_id')->references('id')->on('questions')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('sheet_id')->references('id')->on('sheets')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resolved_questions');
    }
}
