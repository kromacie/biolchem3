<?php

use Illuminate\Database\Seeder;

class CreateInputGroupTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws Throwable
     */
    public function run()
    {

        $types = app()->make(\App\Services\InputGroupTypeService::class);

        $types->adminCreate([
            'name' => 'SINGULAR_TEXT',
            'input_class' => 'input-singular-text',
            'group_class' => 'input-singular-text-container'
        ]);
        $types->adminCreate([
            'name' => 'MULTIPLE_RADIO',
            'input_class' => 'input-radio-checkmark',
            'group_class' => 'input-radio-container'
        ]);
        $types->adminCreate([
            'name' => 'MULTIPLE_TEXT',
            'input_class' => 'input-multiple-text',
            'group_class' => 'input-multiple-text-container'
        ]);
        $types->adminCreate([
            'name' => 'SINGULAR_LETTER',
            'input_class' => 'input-singular-letter',
            'group_class' => 'input-singular-letter-container'
        ]);
        $types->adminCreate([
            'name' => 'MULTIPLE_LETTER',
            'input_class' => 'input-multiple-letter',
            'group_class' => 'input-multiple-letter-container'
        ]);
        $types->adminCreate([
            'name' => 'SINGULAR_CHECKBOX',
            'input_class' => 'input-checkbox',
            'group_class' => 'input-checkbox-container'
        ]);

    }
}
