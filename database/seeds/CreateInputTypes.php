<?php

use Illuminate\Database\Seeder;

class CreateInputTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws Throwable
     */
    public function run()
    {
        $types = app()->make(\App\Services\InputTypeService::class);

        $types->adminCreate([
            'name' => 'SUPERSCRIPT',
            'class' => 'input-sup'
        ]);

        $types->adminCreate([
            'name' => 'SUBSCRIPT',
            'class' => 'input-sub'
        ]);

        $types->adminCreate([
            'name' => 'SUPERSCRIPT-H',
            'class' => 'input-sup half'
        ]);

        $types->adminCreate([
            'name' => 'SUBSCRIPT-H',
            'class' => 'input-sub half'
        ]);

        $types->adminCreate([
            'name' => 'SUPERSCRIPT-3',
            'class' => 'input-sup three'
        ]);

        $types->adminCreate([
            'name' => 'SUBSCRIPT-3',
            'class' => 'input-sub three'
        ]);

        $types->adminCreate([
            'name' => 'SUPERSCRIPT-4',
            'class' => 'input-sup four'
        ]);

        $types->adminCreate([
            'name' => 'SUBSCRIPT-4',
            'class' => 'input-sub four'
        ]);
        $types->adminCreate([
            'name' => 'H',
            'class' => 'half'
        ]);
        $types->adminCreate([
            'name' => '3',
            'class' => 'three'
        ]);
        $types->adminCreate([
            'name' => '4',
            'class' => 'four'
        ]);
    }
}
