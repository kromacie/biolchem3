<?php

use Illuminate\Database\Seeder;

class CreateSheetTiers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tiers = app()->make(\App\Services\SheetTierService::class);

        $tiers->adminCreate([
            'name' => 'Poziom rozszerzony'
        ]);
        $tiers->adminCreate([
            'name' => 'Poziom podstawowy'
        ]);
    }
}
