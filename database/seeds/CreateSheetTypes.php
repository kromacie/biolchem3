<?php

use Illuminate\Database\Seeder;

class CreateSheetTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function run()
    {
        $types = app()->make(\App\Services\SheetTypeService::class);

        $types->adminCreate([
            'name' => 'Oficialny'
        ]);

        $types->adminCreate([
            'name' => 'Podstawowy'
        ]);
    }
}
