<?php

use Illuminate\Database\Seeder;

class CreateSheetVersions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function run()
    {
        $versions = app()->make(\App\Services\SheetVersionService::class);

        $versions->adminCreate([
            'description' => 'Nowa matura',
            'short_description' => '(nowa)',
            'display_stat' => false,
            'display_name' => true
        ]);

        $versions->adminCreate([
            'description' => 'Stara matura',
            'short_description' => '(stara)',
            'display_stat' => false,
            'display_name' => false
        ]);
    }
}
