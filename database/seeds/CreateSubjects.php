<?php

use App\Services\SubjectService;
use Illuminate\Database\Seeder;

class CreateSubjects extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function run()
    {
        $subjects = app()->make(SubjectService::class);

        $subjects->adminCreate([
            'name' => 'Biologia',
            'icon' => 'icon-dna',
            'theme' => 'secondary'
        ]);

        $subjects->adminCreate([
            'name' => 'Chemia',
            'icon' => 'icon-flask',
            'theme' => 'primary'
        ]);
    }
}
