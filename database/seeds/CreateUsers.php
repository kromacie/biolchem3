<?php

use Illuminate\Database\Seeder;

class CreateUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $user->fill([
            'name' => 'Maciej',
            'password' => '$2y$10$i8Pofo71cRRonZ/CFlpXM.WPNOWjo3oF11/tyuYoH2Dez3Iacl0LS',
            'email' => 'kromacie@gmail.com',
            'email_verified_at' => \Carbon\Carbon::now()
        ]);
        $user->save();
    }
}
