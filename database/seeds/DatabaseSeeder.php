<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CreateInputGroupTypes::class);
        $this->call(CreateInputTypes::class);
        $this->call(CreateSubjects::class);
        $this->call(CreateSheetTiers::class);
        $this->call(CreateSheetTypes::class);
        $this->call(CreateSheetVersions::class);
        $this->call(CreateUsers::class);
    }
}
