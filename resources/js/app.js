/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

$('.expand-toggler').on('click', function () {
    $($(this).data("target")).toggleClass('is-expanded');
    $(this).toggleClass('is-expanded');
});

$('.tag-filters-close-toggle').on('click', function () {
    $(this).parent().remove()
});

$('.input-checkbox-toggler').on('click', function () {
    let label = $(this).data("target");
    let checkboxes = $(label).find('input[type=checkbox]');
    checkboxes.prop('checked', $(this).find('.input-checkbox').is(':checked'));
});
$('.article-image-container').on('click', function () {
   $(this).toggleClass('is-zoomed');
});
$('.modal-expander').on('click', function () {
    $($(this).data("target")).toggleClass('is-expanded');
});
$('.modal-close').on('click', function () {
    $(this).closest('.modal').toggleClass('is-expanded');
});
$('.input-upload-close').on('click', function () {

});

function previewInputImage(input) {

    if (input.files && input.files[0]) {

        let reader = new FileReader();
        reader.onload = function (e) {
            console.log($(input).data("target"));
            $($(input).data("target")).attr('src', e.target.result);
            $($(input).data("target")).parent().toggleClass('is-expanded');

        };

        reader.readAsDataURL(input.files[0]);
    }
}

$(".input-image-preview").change(function(){
    previewInputImage(this);
});

$('.form-reset').on('click', function () {
   $(this).closest('.form').trigger('reset');
});

$('.debug-expander').on('click', function () {
    $($(this).data('target')).toggleClass('is-visible');
    $(this).toggleClass('is-visible');
});

let callbacks = [];

window.ajaxLoaderDone = function(callback, id) {
    callbacks[id] = callback;
}

$(".ajax-loader").on('change', function () {
    let select = $(this);

    let value = select.find('option:selected').val();
    let url = select.data('url');
    let method = select.data('method');

    let name = select.attr('name');
    let id = select.attr('id');

    let data = {};
    data[name] = value;

    let request = $.ajax({
        url: url,
        data: data,
        cache: false,
        type: method
    });

    if(id != null && callbacks[id] != null) {
        request.done(callbacks[id]);
    }

    request.done();
});
