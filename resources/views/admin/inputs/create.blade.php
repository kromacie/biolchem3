@extends('layouts.admin', ['title' => 'Stwórz typ inputa'])

@section('content')
    <div class="button-container">
        <a class="button" href="{{ route('inputs.types.index') }}">Wróć</a>
    </div>
    @if($errors->any())
        <div class="notifier">
            <div class="notifier-header">
                <h3 class="notifier-title">Powiadomienia</h3>
            </div>
            <div class="notifier-body">
                @foreach($errors->all() as $error)
                <span class="notifier-item">{{ $error }}</span>
                @endforeach
            </div>
        </div>
    @endif
    <div class="form-container">
        <form class="form" action="{{ route('inputs.types.store') }}" method="post">
            @csrf
            <label class="input-container">
                Nazwa
                <input name="name" class="input-admin-text" type="text">
            </label>
            <label class="input-container">
                Klasa
                <input name="class" class="input-admin-text" type="text">
            </label>
            <div class="button-container">
                <button class="button" type="submit">
                    Dodaj
                </button>
            </div>
        </form>
    </div>
@endsection
