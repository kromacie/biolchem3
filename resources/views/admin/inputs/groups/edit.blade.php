@extends('layouts.admin', ['title' => 'Edytuj typ grupy inputa'])

@section('content')
    <div class="button-container">
        <a class="button" href="{{ route('inputs.groups.types.index') }}">Wróć</a>
    </div>
    @if($errors->any())
        <div class="notifier">
            <div class="notifier-header">
                <h3 class="notifier-title">Powiadomienia</h3>
            </div>
            <div class="notifier-body">
                @foreach($errors->all() as $error)
                    <span class="notifier-item">{{ $error }}</span>
                @endforeach
            </div>
        </div>
    @endif
    <div class="form-container">
        <form class="form" action="{{ route('inputs.groups.types.update', $type->id) }}" method="post">
            @csrf
            @method('PUT')
            <label class="input-container">
                Nazwa
                <input name="name" class="input-admin-text" type="text" value="{{ $type->name }}">
            </label>
            <label class="input-container">
                Klasa grupy
                <input name="group_class" class="input-admin-text" type="text" value="{{ $type->group_class }}">
            </label>
            <label class="input-container">
                Klasa inputa
                <input name="input_class" class="input-admin-text" type="text" value="{{ $type->input_class }}">
            </label>
            <div class="button-container">
                <button class="button" type="submit">
                    Dodaj
                </button>
            </div>
        </form>
    </div>
@endsection
