@extends('layouts.admin', ['title' => 'Typy group inputów'])

@section('content')
<div class="button-container">
    <a class="button" href="{{ route('inputs.groups.types.create') }}">Dodaj nowy typ</a>
</div>
<table class="table">
    <tr class="table-head">
        <th>ID</th>
        <th>Nazwa</th>
        <th>Klasa grupy</th>
        <th>Klasa inputa</th>
        <th>Actions</th>
    </tr>
    @foreach($types as $type)
    <tr>
        <td>{{ $type->id }}</td>
        <td>{{ $type->name }}</td>
        <td>{{ $type->group_class }}</td>
        <td>{{ $type->input_class }}</td>
        <td>
            <button class="mini-link mini-link-delete" type="submit" form="tag-delete-{{ $type->id }}"></button>
            <form id="tag-delete-{{ $type->id }}" method="post" action="{{ route('inputs.groups.types.destroy', $type->id) }}" style="display: none;">
                @csrf
                @method('DELETE')
            </form>
            <a class="mini-link mini-link-edit" href="{{ route('inputs.groups.types.edit', $type->id) }}"></a>
        </td>
    </tr>
    @endforeach
</table>
@endsection