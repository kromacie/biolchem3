@extends('layouts.admin', ['title' => 'Stwórz pytanie'])

@section('content')
    <div class="button-container">
        <a class="button" href="{{ route('sheets.questions.index', $sheet->id) }}">Wróć</a>
    </div>
    @if($errors->any())
    <div class="notifier">
        <div class="notifier-header">
            <h3 class="notifier-title">Powiadomienia</h3>
        </div>
        <div class="notifier-body">
            <span class="notifier-item">Wszystko jest w porządku</span>
            <span class="notifier-item">Wszystko jest w porządku</span>
            <span class="notifier-item">Wszystko jest w porządku</span>
        </div>
    </div>
    @endif
    <div class="form-container">
        <form class="form" action="{{ route('sheets.questions.store', $sheet->id) }}" method="post">
            @csrf
            <input type="hidden" name="sheet_id" value="{{ $sheet->id }}">
            <label class="input-container">
                Przedmiot
                <input class="input-admin-text" type="text" disabled value="{{ $sheet->subject->name }}">
            </label>
            <label class="input-container">
                Arkusz
                <input id="question-sheets" class="input-admin-text" disabled value="{{ $sheet->release }}">
            </label>
            <label class="input-container">
                Tytuł
                <input name="title" class="input-admin-text" type="text">
            </label>
            <label class="input-container input-checkbox-container">
                Czy włączony?
                <input name="enabled" class="input-checkbox" type="checkbox">
                <span class="input-checkmark"></span>
            </label>
            <div class="button-container">
                <button class="button" type="submit">
                    Dodaj
                </button>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script>
        $(function () {
            ajaxLoaderDone(function(response){
                let sheets = $("#question-sheets");
                    sheets.empty();

                    $.each(response, function (index, value) {
                        let option = $('<option/>');
                            option.attr({value: value.id}).text(value.release);
                            sheets.append(option);
                    });
            }, 'question-select');

            $(".ajax-loader").trigger('change');
        });
    </script>
@endsection