@extends('layouts.admin', ['title' => 'Edytuj pytanie'])

@section('content')
    <div class="button-container">
        <a class="button" href="{{ route('sheets.questions.index', $sheet->id) }}">Wróć</a>
    </div>
    @if($errors->any())
        <div class="notifier">
            <div class="notifier-header">
                <h3 class="notifier-title">Powiadomienia</h3>
            </div>
            <div class="notifier-body">
                @foreach($errors->all() as $error)
                <span class="notifier-item">{{ $error }}</span>
                @endforeach
            </div>
        </div>
    @endif
    <div class="form-container">
        <form class="form" action="{{ route('sheets.questions.update', [$sheet->id, $question->id]) }}" method="post">
            @csrf
            @method('PUT')
            <input type="hidden" name="sheet_id" value="{{ $sheet->id }}">
            <label class="input-container">
                ID
                <input disabled name="title" class="input-admin-text" type="text" value="{{ $question->id }}">
            </label>
            <label class="input-container">
                Tytuł
                <input name="title" class="input-admin-text" type="text" value="{{ $question->title }}">
            </label>
            <label class="input-container input-checkbox-container">
                Czy włączony?
                <input name="enabled" class="input-checkbox" type="checkbox" {{ $question->enabled ? 'checked' : '' }}>
                <span class="input-checkmark"></span>
            </label>
            <div class="button-container">
                <button class="button" type="submit">
                    Edytuj
                </button>
            </div>
        </form>
    </div>
@endsection