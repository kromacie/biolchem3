@extends('layouts.admin', ['title' => 'Edytor'])

@section('content')
    <div class="button-container">
        <a class="button" href="{{ route('sheets.questions.index', $sheet->id) }}">Wróć</a>
    </div>
    @if($errors->any())
        <div class="notifier">
            <div class="notifier-header">
                <h3 class="notifier-title">Powiadomienia</h3>
            </div>
            <div class="notifier-body">
                @foreach($errors->all() as $error)
                    <span class="notifier-item">{{ $error }}</span>
                @endforeach
            </div>
        </div>
    @endif
    <div>
        <span class="debug-expander is-visible" data-target="#debug">
            Debugowanie: <i class="debug-expander-icon"></i>
        </span>
    </div>
    <div id="debug" class="debug-container debug is-visible debug-question">
        <article class="article article-no-gutters-top editor-editable">
            <div class="editor-actions">
                <button class="mini-link mini-link-edit modal-expander" data-target="#edit-question"></button>
                <button class="mini-link mini-link-plus-square modal-expander" form="create-question-substitute"></button>
                <button class="mini-link mini-link-sheet modal-expander" data-target="#create-question-content"></button>
                <button class="mini-link mini-link-tag modal-expander" data-target="#create-question-tag"></button>
                <form id="create-question-substitute" class="form" action="{{ route('sheets.questions.substitutes.store', [$sheet->id, $question->id]) }}" method="post">
                    @csrf
                </form>
            </div>
            <div class="mini-statistic-container">
                <div class="mini-statistic">
                                <span class="mini-statistic-icon icon icon-user">
                                    <span class="mini-statistic-icon-description">
                                        Rozwiązanych arkuszy
                                    </span>
                                </span>
                    <span class="mini-statistic-value">12</span>
                </div>
                <div class="mini-statistic">
                                <span class="mini-statistic-icon icon icon-calendar-check">
                                    <span class="mini-statistic-icon-description">
                                        Średni wynik
                                    </span>
                                </span>
                    <span class="mini-statistic-value">76%</span>
                </div>
                @if($sheet->type->display_stat)
                    <div class="mini-statistic mini-statistic-right">
                                    <span class="mini-statistic-icon icon icon-info-circle">
                                    </span>
                        <span class="mini-statistic-value">{{ $sheet->type->description }}</span>
                    </div>
                @endif
            </div>
            <div class="article-header article-header-has-number">
                <span class="article-number article-number-primary">1</span>
                <h2 class="article-title title">
                    Matura {{ $sheet->release }}, Poziom rozszerzony {{ $sheet->type->display_name ? $sheet->type->short_description : '' }}<br />
                    <a class="primary" href="">{{ $question->title }} ({{ $question->points }} pkt)</a>
                </h2>
            </div>
            <div class="article-header">
                <div class="tags">
                    @foreach($question->tags as $tag)
                        <span class="tag tag-primary">{{ $tag->name }} <button class="icon icon-times" form="delete-question-tag-{{ $question->id }}-{{ $tag->id }}"></button></span>
                        <form id="delete-question-tag-{{ $question->id }}-{{ $tag->id }}" style="display: none" method="post" action="{{ route('sheets.questions.tags.destroy', [$sheet->id, $question->id, $tag->id]) }}">
                            @csrf
                            @method('DELETE')
                        </form>
                    @endforeach
                </div>
            </div>
            <div class="article-body">
                @foreach($question->contents as $content)
                    <div class="editor-editable debug debug-question-content">
                        <div class="editor-actions">
                            <button class="mini-link mini-link-edit modal-expander" data-target="#edit-question-content-{{ $content->id }}"></button>
                            <button class="mini-link mini-link-delete" form="question-content-{{ $content->id }}"></button>
                            <form id="question-content-{{ $content->id }}" method="post" action="{{ route('sheets.questions.contents.destroy', [$sheet->id, $question->id, $content->id]) }}">
                                @csrf
                                @method('DELETE')
                            </form>
                        </div>
                        <div id="edit-question-content-{{ $content->id }}" class="modal">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title">Edytuj zawartość pytania</h3>
                                    <button class="modal-close"></button>
                                </div>
                                <div class="modal-body">
                                    <form class="form" action="{{ route('sheets.questions.contents.update', [$sheet->id, $question->id, $content->id]) }}" method="post">
                                        @csrf
                                        @method('PUT')
                                        <label class="input-container">
                                            Tekst
                                            <textarea class="input-admin-text" name="description" id="" cols="30" rows="10">{{ $content->description }}</textarea>
                                        </label>
                                        <div class="button-container">
                                            <button class="button" type="submit">Edytuj</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @if($content->description)
                            <div class="article-text-intended">
                                @markdown($content->description)
                            </div>
                        @endif
                        @if($content->image_id)
                            <div class="article-image-container article-image-container-half">
                                <img class="article-image" src="{{ asset('storage/' . $content->image->path) }}" alt="">
                            </div>
                        @endif
                    </div>
                @endforeach
                @foreach($question->substitutes as $index => $substitute)
                    <div class="editor-editable debug debug-question-substitute">
                        <div class="editor-actions">
                            <button class="mini-link mini-link-sheet modal-expander" data-target="#create-question-substitute-content-{{ $substitute->id }}"></button>
                            <button class="mini-link mini-link-plus-square modal-expander" data-target="#create-question-substitute-input-group-{{ $substitute->id }}"></button>
                            <button class="mini-link mini-link-reward modal-expander" data-target="#create-question-substitute-reward-{{ $substitute->id }}"></button>
                            <button class="mini-link mini-link-delete" form="question-substitute-{{ $substitute->id }}"></button>
                            <form id="question-substitute-{{ $substitute->id }}" method="post" action="{{ route('sheets.questions.substitutes.destroy', [$sheet->id, $question->id, $substitute->id]) }}">
                                @csrf
                                @method('DELETE')
                            </form>
                        </div>
                        <div class="editor editor-rewards">
                            @foreach($substitute->rewards as $reward)
                                <div class="editor-rewards-item">
                                    <span>{{ $reward->name }}: {{ $reward->points }}pkt.</span> <button class="icon icon-times" form="questions-substitutes-rewards-{{ $reward->id }}"></button>
                                    <form id="questions-substitutes-rewards-{{ $reward->id }}" method="post" action="{{ route('sheets.questions.substitutes.rewards.destroy', [$sheet->id, $question->id, $substitute->id, $reward->id]) }}">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                </div>
                            @endforeach
                        </div>
                        <div id="create-question-substitute-content-{{ $substitute->id }}" class="modal">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title">Dodaj zawartość pytania podrzędnego</h3>
                                    <button class="modal-close"></button>
                                </div>
                                <div class="modal-body">
                                    <form class="form" action="{{ route('sheets.questions.substitutes.contents.store', [$sheet->id, $question->id, $substitute->id]) }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <label class="input-container">
                                            Tekst
                                            <textarea class="input-admin-text" name="description" id="" cols="30" rows="10"></textarea>
                                        </label>
                                        <label class="input-container input-container-upload">
                                            Obrazek
                                            <input class="input-upload input-image-preview" type="file" name="image" data-target="#question-create-image-preview">
                                        </label>
                                        <div class="article-image-container article-image-container-half is-hidden">
                                            <img id="question-create-image-preview" class="article-image" src="" alt="Obrazek">
                                        </div>
                                        <div class="button-container">
                                            <button class="button" type="submit">Dodaj</button>
                                            <span class="button form-reset">Resetuj</span>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div id="create-question-substitute-reward-{{ $substitute->id }}" class="modal">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title">Dodaj nagordę dla podpytania!</h3>
                                    <button class="modal-close"></button>
                                </div>
                                <div class="modal-body">
                                    <form class="form" action="{{ route('sheets.questions.substitutes.rewards.store', [$sheet->id, $question->id, $substitute->id]) }}" method="post">
                                        @csrf
                                        <input type="hidden" name="question_substitute_id" value="{{ $substitute->id }}">
                                        <label class="input-container">
                                            Nazwa
                                            <input class="input-admin-text" name="name">
                                        </label>
                                        <label class="input-container">
                                            Punkty
                                            <input class="input-admin-text" name="points">
                                        </label>
                                        <div class="button-container">
                                            <button class="button" type="submit">Dodaj</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div id="create-question-substitute-input-group-{{ $substitute->id }}" class="modal">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title">Dodaj grupę pytań</h3>
                                    <button class="modal-close"></button>
                                </div>
                                <div class="modal-body">
                                    <form class="form" action="{{ route('sheets.questions.substitutes.inputGroups.store', [$sheet->id, $question->id, $substitute->id]) }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <label class="input-container">
                                            Typ
                                            <select class="input-admin-text" name="type_id" id="">
                                                @foreach($group_types as $type)
                                                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                                                @endforeach
                                            </select>
                                        </label>
                                        <label class="input-container">
                                            Opis
                                            <textarea class="input-admin-text" name="description" cols="30" rows="10"></textarea>
                                        </label>
                                        <div class="button-container">
                                            <button class="button" type="submit">Dodaj</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="question">
                            <div class="question-header">
                                <h2 class="question-title">{{ $question->title . ($index + 1) }}</h2>
                                <span class="question-points">
                                    {{ trans_choice('questions.points', $substitute->points, ['value' => $substitute->points]) }}
                                </span>
                            </div>
                            <div class="question-body">
                                @foreach($substitute->contents as $content)
                                    <div class="debug debug-question-content editor-editable">
                                        <div class="editor-actions">
                                            <button class="mini-link mini-link-edit modal-expander" data-target="#edit-question-substitute-content-{{ $content->id }}"></button>
                                            <button class="mini-link mini-link-delete" form="question-substitute-content-{{ $content->id }}"></button>
                                            <form id="question-substitute-content-{{ $content->id }}" method="post" action="{{ route('sheets.questions.substitutes.contents.destroy', [$sheet->id, $question->id, $substitute->id, $content->id]) }}">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </div>
                                        @if($content->description)
                                            @markdown($content->description)
                                        @endif
                                        @if($content->image_id)
                                            <div class="article-image-container article-image-container-half">
                                                <img class="article-image" src="{{ asset('storage/' .$content->image->path) }}" alt="">
                                            </div>
                                        @endif
                                    </div>
                                    <div id="edit-question-substitute-content-{{ $content->id }}" class="modal">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h3 class="modal-title">Edytuj zawartość pytania podrzędnego</h3>
                                                <button class="modal-close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <form class="form" action="{{ route('sheets.questions.substitutes.contents.update', [$sheet->id, $question->id, $substitute->id, $content->id]) }}" method="post">
                                                    @csrf
                                                    @method('PUT')
                                                    <label class="input-container">
                                                        Tekst
                                                        <textarea class="input-admin-text" name="description" id="" cols="30" rows="10">{{ $content->description }}</textarea>
                                                    </label>
                                                    <div class="button-container">
                                                        <button class="button" type="submit">Edytuj</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="answer-container">
                                    <h3 class="answer-title">
                                        Odpowiedzi
                                    </h3>
                                        <div class="answer">
                                            @foreach($substitute->inputGroups as $group)
                                                <div class="editor-editable debug debug-input-group">
                                                    <div class="editor-actions">
                                                        <button class="mini-link mini-link-edit modal-expander" data-target="#edit-question-substitute-input-group-{{ $group->id }}"></button>
                                                        <button class="mini-link mini-link-plus-square modal-expander" data-target="#create-question-substitute-input-{{ $group->id }}"></button>
                                                        <button class="mini-link mini-link-delete" form="question-substitute-input-group-{{ $group->id }}"></button>
                                                        <form id="question-substitute-input-group-{{ $group->id }}" method="post" action="{{ route('sheets.questions.substitutes.inputGroups.destroy', [$sheet->id, $question->id, $substitute->id, $group->id]) }}">
                                                            @csrf
                                                            @method('DELETE')
                                                        </form>
                                                    </div>
                                                    <div id="edit-question-substitute-input-group-{{ $group->id }}" class="modal">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h3 class="modal-title">Edytuj zawartość grupy</h3>
                                                                <button class="modal-close"></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form class="form" action="{{ route('sheets.questions.substitutes.inputGroups.update', [$sheet->id, $question->id, $substitute->id, $group->id]) }}" method="post">
                                                                    @csrf
                                                                    @method('PUT')
                                                                    <label class="input-container">
                                                                        Typ
                                                                        <select class="input-admin-text" name="type_id" id="">
                                                                            @foreach($group_types as $type)
                                                                                <option value="{{ $type->id }}" {{ $type->id == $group->type_id ? 'selected' : '' }}>{{ $type->name }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </label>
                                                                    <label class="input-container">
                                                                        Opis
                                                                        <textarea class="input-admin-text" name="description" id="" cols="30"
                                                                                  rows="10">{{ $group->description }}</textarea>
                                                                    </label>
                                                                    <div class="button-container">
                                                                        <button class="button" type="submit">Edytuj</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="create-question-substitute-input-{{ $group->id }}" class="modal">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h3 class="modal-title">Dodaj odpowiedź</h3>
                                                                <button class="modal-close"></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form class="form" action="{{ route('sheets.questions.substitutes.inputs.store', [$sheet->id, $question->id, $substitute->id]) }}" method="post">
                                                                    @csrf
                                                                    <input type="hidden" name="input_group_id" value="{{ $group->id }}">
                                                                    <label class="input-container">
                                                                        Typ
                                                                        <select class="input-admin-text" name="type_id" id="">
                                                                            <option value="">NORMAL</option>
                                                                            @foreach($input_types as $type)
                                                                                <option value="{{ $type->id }}">{{ $type->name }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </label>
                                                                    <label class="input-container">
                                                                        Opis
                                                                        <textarea class="input-admin-text" name="description" id="" cols="30"
                                                                                  rows="10"></textarea>
                                                                    </label>
                                                                    <label class="input-container">
                                                                        Placeholder
                                                                        <input class="input-admin-text" name="placeholder">
                                                                    </label>
                                                                    <label class="input-container input-checkbox-container">
                                                                        Czy wyłączony?
                                                                        <input class="input-checkbox" type="checkbox" name="disabled">
                                                                        <span class="input-checkmark"></span>
                                                                    </label>
                                                                    <label class="input-container input-checkbox-container">
                                                                        Czy zaznaczony?
                                                                        <input class="input-checkbox" type="checkbox" name="checked">
                                                                        <span class="input-checkmark"></span>
                                                                    </label>
                                                                    <label class="input-container input-checkbox-container">
                                                                        Czy jest opisem?
                                                                        <input class="input-checkbox" type="checkbox" name="is_description">
                                                                        <span class="input-checkmark"></span>
                                                                    </label>

                                                                    <div class="button-container">
                                                                        <button class="button" type="submit">Dodaj</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @if($group->description)
                                                        @markdown($group->description)
                                                    @endif

                                                    <div class="input-group">
                                                        @foreach($group->inputs as $input)
                                                            <div class="debug debug-input editor-editable">
                                                                <div class="editor-actions">
                                                                    <button class="mini-link mini-link-edit modal-expander" data-target="#edit-question-substitute-input-{{ $input->id }}"></button>
                                                                    <button class="mini-link mini-link-reward modal-expander" data-target="#create-question-substitute-input-reward-{{ $input->id }}"></button>
                                                                    <button class="mini-link mini-link-correct modal-expander" data-target="#create-question-substitute-input-correct-{{ $input->id }}"></button>
                                                                    <button class="mini-link mini-link-delete" form="question-substitute-input-{{ $input->id }}"></button>
                                                                    <form id="question-substitute-input-{{ $input->id }}" method="post" action="{{ route('sheets.questions.substitutes.inputs.destroy', [$sheet->id, $question->id, $substitute->id, $input->id]) }}">
                                                                        @csrf
                                                                        @method('DELETE')
                                                                    </form>
                                                                </div>
                                                                <div id="edit-question-substitute-input-{{ $input->id }}" class="modal">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h3 class="modal-title">Edytuj odpowiedź</h3>
                                                                            <button class="modal-close"></button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form class="form" action="{{ route('sheets.questions.substitutes.inputs.update', [$sheet->id, $question->id, $substitute->id, $input->id]) }}" method="post">
                                                                                @csrf
                                                                                @method('PUT')
                                                                                <input type="hidden" name="input_group_id" value="{{ $group->id }}">
                                                                                <label class="input-container">
                                                                                    Typ
                                                                                    <select class="input-admin-text" name="type_id" id="">
                                                                                        <option value="">NORMAL</option>
                                                                                        @foreach($input_types as $type)
                                                                                            <option value="{{ $type->id }}" {{ $type->id == $input->type_id ? 'selected' : '' }}>{{ $type->name }}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </label>
                                                                                <label class="input-container">
                                                                                    Opis
                                                                                    <textarea class="input-admin-text" name="description" id="" cols="30"
                                                                                              rows="10">{{ $input->description }}</textarea>
                                                                                </label>
                                                                                <label class="input-container">
                                                                                    Placeholder
                                                                                    <input class="input-admin-text" name="placeholder" value="{{ $input->placeholder }}">
                                                                                </label>
                                                                                <label class="input-container input-checkbox-container">
                                                                                    Czy wyłączony?
                                                                                    <input class="input-checkbox" type="checkbox" name="disabled" {{ $input->disabled ? 'checked' : '' }}>
                                                                                    <span class="input-checkmark"></span>
                                                                                </label>
                                                                                <label class="input-container input-checkbox-container">
                                                                                    Czy zaznaczony?
                                                                                    <input class="input-checkbox" type="checkbox" name="checked" {{ $input->checked ? 'checked' : '' }}>
                                                                                    <span class="input-checkmark"></span>
                                                                                </label>
                                                                                <label class="input-container input-checkbox-container">
                                                                                    Czy jest opisem?
                                                                                    <input class="input-checkbox" type="checkbox" name="is_description" {{ $input->is_description ? 'checked' : '' }}>
                                                                                    <span class="input-checkmark"></span>
                                                                                </label>

                                                                                <div class="button-container">
                                                                                    <button class="button" type="submit">Edytuj</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div id="create-question-substitute-input-reward-{{ $input->id }}" class="modal">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h3 class="modal-title">Dodaj nagrodę dla poprawnej odpowiedzi</h3>
                                                                            <button class="modal-close"></button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form class="form" action="{{ route('sheets.questions.substitutes.inputs.rewards.store', [$sheet->id, $question->id, $substitute->id, $input->id]) }}" method="post">
                                                                                @csrf
                                                                                <input type="hidden" name="input_id" value="{{ $input->id }}">
                                                                                <label class="input-container">
                                                                                    Nagorda
                                                                                    <select class="input-admin-text" name="reward_id" id="">
                                                                                        @foreach($substitute->rewards as $reward)
                                                                                            <option value="{{ $reward->id }}">{{ $reward->name }} ({{ $reward->points }}pkt)</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </label>

                                                                                <div class="button-container">
                                                                                    <button class="button" type="submit">Dodaj</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div id="create-question-substitute-input-correct-{{ $input->id }}" class="modal">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h3 class="modal-title">Dodaj poprawną odpowiedź</h3>
                                                                            <button class="modal-close"></button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form class="form" action="{{ route('sheets.questions.substitutes.inputs.corrects.store', [$sheet->id, $question->id, $substitute->id, $input->id]) }}" method="post">
                                                                                @csrf
                                                                                <input type="hidden" value="{{ $input->id }}" name="input_id">
                                                                                <label class="input-container">
                                                                                    Wartość
                                                                                    <input type="text" class="input-admin-text" name="value">
                                                                                </label>

                                                                                <div class="button-container">
                                                                                    <button class="button" type="submit">Dodaj</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                @include('components.input', ['group' => $group, 'input' => $input, 'question' => $question, 'substitute' => $substitute])
                                                                <div class="editor-container">
                                                                    <div class="editor-rewards">
                                                                        <div>Nagrody</div>
                                                                        @foreach($input->rewards as $reward)
                                                                            <div class="editor-rewards-item">{{ $reward->name }} {{ $reward->points }}pkt <button form="question-substitute-input-reward-{{ $input->id }}-{{ $reward->id }}" class="icon icon-times"></button></div>
                                                                            <form style="display: none;" id="question-substitute-input-reward-{{ $input->id }}-{{ $reward->id }}" method="post" action="{{ route('sheets.questions.substitutes.inputs.rewards.destroy', [$sheet->id, $question->id, $substitute->id, $input->id, $reward->id]) }}">
                                                                                @csrf
                                                                                @method('DELETE')
                                                                            </form>
                                                                        @endforeach
                                                                    </div>
                                                                    <div class="editor-corrects">
                                                                        <div>Odpowiedzi</div>
                                                                        @foreach($input->corrects as $correct)
                                                                            <div class="editor-corrects-item">{{ $correct->value }} <button form="question-substitute-input-correct-{{ $input->id }}-{{ $correct->id }}" class="icon icon-times"></button></div>
                                                                            <form style="display: none" id="question-substitute-input-correct-{{ $input->id }}-{{ $correct->id }}" method="post" action="{{ route('sheets.questions.substitutes.inputs.corrects.destroy', [$sheet->id, $question->id, $substitute->id, $input->id, $correct->id]) }}">
                                                                                @csrf
                                                                                @method('DELETE')
                                                                            </form>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="article-button-container">
                    <a class="button button-primary button-inline article-button" href="">
                        Rozwiązuj online
                    </a>
                    <a class="button button-primary article-button" href="">
                        Przejdź do arkusza
                    </a>
                </div>
            </div>
        </article>
    </div>

    <div id="create-question-content" class="modal">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Dodaj zawartość pytania</h3>
                <button class="modal-close"></button>
            </div>
            <div class="modal-body">
                <form class="form" action="{{ route('sheets.questions.contents.store', [$sheet->id, $question->id]) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <label class="input-container">
                        Tekst
                        <textarea class="input-admin-text" name="description" id="" cols="30" rows="10"></textarea>
                    </label>
                    <label class="input-container input-container-upload">
                        Obrazek
                        <input class="input-upload input-image-preview" type="file" name="image" data-target="#question-create-image-preview">
                    </label>
                    <div class="article-image-container article-image-container-half is-hidden">
                        <img id="question-create-image-preview" class="article-image" src="" alt="Obrazek">
                    </div>
                    <div class="button-container">
                        <button class="button" type="submit">Dodaj</button>
                        <span class="button form-reset">Resetuj</span>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="edit-question" class="modal">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Edytuj zawartość pytania</h3>
                <button class="modal-close"></button>
            </div>
            <div class="modal-body">
                <form class="form" action="{{ route('sheets.questions.update', [$sheet->id, $question->id]) }}" method="post">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="sheet_id" value="{{ $sheet->id }}">
                    <label class="input-container">
                        Tytuł
                        <input class="input-admin-text" name="title" type="text" value="{{ $question->title }}">
                    </label>
                    <div class="button-container">
                        <button class="button" type="submit">Edytuj</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="create-question-tag" class="modal">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Dodaj kategorię</h3>
                <button class="modal-close"></button>
            </div>
            <div class="modal-body">
                <form class="form" action="{{ route('sheets.questions.tags.store', [$sheet->id, $question->id]) }}" method="post">
                    @csrf
                    <input type="hidden" name="question_id" value="{{ $question->id }}">
                    <label class="input-container">
                        Kategoria
                        <select class="input-admin-text" name="tag_id" id="">
                            @foreach($tags as $tag)
                                <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                            @endforeach
                        </select>
                    </label>
                    <div class="button-container">
                        <button class="button" type="submit">Dodaj</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection