@extends('layouts.admin', ['title' => 'Pytania'])

@section('content')
    <div class="button-container">
        <a class="button" href="{{ route('sheets.index') }}">Wróć</a>
    </div>
    <div class="button-container">
        <a class="button" href="{{ route('sheets.questions.create', $sheet->id) }}">Dodaj nowe pytanie</a>
    </div>
    <div class="form-container">
        <form class="form" action="{{ route('tags.store') }}" method="post">
            <label class="input-container">
                Matura
                <input name="name" class="input-admin-text" type="text" disabled value="{{ $sheet->release }}">
            </label>
            <label class="input-container">
                Przedmiot
                <input name="name" class="input-admin-text" type="text" disabled value="{{ $sheet->subject->name }}">
            </label>
            <label class="input-container">
                Poziom
                <input name="name" class="input-admin-text" type="text" disabled value="{{ $sheet->tier->name }}">
            </label>
        </form>
    </div>
    <table class="table">
        <tr class="table-head">
            <th>ID</th>
            <th>Przedmiot</th>
            <th>Arkusz</th>
            <th>Tytuł</th>
            <th>Aktywny</th>
            <th>Podzadań</th>
            <th>Punktów</th>
            <th>Actions</th>
        </tr>
        @foreach($questions as $question)
            <tr>
                <td>{{ $question->id }}</td>
                <td>{{ $sheet->subject->name }}</td>
                <td>{{ $sheet->release }} {{ $sheet->type->short_description }}</td>
                <td>{{ $question->title }}</td>
                <td>{{ $question->enabled ? 'Tak' : 'Nie' }}</td>
                <td>{{ $question->substitutes->count() }}</td>
                <td>{{ $question->points }}</td>
                <td>
                    <button class="mini-link mini-link-delete" type="submit" form="question-delete-{{ $question->id }}"></button>
                    <form id="question-delete-{{ $question->id }}" method="post" action="{{ route('sheets.questions.destroy', [$sheet->id, $question->id]) }}" style="display: none;">
                        @csrf
                        @method('DELETE')
                    </form>
                    <a class="mini-link mini-link-edit" href="{{ route('sheets.questions.edit', [$sheet->id, $question->id]) }}"></a>
                    <a class="mini-link mini-link-editor" href="{{ route('sheets.questions.editor.index', [$sheet->id, $question->id]) }}"></a>
                </td>
            </tr>
        @endforeach
    </table>
@endsection