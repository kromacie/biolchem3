@extends('layouts.admin', ['title' => 'Stwórz arkusz'])

@section('content')
    <div class="button-container">
        <a class="button" href="{{ route('sheets.index') }}">Wróć</a>
    </div>
    @if($errors->any())
        <div class="notifier">
            <div class="notifier-header">
                <h3 class="notifier-title">Powiadomienia</h3>
            </div>
            <div class="notifier-body">
                <span class="notifier-item">Wszystko jest w porządku</span>
                <span class="notifier-item">Wszystko jest w porządku</span>
                <span class="notifier-item">Wszystko jest w porządku</span>
            </div>
        </div>
    @endif
    <div class="form-container">
        <form class="form" action="{{ route('sheets.store') }}" method="post">
            @csrf
            <label class="input-container">
                Przedmiot
                <select class="input-admin-text" name="subject_id" id="">
                    @foreach($subjects as $subject)
                        <option value="{{ $subject->id }}">{{ $subject->name }}</option>
                    @endforeach
                </select>
            </label>
            <label class="input-container">
                Data
                <input name="release" class="input-admin-text" type="date">
            </label>
            <label class="input-container">
                Typ matury
                <select class="input-admin-text" name="type_id" id="">
                    @foreach($types as $type)
                        <option value="{{ $type->id }}">{{ $type->name }}</option>
                    @endforeach
                </select>
            </label>
            <label class="input-container">
                Wersja matury
                <select class="input-admin-text" name="version_id" id="">
                    @foreach($versions as $version)
                        <option value="{{ $version->id }}">{{ $version->description }}</option>
                    @endforeach
                </select>
            </label>
            <label class="input-container">
                Poziom matury
                <select class="input-admin-text" name="tier_id" id="">
                    @foreach($tiers as $tier)
                        <option value="{{ $tier->id }}">{{ $tier->name }}</option>
                    @endforeach
                </select>
            </label>
            <label class="input-container input-checkbox-container">
                Czy włączone?
                <input type="checkbox" name="enabled" class="input-checkbox">
                <span class="input-checkmark"></span>
            </label>
            <div class="button-container">
                <button class="button" type="submit">
                    Dodaj
                </button>
            </div>
        </form>
    </div>
@endsection