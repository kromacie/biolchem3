@extends('layouts.admin', ['title' => 'Edytuj arkusz'])

@section('content')
    <div class="button-container">
        <a class="button" href="{{ route('sheets.index') }}">Wróć</a>
    </div>
    @if($errors->any())
        <div class="notifier">
            <div class="notifier-header">
                <h3 class="notifier-title">Powiadomienia</h3>
            </div>
            <div class="notifier-body">
                <span class="notifier-item">Wszystko jest w porządku</span>
                <span class="notifier-item">Wszystko jest w porządku</span>
                <span class="notifier-item">Wszystko jest w porządku</span>
            </div>
        </div>
    @endif
    <div class="form-container">
        <form class="form" action="{{ route('sheets.update', $sheet->id) }}" method="post">
            @csrf
            @method('PUT')
            <label class="input-container">
                Przedmiot
                <select class="input-admin-text" name="subject_id" id="">
                    @foreach($subjects as $subject)
                        <option {{ $sheet->subject_id == $subject->id ? 'selected' : '' }} value="{{ $subject->id }}">{{ $subject->name }}</option>
                    @endforeach
                </select>
            </label>
            <label class="input-container">
                Data
                <input name="release" class="input-admin-text" type="date" value="{{ $sheet->isoRelease }}">
            </label>
            <label class="input-container">
                Typ matury
                <select class="input-admin-text" name="type_id" id="">
                    @foreach($types as $type)
                        <option value="{{ $type->id }}" {{ $sheet->type_id == $type->id ? 'selected' : '' }}>{{ $type->name }}</option>
                    @endforeach
                </select>
            </label>
            <label class="input-container">
                Wersja matury
                <select class="input-admin-text" name="version_id" id="">
                    @foreach($versions as $version)
                        <option value="{{ $version->id }}" {{ $sheet->version_id == $version->id ? 'selected' : '' }}>{{ $version->description }}</option>
                    @endforeach
                </select>
            </label>
            <label class="input-container">
                Poziom matury
                <select class="input-admin-text" name="tier_id" id="">
                    @foreach($tiers as $tier)
                        <option value="{{ $tier->id }}" {{ $sheet->tier_id == $tier->id ? 'selected' : '' }}>{{ $tier->name }}</option>
                    @endforeach
                </select>
            </label>
            <label class="input-container input-checkbox-container">
                Czy włączone?
                <input type="checkbox" name="enabled" class="input-checkbox" {{ $sheet->enabled ? 'checked' : '' }}>
                <span class="input-checkmark"></span>
            </label>
            <div class="button-container">
                <button class="button" type="submit">
                    Edytuj
                </button>
            </div>
        </form>
    </div>
@endsection