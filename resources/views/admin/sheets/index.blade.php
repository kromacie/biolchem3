@extends('layouts.admin', ['title' => 'Arkusze'])

@section('content')
    <div class="button-container">
        <a class="button" href="{{ route('sheets.create') }}">Dodaj nowy arkusz</a>
    </div>
    <table class="table">
        <tr class="table-head">
            <th>ID</th>
            <th>Przedmiot</th>
            <th>Data</th>
            <th>Czy włączone?</th>
            <th>Wersja</th>
            <th>Poziom</th>
            <th>Typ</th>
            <th>Zadań</th>
            <th>Actions</th>
        </tr>
        @foreach($sheets as $sheet)
            <tr>
                <td>{{ $sheet->id }}</td>
                <td>{{ $sheet->subject->name }}</td>
                <td>{{ $sheet->release }}</td>
                <td>{{ $sheet->enabled ? 'Tak' : 'Nie' }}</td>
                <td>{{ $sheet->version->description }}</td>
                <td>{{ $sheet->tier->name }}</td>
                <td>{{ $sheet->type->name }}</td>
                <td>{{ $sheet->questions->count() }}</td>
                <td>
                    <button class="mini-link mini-link-delete" type="submit" form="sheet-delete-{{ $sheet->id }}"></button>
                    <form id="sheet-delete-{{ $sheet->id }}" method="post" action="{{ route('sheets.destroy', $sheet->id) }}" style="display: none;">
                        @csrf
                        @method('DELETE')
                    </form>
                    <a class="mini-link mini-link-edit" href="{{ route('sheets.edit', $sheet->id) }}"></a>
                    <a class="mini-link mini-link-show" href="{{ route('sheets.questions.index', $sheet->id) }}"></a>
                </td>
            </tr>
        @endforeach
    </table>
@endsection