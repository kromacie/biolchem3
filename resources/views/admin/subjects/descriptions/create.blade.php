@extends('layouts.admin', ['title' => 'Stwórz opis przedmiotu'])

@section('content')
    <div class="button-container">
        <a class="button" href="{{ route('subjects.descriptions.index', $subject->id) }}">Wróć</a>
    </div>
    @if($errors->any())
        <div class="notifier">
            <div class="notifier-header">
                <h3 class="notifier-title">Powiadomienia</h3>
            </div>
            <div class="notifier-body">
                <span class="notifier-item">Wszystko jest w porządku</span>
                <span class="notifier-item">Wszystko jest w porządku</span>
                <span class="notifier-item">Wszystko jest w porządku</span>
            </div>
        </div>
    @endif
    <div class="form-container">
        <form class="form" action="{{ route('subjects.descriptions.store', $subject->id) }}" method="post">
            @csrf
            <input type="hidden" name="subject_id" value="{{ $subject->id }}">
            <label class="input-container">
                Zawartość
                <textarea class="input-admin-text" name="content"></textarea>
            </label>
            <div class="button-container">
                <button class="button" type="submit">
                    Dodaj
                </button>
            </div>
        </form>
    </div>
@endsection