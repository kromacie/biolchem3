@extends('layouts.admin', ['title' => 'Opis przedmiotu'])

@section('content')
    <div class="button-container">
        <a class="button" href="{{ route('subjects.index') }}">Wróć</a>
    </div>
    @empty($description)
    <div class="button-container">
        <a class="button" href="{{ route('subjects.descriptions.create', $subject->id) }}">Dodaj</a>
    </div>
    @endempty
    @if($errors->any())
        <div class="notifier">
            <div class="notifier-header">
                <h3 class="notifier-title">Powiadomienia</h3>
            </div>
            <div class="notifier-body">
                @foreach($errors->all() as $error)
                    <span class="notifier-item">{{ $error }}</span>
                @endforeach
            </div>
        </div>
    @endif
    @isset($description)
    <div class="form-container">
        <form class="form" action="{{ route('subjects.descriptions.update', [$subject->id, $description->id]) }}" method="post">
            @csrf
            @method('PUT')
            <input type="hidden" name="subject_id" value="{{ $subject->id }}">
            <label class="input-container">
                Zawartość
                <textarea class="input-admin-text" name="content">{{ $description->content }}</textarea>
            </label>
            <div class="button-container">
                <button class="button" type="submit">
                    Edytuj
                </button>
            </div>
        </form>
    </div>
    @endisset
@endsection