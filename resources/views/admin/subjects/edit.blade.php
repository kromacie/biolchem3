@extends('layouts.admin', ['title' => 'Edytuj przedmiot'])

@section('content')
    <div class="button-container">
        <a class="button" href="{{ route('subjects.index') }}">Wróć</a>
    </div>
    @if($errors->any())
        <div class="notifier">
            <div class="notifier-header">
                <h3 class="notifier-title">Powiadomienia</h3>
            </div>
            <div class="notifier-body">
                <span class="notifier-item">Wszystko jest w porządku</span>
                <span class="notifier-item">Wszystko jest w porządku</span>
                <span class="notifier-item">Wszystko jest w porządku</span>
            </div>
        </div>
    @endif
    <div class="form-container">
        <form class="form" action="{{ route('subjects.update', $subject->id) }}" method="post">
            @csrf
            @method('PUT')
            <label class="input-container">
                Nazwa
                <input class="input-admin-text" type="text" name="name" value="{{ $subject->name }}">
            </label>
            <label class="input-container">
                Ikona
                <input class="input-admin-text" name="icon" type="text" value="{{ $subject->icon }}">
            </label>
            <label class="input-container">
                Wygląd
                <input class="input-admin-text" name="theme" type="text" value="{{ $subject->theme }}">
            </label>
            <label class="input-container input-checkbox-container">
                Czy włączone?
                <input class="input-checkbox" name="enabled" type="checkbox" {{ $subject->enabled ? 'checked' : '' }}>
                <span class="input-checkmark"></span>
            </label>
            <div class="button-container">
                <button class="button" type="submit">
                    Edytuj
                </button>
            </div>
        </form>
    </div>
@endsection