@extends('layouts.admin', ['title' => 'Przedmioty'])

@section('content')
    <div class="button-container">
        <a class="button" href="{{ route('subjects.create') }}">Dodaj nowy przedmiot</a>
    </div>
    <table class="table">
        <tr class="table-head">
            <th>ID</th>
            <th>Nazwa</th>
            <th>Ikona</th>
            <th>Wygląd</th>
            <th>Czy włączone?</th>
            <th>Actions</th>
        </tr>
        @foreach($subjects as $subject)
            <tr>
                <td>{{ $subject->id }}</td>
                <td>{{ $subject->name }}</td>
                <td>{{ $subject->icon }}</td>
                <td>{{ $subject->theme }}</td>
                <td>{{ $subject->enabled ? 'Tak' : 'Nie' }}</td>
                <td>
                    <button class="mini-link mini-link-delete" type="submit" form="subject-delete-{{ $subject->id }}"></button>
                    <form id="subject-delete-{{ $subject->id }}" method="post" action="{{ route('subjects.destroy', $subject->id) }}" style="display: none;">
                        @csrf
                        @method('DELETE')
                    </form>
                    <a class="mini-link mini-link-edit" href="{{ route('subjects.edit', $subject->id) }}"></a>
                    <a class="mini-link mini-link-sheet" href="{{ route('subjects.descriptions.index', $subject->id) }}"></a>
                </td>
            </tr>
        @endforeach
    </table>
@endsection