@extends('layouts.admin', ['title' => 'Edytuj kategorię'])

@section('content')
    <div class="button-container">
        <a class="button" href="{{ route('tags.index') }}">Wróć</a>
    </div>
    @if($errors->any())
        <div class="notifier">
            <div class="notifier-header">
                <h3 class="notifier-title">Powiadomienia</h3>
            </div>
            <div class="notifier-body">
                @foreach($errors->all() as $error)
                    <span class="notifier-item">{{ $error }}</span>
                @endforeach
            </div>
        </div>
    @endif
    <div class="form-container">
        <form class="form" action="{{ route('tags.update', $tag->id) }}" method="post">
            @csrf
            @method('PUT')
            <label class="input-container">
                Nazwa
                <input name="name" class="input-admin-text" type="text" value="{{ $tag->name }}">
            </label>
            <label class="input-container">
                Przedmiot
                <select class="input-admin-text ajax-loader" name="subject_id" id="subject-select" data-method="get" data-url="{{ route('tags.index', ['without_tags' => 1]) }}">
                    @foreach($subjects as $subject)
                        <option value="{{ $subject->id }}" {{ $tag->subject_id == $subject->id ? 'selected=selected' : '' }}>{{ $subject->name }}</option>
                    @endforeach
                </select>
            </label>
            <label class="input-container">
                Podkategoria
                <select class="input-admin-text" name="tag_id" id="tags">
                    <option value="" selected>--</option>
                </select>
            </label>
            <div class="button-container">
                <button class="button" type="submit">
                    Edytuj
                </button>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script>
        let selected = "{{ $tag->tag_id }}";
        $(function () {
            ajaxLoaderDone(function(response){
                let tags = $("#tags");
                    tags.find(`[data-loaded="true"]`).remove();

                $.each(response, function (index, value) {
                    let option = $('<option/>');
                    option.attr({value: value.id, "data-loaded": "true"}).text(value.name);
                    if(value.id == selected) {
                        option.attr("selected", "selected");
                    }
                    tags.append(option);
                });
            }, 'subject-select');

            $(".ajax-loader").trigger('change');
        });
    </script>
@endsection