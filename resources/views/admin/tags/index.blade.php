@extends('layouts.admin', ['title' => 'Kategorie'])

@section('content')
<div class="button-container">
    <a class="button" href="{{ route('tags.create') }}">Dodaj nową kategorię</a>
</div>
<form class="form">
    <label class="input-container">
        Kategoria
        <select class="input-admin-text" name="tag_id">
            <option value="">--</option>
            @foreach($categories as $tag)
                <option {{ $tag_id == $tag->id ? 'selected' : '' }} value="{{ $tag->id }}">{{ $tag->name }}</option>
            @endforeach
        </select>
    </label>
    <label class="input-container">
        Przedmiot
        <select class="input-admin-text" name="subject_id">
            <option value="">--</option>
            @foreach($subjects as $subject)
                <option {{ $subject_id == $subject->id ? 'selected' : '' }} value="{{ $subject->id }}">{{ $subject->name }}</option>
            @endforeach
        </select>
    </label>
    <div class="button-container">
        <button class="button" type="submit">
            Szukaj
        </button>
    </div>
</form>
<table class="table">
    <tr class="table-head">
        <th>ID</th>
        <th>Nazwa</th>
        <th>Przedmiot</th>
        <th>Podkategoria</th>
        <th>Actions</th>
    </tr>
    @foreach($tags as $tag)
    <tr>
        <td>{{ $tag->id }}</td>
        <td>{{ $tag->name }}</td>
        <td>{{ $tag->subject->name }}</td>
        <td>{{ $tag->tag_id ? $tag->tag->name : '--' }}</td>
        <td>
            <button class="mini-link mini-link-delete" type="submit" form="tag-delete-{{ $tag->id }}"></button>
            <form id="tag-delete-{{ $tag->id }}" method="post" action="{{ route('tags.destroy', $tag->id) }}" style="display: none;">
                @csrf
                @method('DELETE')
            </form>
            <a class="mini-link mini-link-edit" href="{{ route('tags.edit', $tag->id) }}"></a>
        </td>
    </tr>
    @endforeach
</table>
@endsection