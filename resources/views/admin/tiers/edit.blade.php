@extends('layouts.admin', ['title' => 'Edytuj poziom'])

@section('content')
    <div class="button-container">
        <a class="button" href="{{ route('tiers.index') }}">Wróć</a>
    </div>
    @if($errors->any())
        <div class="notifier">
            <div class="notifier-header">
                <h3 class="notifier-title">Powiadomienia</h3>
            </div>
            <div class="notifier-body">
                @foreach($errors->all() as $error)
                    <span class="notifier-item">{{ $error }}</span>
                @endforeach
            </div>
        </div>
    @endif
    <div class="form-container">
        <form class="form" action="{{ route('tiers.update', $tier->id) }}" method="post">
            @csrf
            @method('PUT')
            <label class="input-container">
                Nazwa
                <input name="name" class="input-admin-text" type="text" value="{{ $tier->name }}">
            </label>
            <div class="button-container">
                <button class="button" type="submit">
                    Edytuj
                </button>
            </div>
        </form>
    </div>
@endsection