@extends('layouts.admin', ['title' => 'Poziomy arkuszy'])

@section('content')
    <div class="button-container">
        <a class="button" href="{{ route('tiers.create') }}">Dodaj poziom</a>
    </div>
    <table class="table">
        <tr class="table-head">
            <th>ID</th>
            <th>Nazwa</th>
            <th>Actions</th>
        </tr>
        @foreach($tiers as $tier)
            <tr>
                <td>{{ $tier->id }}</td>
                <td>{{ $tier->name }}</td>
                <td>
                    <button class="mini-link mini-link-delete" type="submit" form="tier-delete-{{ $tier->id }}"></button>
                    <form id="tier-delete-{{ $tier->id }}" method="post" action="{{ route('tiers.destroy', $tier->id) }}" style="display: none;">
                        @csrf
                        @method('DELETE')
                    </form>
                    <a class="mini-link mini-link-edit" href="{{ route('tiers.edit', $tier->id) }}"></a>
                </td>
            </tr>
        @endforeach
    </table>
@endsection