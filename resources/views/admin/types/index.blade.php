@extends('layouts.admin', ['title' => 'Typy arkuszy'])

@section('content')
    <div class="button-container">
        <a class="button" href="{{ route('types.create') }}">Dodaj typ</a>
    </div>
    <table class="table">
        <tr class="table-head">
            <th>ID</th>
            <th>Nazwa</th>
            <th>Actions</th>
        </tr>
        @foreach($types as $type)
            <tr>
                <td>{{ $type->id }}</td>
                <td>{{ $type->name }}</td>
                <td>
                    <button class="mini-link mini-link-delete" type="submit" form="tier-delete-{{ $type->id }}"></button>
                    <form id="tier-delete-{{ $type->id }}" method="post" action="{{ route('types.destroy', $type->id) }}" style="display: none;">
                        @csrf
                        @method('DELETE')
                    </form>
                    <a class="mini-link mini-link-edit" href="{{ route('types.edit', $type->id) }}"></a>
                </td>
            </tr>
        @endforeach
    </table>
@endsection