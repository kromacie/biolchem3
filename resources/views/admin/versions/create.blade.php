@extends('layouts.admin', ['title' => 'Dodaj nowy typ matur'])

@section('content')
    <div class="button-container">
        <a class="button" href="{{ route('versions.index') }}">Wróć</a>
    </div>
    @if($errors->any())
        <div class="notifier">
            <div class="notifier-header">
                <h3 class="notifier-title">Powiadomienia</h3>
            </div>
            <div class="notifier-body">
                @foreach($errors->all() as $error)
                    <span class="notifier-item">{{ $error }}</span>
                @endforeach
            </div>
        </div>
    @endif
    <div class="form-container">
        <form class="form" action="{{ route('versions.store') }}" method="post">
            @csrf
            <label class="input-container">
                Opis
                <input name="description" class="input-admin-text" type="text">
            </label>
            <label class="input-container">
                Krótki opis
                <input name="short_description" class="input-admin-text" type="text">
            </label>
            <label class="input-container input-checkbox-container">
                Czy wyświetlać opis?
                <input name="display_stat" class="input-checkbox" type="checkbox">
                <span class="input-checkmark"></span>
            </label>
            <label class="input-container input-checkbox-container">
                Czy wyświetlać krótki opis?
                <input name="display_name" class="input-checkbox" type="checkbox">
                <span class="input-checkmark"></span>
            </label>
            <div class="button-container">
                <button class="button" type="submit">
                    Dodaj
                </button>
            </div>
        </form>
    </div>
@endsection