@extends('layouts.admin', ['title' => 'Edytuj typ matur'])

@section('content')
    <div class="button-container">
        <a class="button" href="{{ route('versions.index') }}">Wróć</a>
    </div>
    @if($errors->any())
        <div class="notifier">
            <div class="notifier-header">
                <h3 class="notifier-title">Powiadomienia</h3>
            </div>
            <div class="notifier-body">
                @foreach($errors->all() as $error)
                    <span class="notifier-item">{{ $error }}</span>
                @endforeach
            </div>
        </div>
    @endif
    <div class="form-container">
        <form class="form" action="{{ route('versions.update', $version->id) }}" method="post">
            @csrf
            @method('PUT')
            <label class="input-container">
                Opis
                <input name="description" class="input-admin-text" type="text" value="{{ $version->description }}">
            </label>
            <label class="input-container">
                Krótki opis
                <input name="short_description" class="input-admin-text" type="text" value="{{ $version->short_description }}">
            </label>
            <label class="input-container input-checkbox-container">
                Czy wyświetlać opis?
                <input name="display_stat" class="input-checkbox" type="checkbox" {{ $version->display_stat ? 'checked' : '' }}>
                <span class="input-checkmark"></span>
            </label>
            <label class="input-container input-checkbox-container">
                Czy wyświetlać krótki opis?
                <input name="display_name" class="input-checkbox" type="checkbox" {{ $version->display_name ? 'checked' : '' }}>
                <span class="input-checkmark"></span>
            </label>
            <div class="button-container">
                <button class="button" type="submit">
                    Edytuj
                </button>
            </div>
        </form>
    </div>
@endsection