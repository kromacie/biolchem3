@extends('layouts.admin', ['title' => 'Wersje matur'])

@section('content')
    <div class="button-container">
        <a class="button" href="{{ route('versions.create') }}">Dodaj nową wersję</a>
    </div>
    <table class="table">
        <tr class="table-head">
            <th>ID</th>
            <th>Opis</th>
            <th>Krótki opis</th>
            <th>Wyświetlać opis?</th>
            <th>Wyświetlać krótki opis?</th>
            <th>Akcje</th>
        </tr>
        @foreach($versions as $version)
            <tr>
                <td>{{ $version->id }}</td>
                <td>{{ $version->description }}</td>
                <td>{{ $version->short_description }}</td>
                <td>{{ $version->display_stat ? 'Tak' : 'Nie' }}</td>
                <td>{{ $version->display_name ? 'Tak' : 'Nie' }}</td>
                <td>
                    <button class="mini-link mini-link-delete" type="submit" form="version-delete-{{ $version->id }}"></button>
                    <form id="version-delete-{{ $version->id }}" method="post" action="{{ route('versions.destroy', $version->id) }}" style="display: none;">
                        @csrf
                        @method('DELETE')
                    </form>
                    <a class="mini-link mini-link-edit" href="{{ route('versions.edit', $version->id) }}"></a>
                </td>
            </tr>
        @endforeach
    </table>
@endsection