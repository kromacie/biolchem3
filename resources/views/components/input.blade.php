@if($group->type->name == "SINGULAR_TEXT")
    <label class="{{ $group->class }}">
        @markdown($input->description)
        @if(!$input->is_description)
            <input id="answer_{{ $question->id }}_{{ $substitute->id }}_{{ $group->id }}_{{ $input->id }}" class="{{ $input->classes }}" type="text" name="answer[{{ $question->id }}][{{ $substitute->id }}][{{ $group->id }}][{{ $input->id }}]" placeholder="{{ $input->placeholder }}" {{ $input->disabled ? 'disabled' : '' }}>
        @endif
    </label>
@endif
@if($group->type->name == "MULTIPLE_RADIO")
    <label class="{{ $group->class }}">
        <input id="answer_{{ $question->id }}_{{ $substitute->id }}_{{ $group->id }}_{{ $input->id }}" class="{{ $input->classes }}" type="radio" name="answer[{{ $question->id }}][{{ $substitute->id }}][{{ $group->id }}]" data-input="{{ $input->id }}" {{ $input->checked ? 'checked' : '' }} {{ $input->disabled ? 'disabled' : '' }}>
        <span class="input-radio"></span>
        <span class="input-radio-text">
        @markdown($input->description)
    </span>
    </label>
@endif
@if($group->type->name == "MULTIPLE_TEXT")
    <label class="{{ $group->class }}">
        @markdown($input->description)
        @if(!$input->is_description)
            <input id="answer_{{ $question->id }}_{{ $substitute->id }}_{{ $group->id }}_{{ $input->id }}" class="{{ $input->classes }}" type="text" name="answer[{{ $question->id }}][{{ $substitute->id }}][{{ $group->id }}][{{ $input->id }}]" placeholder="{{ $input->placeholder }}" {{ $input->disabled ? 'disabled' : '' }}>
        @endif
    </label>
@endif
@if($group->type->name == "SINGULAR_LETTER")
    <label class="{{ $group->class }}">
        @markdown($input->description)
        @if(!$input->is_description)
            <input id="answer_{{ $question->id }}_{{ $substitute->id }}_{{ $group->id }}_{{ $input->id }}" class="{{ $input->classes }}" type="text" name="answer[{{ $question->id }}][{{ $substitute->id }}][{{ $group->id }}][{{ $input->id }}]" placeholder="{{ $input->placeholder }}" {{ $input->disabled ? 'disabled' : '' }}>
        @endif
    </label>
@endif

@if($group->type->name == "MULTIPLE_LETTER")
    <label class="{{ $group->class }}">
        @markdown($input->description)
        @if(!$input->is_description)
            <input id="answer_{{ $question->id }}_{{ $substitute->id }}_{{ $group->id }}_{{ $input->id }}" class="{{ $input->classes }}" type="text" name="answer[{{ $question->id }}][{{ $substitute->id }}][{{ $group->id }}][{{ $input->id }}]" placeholder="{{ $input->placeholder }}" {{ $input->disabled ? 'disabled' : '' }}>
        @endif
    </label>
@endif

@if($group->type->name == "SINGULAR_CHECKBOX")
    <label class="{{ $group->class }}">
        @markdown($input->description)
        @if(!$input->is_description)
            <input id="answer_{{ $question->id }}_{{ $substitute->id }}_{{ $group->id }}_{{ $input->id }}" class="{{ $input->classes }}" type="checkbox" name="answer[{{ $question->id }}][{{ $substitute->id }}][{{ $group->id }}][{{ $input->id }}]" data-group="{{ $group->id }}" placeholder="{{ $input->placeholder }}" {{ $input->checked ? 'checked' : '' }} {{ $input->disabled ? 'disabled' : '' }}>
            <span class="input-checkmark"></span>
        @endif
    </label>
@endif