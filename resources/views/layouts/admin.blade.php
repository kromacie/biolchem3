<html lang="pl">
<head>
    <title>Biolchem</title>
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
<div class="flex flex-row">
    <div class="nav col-2">
        <div class="nav-header">
            <h1 class="nav-title">Panel</h1>
        </div>
        <div class="nav-body">
            <ul class="nav-list">
                <li class="nav-item expand-toggler" data-target="#subjects-nav">Przedmioty</li>
                <li id="subjects-nav" class="nav-expand-target expand-target">
                    <ul class="nav-list">
                        <li class="nav-item"><a class="nav-link" href="{{ route('subjects.index') }}">Przeglądaj</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('subjects.create') }}">Dodaj</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav-list">
                <li class="nav-item expand-toggler" data-target="#sheets-nav">Arkusze</li>
                <li id="sheets-nav" class="nav-expand-target expand-target">
                    <ul class="nav-list">
                        <li class="nav-item"><a class="nav-link" href="{{ route('sheets.index') }}">Przeglądaj</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('sheets.create') }}">Dodaj</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav-list">
                <li class="nav-item expand-toggler" data-target="#versions-nav">Wersje arkuszy</li>
                <li id="versions-nav" class="nav-expand-target expand-target">
                    <ul class="nav-list">
                        <li class="nav-item"><a class="nav-link" href="{{ route('versions.index') }}">Przeglądaj</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('versions.create') }}">Dodaj</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav-list">
                <li class="nav-item expand-toggler" data-target="#tiers-nav">Poziomy arkuszy</li>
                <li id="tiers-nav" class="nav-expand-target expand-target">
                    <ul class="nav-list">
                        <li class="nav-item"><a class="nav-link" href="{{ route('tiers.index') }}">Przeglądaj</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('tiers.create') }}">Dodaj</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav-list">
                <li class="nav-item expand-toggler" data-target="#types-nav">Typy arkuszy</li>
                <li id="types-nav" class="nav-expand-target expand-target">
                    <ul class="nav-list">
                        <li class="nav-item"><a class="nav-link" href="{{ route('types.index') }}">Przeglądaj</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('types.create') }}">Dodaj</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav-list">
                <li class="nav-item expand-toggler" data-target="#tags-nav">Tagi</li>
                <li id="tags-nav" class="nav-expand-target expand-target">
                    <ul class="nav-list">
                        <li class="nav-item"><a class="nav-link" href="{{ route('tags.index') }}">Przeglądaj</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('tags.create') }}">Dodaj</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav-list">
                <li class="nav-item expand-toggler" data-target="#inputs-groups-types-nav">Typy grup inputów</li>
                <li id="inputs-groups-types-nav" class="nav-expand-target expand-target">
                    <ul class="nav-list">
                        <li class="nav-item"><a class="nav-link" href="{{ route('inputs.groups.types.index') }}">Przeglądaj</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('inputs.groups.types.create') }}">Dodaj</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav-list">
                <li class="nav-item expand-toggler" data-target="#inputs-types-nav">Typy inputów</li>
                <li id="inputs-types-nav" class="nav-expand-target expand-target">
                    <ul class="nav-list">
                        <li class="nav-item"><a class="nav-link" href="{{ route('inputs.types.index') }}">Przeglądaj</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('inputs.types.create') }}">Dodaj</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav-list">
                <li class="nav-item expand-toggler" data-target="#stats-nav">Statystyki</li>
                <li id="stats-nav" class="nav-expand-target expand-target">
                    <ul class="nav-list">
                        <li class="nav-item">Przeglądaj</li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-10">
        <div class="header">
            <h2 class="header-title">{{ $title }}</h2>
        </div>
        <section class="section">
            <div class="section-header">
                <h2 class="section-title">Zawartość</h2>
            </div>
            <div class="section-body">
                @section('content')
                    <span>Wybierz opcję z listy</span>
                @show
            </div>
        </section>
    </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>
@section('scripts')

@show
</body>
</html>