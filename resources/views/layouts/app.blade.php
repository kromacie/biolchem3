<html lang="pl">
<head>
    <title>Biolchem</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>

<div class="header-main">
    <div class="header-main-image"></div>
    <h1 class="header-main-logo">
        <i class="icon icon-seedling"></i> Biolchem
    </h1>
    <p class="header-main-description">Because we love science!</p>
</div>
<div class="nav-main">
    <ul class="nav-main-navbar">
        <li class="nav-main-item">
            <a href="{{ route('index') }}">Strona główna</a>
        </li>
        @foreach($subjects as $subject)
            <li class="nav-main-item">
                <a href="{{ route('users.subjects.show', $subject->slug) }}">
                    {{ $subject->name }}
                    @if($subject->icon)
                        <i class="nav-main-icon icon {{ $subject->icon }}"></i>
                    @endif
                </a>
            </li>
        @endforeach
        <li class="nav-main-item nav-main-item-disabled">
            <a href="#">Kontakt</a>
        </li>
        <li class="nav-main-item">
            <a href="{{ route('about_us') }}">O nas</a>
        </li>
    </ul>
</div>
<br />
<div class="container">
    @yield('content')
</div>

@yield('footer')

<script src="{{ asset('js/app.js') }}"></script>
@section('scripts')

@show
</body>
</html>