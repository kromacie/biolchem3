<html lang="pl">
<head>
    <title>Biolchem</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
<div class="header-main">
    <div class="header-main-image"></div>
    <h1 class="header-main-logo">
        <i class="icon icon-seedling"></i> Biolchem
    </h1>
    <p class="header-main-description">Because we love science!</p>
</div>
<div class="nav-main">
    <ul class="nav-main-navbar">
        <li class="nav-main-item">
            <a href="#">Strona główna</a>
        </li>
        <li class="nav-main-item">
            <a href="#">Biologia <i class="nav-main-icon icon icon-dna"></i></a>
        </li>
        <li class="nav-main-item">
            <a href="#">Chemia <i class="nav-main-icon icon icon-flask"></i></a>
        </li>
        <li class="nav-main-item">
            <a href="#">Arkusze</a>
        </li>
        <li class="nav-main-item">
            <a href="#">Kontakt</a>
        </li>
        <li class="nav-main-item">
            <a href="#">O nas</a>
        </li>
    </ul>
</div>
<br />
<div class="container">
    <ul class="breadcrumb">
        <li class="breadcrumb-item selected">
            <i class="icon icon-home"></i>
        </li>
        <li class="breadcrumb-item"><i class="breadcrumb-icon icon icon-sheet"></i>Arkusze</li>
        <li class="breadcrumb-item"><i class="breadcrumb-icon icon icon-dna"></i>Biologia</li>
    </ul>
    <div class="section">
        <div class="section-header">
            <h1 class="title title-primary">Zadanie</h1>
        </div>
        <div class="section-body">
            <article class="article article-no-gutters-top">
                <div class="mini-statistic-container">
                    <div class="mini-statistic">
                                <span class="mini-statistic-icon icon icon-user">
                                    <span class="mini-statistic-icon-description">
                                        Rozwiązanych arkuszy
                                    </span>
                                </span>
                        <span class="mini-statistic-value">12</span>
                    </div>
                    <div class="mini-statistic">
                                <span class="mini-statistic-icon icon icon-calendar-check">
                                    <span class="mini-statistic-icon-description">
                                        Średni wynik
                                    </span>
                                </span>
                        <span class="mini-statistic-value">76%</span>
                    </div>
                    @if($question->type == 'extended')
                        <div class="mini-statistic mini-statistic-right">
                                    <span class="mini-statistic-icon icon icon-info-circle">
                                    </span>
                            <span class="mini-statistic-value">Nowa matura</span>
                        </div>
                    @endif
                </div>
                <div class="article-header article-header-has-number">
                    <span class="article-number article-number-primary">1</span>
                    <h2 class="article-title title">
                        Matura Czerwiec 2018, Poziom rozszerzony (nowy)<br />
                        <a class="primary" href="">{{ $question->title }} (2 pkt)</a>
                    </h2>
                </div>
                <div class="article-header">
                    <div class="tags">
                        <a class="tag tag-primary" href="">Metablizm</a>
                        <a class="tag tag-primary" href="">Biologia</a>
                        <a class="tag tag-primary" href="">Zwierzęta</a>
                    </div>
                </div>
                <div class="article-body">
                    @foreach($question->contents as $content)
                        @if($content->description)
                            <p class="article-text-intended">
                                {{ $content->description }}
                            </p>
                        @endif
                        @if($content->image_id)
                            <div class="article-image-container article-image-container-half">
                                <img class="article-image" src="{{ asset('storage/' .$content->image->path) }}" alt="">
                            </div>
                        @endif
                    @endforeach
                    @foreach($question->substitutes as $index => $substitute)
                        <div class="question">
                            <div class="question-header">
                                <h2 class="question-title">{{ $question->title . ($index + 1) }}</h2>
                                <span class="question-points">
                                        {{ $substitute->points }} punkt
                                    </span>
                            </div>
                            <div class="question-body">
                                @foreach($substitute->contents as $content)
                                    @if($content->description)
                                        <p class="question-text">
                                            {{ $content->description }}
                                        </p>
                                    @endif
                                    @if($content->image_id)
                                        <div class="article-image-container article-image-container-half">
                                            <img class="article-image" src="{{ asset('storage/' .$content->image->path) }}" alt="">
                                        </div>
                                    @endif
                                @endforeach
                                <div class="answer-container">
                                    <h3 class="answer-title">
                                        Odpowiedzi
                                    </h3>
                                    @foreach($substitute->answers as $answer)
                                        <div class="answer">
                                            @foreach($answer->inputGroups as $group)

                                                @if($group->description)
                                                    <p class="answer-text">{{ $group->description }}</p>
                                                @endif

                                                @foreach($group->inputs as $input)
                                                    @if($group->type == "SINGULAR_TEXT")
                                                        <label class="input-text-container">
                                                            {{ $input->description }} <input class="input-text" type="text">.
                                                        </label>
                                                    @endif
                                                    @if($group->type == "MULTI_RADIO")
                                                        <label class="input-radio-container">
                                                            <input class="input-radio-checkmark" type="radio" name="qw">
                                                            <span class="input-radio"></span>
                                                            <span class="input-radio-text">
                                                                {{ $input->description }}
                                                            </span>
                                                        </label>
                                                    @endif
                                                @endforeach

                                            @endforeach
                                            <div class="checkmark-container">
                                                <button class="checkmark checkmark-check">
                                                    <i class="checkmark-icon icon icon-question"></i> Sprawdź
                                                </button>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="article-button-container">
                        <a class="button button-primary button-inline article-button" href="">
                            Rozwiązuj online
                        </a>
                        <a class="button button-primary article-button" href="">
                            Przejdź do arkusza
                        </a>
                    </div>
                </div>
            </article>
        </div>
    </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>