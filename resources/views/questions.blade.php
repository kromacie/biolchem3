<html lang="pl">
<head>
    <title>Biolchem</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
<div class="header-main">
    <div class="header-main-image"></div>
    <h1 class="header-main-logo">
        <i class="icon icon-seedling"></i> Biolchem
    </h1>
    <p class="header-main-description">Because we love science!</p>
</div>
<div class="nav-main">
    <ul class="nav-main-navbar">
        <li class="nav-main-item">
            <a href="#">Strona główna</a>
        </li>
        <li class="nav-main-item">
            <a href="#">Biologia <i class="nav-main-icon icon icon-dna"></i></a>
        </li>
        <li class="nav-main-item">
            <a href="#">Chemia <i class="nav-main-icon icon icon-flask"></i></a>
        </li>
        <li class="nav-main-item">
            <a href="#">Arkusze</a>
        </li>
        <li class="nav-main-item">
            <a href="#">Kontakt</a>
        </li>
        <li class="nav-main-item">
            <a href="#">O nas</a>
        </li>
    </ul>
</div>
<br />
<div class="container">
    <ul class="breadcrumb">
        <li class="breadcrumb-item selected">
            <i class="icon icon-home"></i>
        </li>
        <li class="breadcrumb-item"><i class="breadcrumb-icon icon icon-sheet"></i>Arkusze</li>
        <li class="breadcrumb-item"><i class="breadcrumb-icon icon icon-dna"></i>Biologia</li>
    </ul>
    <div class="section col col-9-xl">
        <div class="section-header">
            <h1 class="title title-primary">
                <i class="title-icon icon icon-sheet"></i> Zadania
            </h1>
        </div>
        <div class="section-body article-list">
            <div class="article">
                <div class="article-header">
                    <h4 class="tag-filters-title">
                        <i class="title-icon icon icon-sliders tag-filters-icon"></i>Filtry
                        <i class="tag-filters-toggler expand-toggler" data-target="#tag-filters"></i>
                    </h4>
                </div>
                <div class="article-body">
                    <div class="tag-filters expand-target" id="tag-filters">
                                <span class="tag-filters-item">
                                    <span class="tag-filters-group">Ekologia i fizjologia roślin</span> - Układ oddechowy <i class="tag-filters-close-toggle"></i>
                                </span>
                        <span class="tag-filters-item">
                                    <span class="tag-filters-group">Roślony</span> - nasienne <i class="tag-filters-close-toggle"></i>
                                </span>
                        <span class="tag-filters-item">
                                    <span class="tag-filters-group">Ekologia i fizjologia roślin</span> - Układ oddechowy <i class="tag-filters-close-toggle"></i>
                                </span>
                        <span class="tag-filters-item">
                                    <span class="tag-filters-group">Ekologia i fizjologia roślin</span> - Układ krwionośny <i class="tag-filters-close-toggle"></i>
                                </span>
                        <span class="tag-filters-item">
                                    Wirusy, wriony, wroidy <i class="tag-filters-close-toggle"></i>
                                </span>
                    </div>
                </div>
                <div class="article-body">
                    <div class="pagination">
                        <span class="pagination-result">Ilość wyników: <strong>123</strong></span>
                        <div class="pagination-item">
                            <a class="pagination-link pagination-link-first" href=""></a>
                        </div>
                        <span class="pagination-item">
                                    <a class="pagination-link pagination-link-previous" href=""></a>
                                </span>
                        <span class="pagination-item">3</span>
                        <span class="pagination-item">
                                    <a class="pagination-link pagination-link-next" href=""></a>
                                </span>
                        <div class="pagination-item">
                            <a class="pagination-link pagination-link-last" href=""></a>
                        </div>
                    </div>
                </div>
            </div>
            @foreach($questions as $index => $question)
                <article class="article article-no-gutters-top">
                    <div class="mini-statistic-container">
                        <div class="mini-statistic">
                                <span class="mini-statistic-icon icon icon-user">
                                    <span class="mini-statistic-icon-description">
                                        Rozwiązanych arkuszy
                                    </span>
                                </span>
                            <span class="mini-statistic-value">12</span>
                        </div>
                        <div class="mini-statistic">
                                <span class="mini-statistic-icon icon icon-calendar-check">
                                    <span class="mini-statistic-icon-description">
                                        Średni wynik
                                    </span>
                                </span>
                            <span class="mini-statistic-value">76%</span>
                        </div>
                        <div class="mini-statistic mini-statistic-right">
                                <span class="mini-statistic-icon icon icon-info-circle">
                                </span>
                            <span class="mini-statistic-value">Nowa matura</span>
                        </div>
                    </div>
                    <div class="article-header article-header-has-number">
                        <span class="article-number article-number-primary">{{ $index + 1 }}</span>
                        <h2 class="article-title title">
                            Matura Czerwiec 2018 Poziom rozszerzony ,(nowy)<br />
                            <a class="primary" href="">{{ $question->title }} (2 pkt)</a>
                        </h2>
                    </div>
                    <div class="article-header">
                        <div class="tags">
                            <a class="tag tag-primary" href="">Metablizm</a>
                            <a class="tag tag-primary" href="">Biologia</a>
                            <a class="tag tag-primary" href="">Zwierzęta</a>
                        </div>
                    </div>
                    <div class="article-body">
                        @foreach($question->contents as $content)
                            @if($content->description)
                                <p class="article-text article-text-intended">
                                    {{ $content->description }}
                                </p>
                            @endif
                            @if($content->image_id)
                                <div class="article-image-container article-image-container-half">
                                    <img class="article-image" src="{{ asset('storage/' .$content->image->path) }}" alt="">
                                </div>
                            @endif
                        @endforeach
                        <div class="article-button-container">
                            <a class="button button-primary button-inline article-button" href="">
                                Rozwiązuj online
                            </a>
                            <a class="button button-primary article-button" href="">
                                Przejdź do arkusza
                            </a>
                        </div>
                    </div>
                </article>
            @endforeach
        </div>
    </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>