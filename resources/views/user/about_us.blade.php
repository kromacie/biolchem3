@extends('layouts.app')

@section('content')
    <div style="background: white; padding: 2em;text-align: justify;">
        <h1>
            O mnie
        </h1>
        <p>
            <img style="margin-left: 1rem;" class="image image-profile" src="{{ asset('images/my_img.jpg') }}" alt="">
            Cześć! Jestem <b>Maciej</b>, mam 21 lat a ta strona to obecnie moje największe dzieło. Jedyne które właściwie ukończyłem, lecz również jedyne przy którym nikt mi nie pomagał. Nie licząc samego pomysłu, który zawdzięczam
            swojej serdecznej koleżance, która niestety nie zgodziła się abym ją tutaj umieścił, ale jeśli to czyta, to chcę żeby wiedziała, że jestem jej ogromnie wdzięczny za to co dla mnie zrobiła. Bez niej tej
            strony by po prostu nie było.
        </p>
        <p>
            Na codzień zajmuję się tym co najbardziej lubię, czyli właśnie <b>programowaniem</b> i robiłem to już od dzieciństwa, lecz na poważnie zacząłem się tym interesować dopiero w 2 klasie technikum, po jednej
            z lekcji z <i>projektowania stron internetowych</i>. Od razu wiedziałem, że będzie to coś, czym będę chciał się zajmować już do końca życia.
        </p>
        <p>
            Moim największym hobby zaraz po programowaniu, to <b>psychologia</b>. Dzięki niej czuję się uwrażliwiony na wiele procesów zachodzących w naszych mózgach. Z dziedziny posiadanych umiejętności i wiedzy,
            chyba najciekawszym okazał się efekt <i>Krugera-Dunninga</i>, przez co nie przepadam odpowiadać na pytania w stylu <cite>"Jak w skali od 1-10 oceniasz swoje umiejętności w dziedzinie x?"</cite>, w końcu skąd
            mam wiedzieć, że osoba której udzielam odpowiedzi, w ogóle wie o istnieniu takiego procesu?
        </p>
        <p>Oprócz tego jest jeszcze sporo rzeczy którymi lubię się zajmować, ale nie widzę sensu wymieniania tutaj wszystkiego, dlatego jeśli ktoś jest zainteresowany,
            zapraszam do wysyłania wiadomości na adres <a href="mailto:kromacie@gmail.com">kromacie@gmail.com</a>, a oprócz tego zapraszam do testowania strony!
        </p>
        <h3>
            Dla potencjalnych pracodawców
        </h3>
        <p>
            Ta strona została stworzona przez <i>backend developera</i> w języku <b>PHP 7.3</b> przy użyciu frameworka <b>Laravel 5.8</b>.
        </p>
        <p>
            Stack technologiczny
        </p>
        <ul>
            <li>PHP 7.3</li>
            <li>Redis pub/sub</li>
            <li>Redis cache</li>
            <li>Git</li>
            <li>Laravel 5.8</li>
            <li>Laravel Queue</li>
            <li>Laravel Broadcasting</li>
            <li>Laravel Eloquent</li>
            <li>Laravel Echo</li>
            <li>Markdown</li>
            <li>Socket.io</li>
            <li>MySQL</li>
            <li>jQuery</li>
            <li>jQuery - Ajax</li>
            <li>Css - Flexbox</li>
            <li>Sass</li>
            <li>Blade</li>
        </ul>
        <p>
            Kod udostępnię na życzenie jeśli będzie to niezbędne w procesie rekrutacji.
        </p>
        <h3></h3>
    </div>
@endsection