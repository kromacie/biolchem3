@extends('layouts.app')

@section('content')
    <ul class="breadcrumb">
        <li class="breadcrumb-item selected">
            <a href="{{ route('index') }}" class="icon icon-home breadcrumb-link"></a>
        </li>
        <li class="breadcrumb-item"><a class="breadcrumb-link" href="{{ route('users.subjects.show', $subject->slug) }}"><i class="breadcrumb-icon icon {{ $subject->icon }}"></i>{{ $subject->name }}</a></li>
        <li class="breadcrumb-item"><a class="breadcrumb-link" href="{{ route('users.subjects.sheets.index', $subject->slug) }}"><i class="breadcrumb-icon icon icon-sheet"></i>Arkusze</a></li>
    </ul>
    <div class="flex">
        <div id="filters" class="filters-wrapper">
            <div class="filters">
                <div class="filters-title">
                    <h2 class="title title-inline-full title-primary title-gradient">
                        <i class="filters-icon title-icon icon icon-calendar"></i>Filtry
                    </h2>
                </div>
                <form id="filters-form" class="filters-body" method="get" action="{{ route('users.subjects.sheets.index', $subject->slug) }}">
                    <div class="filters-filters">
                        <div class="filters-filters-title">
                            <h2>Daty</h2>
                        </div>
                        <div class="flex flex-col flex-row-lg">
                            <ul class="filters-list">
                                @foreach($years as $year)
                                    <li class="filters-list-item">
                                        <label class="input-checkbox-container">
                                            <input class="input-checkbox" name="years[]" value="{{ $year->year }}" {{ key_exists($year->year, $c_years) ? 'checked' : '' }} type="checkbox"> {{ $year->year }}
                                            <span class="input-checkmark"></span>
                                        </label>
                                    </li>
                                @endforeach
                            </ul>
                            <ul class="filters-list">
                                @foreach($months as $month)
                                    <li class="filters-list-item">
                                        <label class="input-checkbox-container">
                                            <input class="input-checkbox" name="months[]" value="{{ $month->month }}" {{ key_exists($month->month, $c_months) ? 'checked' : '' }} type="checkbox"> {{ $month->month }}
                                            <span class="input-checkmark"></span>
                                        </label>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="filters-filters-title">
                            <h2>Typ arkuszy</h2>
                        </div>
                        <ul class="filters-list filters-checkbox-toggle">
                            @foreach($types as $type)
                                <li class="filters-list-item">
                                    <label class="input-checkbox-container">
                                        <input class="input-checkbox" type="checkbox" name="types[]" value="{{ $type->slug }}" {{ key_exists($type->slug, $c_types) ? 'checked' : '' }}> {{ $type->name }}
                                        <span class="input-checkmark"></span>
                                    </label>
                                </li>
                            @endforeach
                        </ul>
                        <div class="filters-filters-title">
                            <h2>Poziom arkuszy</h2>
                        </div>
                        <ul class="filters-list filters-checkbox-toggle">
                            @foreach($tiers as $tier)
                                <li class="filters-list-item">
                                    <label class="input-checkbox-container">
                                        <input class="input-checkbox" type="checkbox" name="tiers[]" value="{{ $tier->slug }}" {{ key_exists($tier->slug, $c_tiers) ? 'checked' : '' }}> {{ $tier->name }}
                                        <span class="input-checkmark"></span>
                                    </label>
                                </li>
                            @endforeach
                        </ul>
                        <div class="filters-filters-title">
                            <h2>Wersja arkuszy</h2>
                        </div>
                        <ul class="filters-list filters-checkbox-toggle">
                            @foreach($versions as $version)
                                <li class="filters-list-item">
                                    <label class="input-checkbox-container">
                                        <input class="input-checkbox" type="checkbox" name="versions[]" value="{{ $version->slug }}" {{ key_exists($version->slug, $c_versions) ? 'checked' : '' }}> {{ $version->description }}
                                        <span class="input-checkmark"></span>
                                    </label>
                                </li>
                            @endforeach
                        </ul>
                        <div class="filters-filters-title">
                            <h2>Popularność</h2>
                        </div>
                        <ul class="filters-list filters-checkbox-toggle">
                            <li class="filters-list-item">
                                <label class="input-checkbox-container">
                                    <input class="input-checkbox" type="radio" name="popularity" value="desc" {{ $c_popularity == 'desc' ? 'checked' : '' }}> Najbardziej
                                    <span class="input-checkmark"></span>
                                </label>
                            </li>
                            <li class="filters-list-item">
                                <label class="input-checkbox-container">
                                    <input class="input-checkbox" type="radio" name="popularity" value="asc" {{ $c_popularity == 'asc' ? 'checked' : '' }}> Najmniej
                                    <span class="input-checkmark"></span>
                                </label>
                            </li>
                        </ul>
                        <div class="filters-filters-title">
                            <h2>Poziom trudności</h2>
                        </div>
                        <ul class="filters-list filters-checkbox-toggle">
                            <li class="filters-list-item">
                                <label class="input-checkbox-container">
                                    <input class="input-checkbox" type="radio" name="difficulty" value="desc" {{ $c_difficulty == 'desc' ? 'checked' : '' }}> Najtrudniejsze
                                    <span class="input-checkmark"></span>
                                </label>
                            </li>
                            <li class="filters-list-item">
                                <label class="input-checkbox-container">
                                    <input class="input-checkbox" type="radio" name="difficulty" value="asc" {{ $c_difficulty == 'asc' ? 'checked' : '' }}> Najłatwiejsze
                                    <span class="input-checkmark"></span>
                                </label>
                            </li>
                        </ul>
                        <div class="filters-buttons">
                            <button id="reset-filters" class="filters-button button button-danger button-inline">Resetuj</button>
                            <button class="filters-button button button-secondary button-inline">Filtruj</button>
                        </div>
                    </div>
                </form>
                <i class="expand-toggler filters-mobile-toggler" data-target="#filters"></i>
            </div>
        </div>
        <div class="section col col-9-xl">
            <div class="section-header">
                <h1 class="title title-primary">
                    <i class="title-icon icon icon-sheet"></i> Arkusze
                </h1>
            </div>
            <div class="section-body article-list">
                <div class="article">
                    <div class="article-header">
                        <h4 class="tag-filters-title">
                            Wyniki
                        </h4>
                    </div>
                    <div class="article-body">
                        <div class="pagination">
                            <span class="pagination-result">Ilość wyników: <strong>{{ $sheets->total() }}</strong></span>
                            @if($sheets->currentPage() > 1)
                                <div class="pagination-item">
                                    <a class="pagination-link pagination-link-first" href="{{ $sheets->url(1) }}"></a>
                                </div>
                            @endif
                            @if($sheets->currentPage() > 1)
                                <span class="pagination-item">
                                <a class="pagination-link pagination-link-previous" href="{{ $sheets->previousPageUrl() }}"></a>
                            </span>
                            @endif
                            <span class="pagination-item">{{ $sheets->currentPage() }}</span>
                            @if($sheets->hasMorePages())
                                <span class="pagination-item">
                                <a class="pagination-link pagination-link-next" href="{{ $sheets->nextPageUrl() }}"></a>
                            </span>
                                <div class="pagination-item">
                                    <a class="pagination-link pagination-link-last" href="{{ $sheets->url($sheets->lastPage()) }}"></a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                @foreach($sheets as $sheet)
                    <article class="article article-no-gutters-top">
                        <div class="mini-statistic-container">
                            <div class="mini-statistic">
                                <span class="mini-statistic-icon icon icon-user">
                                    <span class="mini-statistic-icon-description">
                                        Rozwiązanych arkuszy
                                    </span>
                                </span>
                                <span class="mini-statistic-value">{{ $sheet->resolved_sheets_count }}</span>
                            </div>
                            <div class="mini-statistic">
                                <span class="mini-statistic-icon icon icon-calendar-check">
                                    <span class="mini-statistic-icon-description">
                                        Średni wynik
                                    </span>
                                </span>
                                <span class="mini-statistic-value">
                                    {{ $sheet->percentage_result }}%
                                </span>
                            </div>
                            @if($sheet->version->display_name)
                                <div class="mini-statistic mini-statistic-right">
                                    <span class="mini-statistic-icon icon icon-info-circle">
                                    </span>
                                    <span class="mini-statistic-value">{{ $sheet->version->description }}</span>
                                </div>
                            @endif
                        </div>
                        <div class="article-header article-header-has-number">
                            <span class="article-number article-number-primary">1</span>
                            <h2 class="article-title title">
                                Matura {{ $sheet->month }} {{ $sheet->year }}, {{ $sheet->tier->name }} {{ $sheet->version->display_name ? $sheet->version->short_description : '' }}<br />
                                <span class="article-title-under-text">{{ $sheet->points }} pkt</span>
                            </h2>

                        </div>
                        <div class="article-body">
                            <div class="article-button-container">
                                <a class="button button-primary button-inline article-button" href="{{ route('users.subjects.sheets.show', [$subject->slug, $sheet->slug]) }}">
                                    Rozwiązuj online
                                </a>
                                <button class="button button-primary article-button expand-toggler mini-question-toggler" data-target="#sheet-questions-{{ $sheet->id }}">
                                    Zobacz zadania
                                </button>
                            </div>
                        </div>
                        <div id="sheet-questions-{{ $sheet->id }}" class="article-body expand-target mini-question-wrapper">
                            <div class="mini-question-container">
                                @foreach($sheet->questions as $key => $question)
                                    <div class="mini-question">
                                <span class="mini-question-number">
                                    {{ $key + 1 }}
                                </span>
                                        <div class="mini-question-content">
                                            <span class="mini-question-title">{{ $question->title }}</span>
                                            <sup>{{ $question->points }}pkt</sup>
                                            <div class="stats-row mini-question-stat stat-primary">
                                                <div class="stats-title mini-question-stat-title">Średni wynik</div>
                                                <div class="stats-bar mini-question-bar">
                                                    <span class="stats-progress-bar active" style="width: 87%">87%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mini-question-buttons">
                                            <a class="mini-link mini-link-go" href="#">
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </article>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function () {
            $("#reset-filters").on('click', function (e) {
                e.preventDefault();
                $('#filters-form input:checkbox').removeAttr('checked');
            })
        });
    </script>
@endsection