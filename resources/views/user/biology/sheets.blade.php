@extends('layouts.app')

@section('content')
    <ul class="breadcrumb">
        <li class="breadcrumb-item selected">
            <a href="{{ route('index') }}" class="icon icon-home breadcrumb-link"></a>
        </li>
        <li class="breadcrumb-item"><a class="breadcrumb-link" href="{{ route('biology') }}"><i class="breadcrumb-icon icon icon-dna"></i>Biologia</a></li>
        <li class="breadcrumb-item"><a class="breadcrumb-link" href="{{ route('biology') }}"><i class="breadcrumb-icon icon icon-sheet"></i>Arkusze</a></li>
    </ul>
    <div class="flex">
        <div id="filters" class="filters-wrapper">
            <div class="filters">
                <div class="filters-title">
                    <h2 class="title title-inline-full title-primary title-gradient">
                        <i class="filters-icon title-icon icon icon-calendar"></i>Filtry
                    </h2>
                </div>
                <div class="filters-body">
                    <div class="filters-filters">
                        <div class="filters-filters-title">
                            <h2>Daty</h2>
                        </div>
                        <div class="flex flex-col flex-row-lg">
                            <ul class="filters-list">
                                @foreach($years as $year)
                                <li class="filters-list-item">
                                    <label class="input-checkbox-container">
                                        <input class="input-checkbox" type="checkbox"> {{ $year->year }}
                                        <span class="input-checkmark"></span>
                                    </label>
                                </li>
                                @endforeach
                            </ul>
                            <ul class="filters-list">
                                @foreach($months as $month)
                                <li class="filters-list-item">
                                    <label class="input-checkbox-container">
                                        <input class="input-checkbox" type="checkbox"> {{ $month->month }}
                                        <span class="input-checkmark"></span>
                                    </label>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="filters-filters-title">
                            <h2>Kategorie</h2>
                        </div>
                        <ul class="filters-list filters-checkbox-toggle">
                            <li class="filters-list-item">
                                <label class="input-checkbox-container">
                                    <input class="input-checkbox" type="checkbox"> Wirusy, wriony, wroidy
                                    <span class="input-checkmark"></span>
                                </label>
                            </li>
                            <li class="filters-list-item">
                                <label class="input-checkbox-container">
                                    <input class="input-checkbox" type="checkbox"> Ekologia i zagrożenia
                                    <span class="input-checkmark"></span>
                                </label>
                            </li>
                            <li class="filters-list-item">
                                <label class="input-checkbox-container">
                                    <input class="input-checkbox" type="checkbox"> Grzyby
                                    <span class="input-checkmark"></span>
                                </label>
                            </li>
                            <li class="filters-list-item">
                                <div class="filters-toggler-container">
                                    <i class="filters-expand-toggler expand-toggler" data-target="#filters-expander-1"></i>
                                    <label class="input-checkbox-container input-checkbox-toggler" data-target="#filters-expander-1">
                                        <input class="input-checkbox" type="checkbox"> Ekologia i fizjologia zwierząt
                                        <span class="input-checkmark"></span>
                                    </label>
                                </div>
                                <div id="filters-expander-1" class="filters-expand-target expand-target">
                                    <ul class="filters-list">
                                        <li class="filters-list-item">
                                            <label class="input-checkbox-container">
                                                <input class="input-checkbox" type="checkbox"> Układ pokarmowy i żywienie
                                                <span class="input-checkmark"></span>
                                            </label>
                                        </li>
                                        <li class="filters-list-item">
                                            <label class="input-checkbox-container">
                                                <input class="input-checkbox" type="checkbox"> Układ hormonalny
                                                <span class="input-checkmark"></span>
                                            </label>
                                        </li>
                                        <li class="filters-list-item">
                                            <label class="input-checkbox-container">
                                                <input class="input-checkbox" type="checkbox"> Układ nerwowy i narządy zmysłów
                                                <span class="input-checkmark"></span>
                                            </label>
                                        </li>
                                        <li class="filters-list-item">
                                            <label class="input-checkbox-container">
                                                <input class="input-checkbox" type="checkbox"> Układ pokarmowy i żywienie
                                                <span class="input-checkmark"></span>
                                            </label>
                                        </li>
                                        <li class="filters-list-item">
                                            <label class="input-checkbox-container">
                                                <input class="input-checkbox" type="checkbox"> Układ immunologiczny
                                                <span class="input-checkmark"></span>
                                            </label>
                                        </li>
                                        <li class="filters-list-item">
                                            <label class="input-checkbox-container">
                                                <input class="input-checkbox" type="checkbox"> Układ krążenia
                                                <span class="input-checkmark"></span>
                                            </label>
                                        </li>
                                        <li class="filters-list-item">
                                            <label class="input-checkbox-container">
                                                <input class="input-checkbox" type="checkbox"> Układ oddechowy
                                                <span class="input-checkmark"></span>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="filters-list-item">
                                <div class="filters-toggler-container">
                                    <label class="input-checkbox-container input-checkbox-toggler" data-target="#filters-expander-2">
                                        <input class="input-checkbox" type="checkbox"> Rośliny
                                        <span class="input-checkmark"></span>
                                    </label>
                                    <i class="filters-expand-toggler expand-toggler" data-target="#filters-expander-2"></i>
                                </div>
                                <div id="filters-expander-2" class="filters-expand-target expand-target">
                                    <ul class="filters-list">
                                        <li class="filters-list-item">
                                            <label class="input-checkbox-container">
                                                <input class="input-checkbox" type="checkbox"> Mszaki
                                                <span class="input-checkmark"></span>
                                            </label>
                                        </li>
                                        <li class="filters-list-item">
                                            <label class="input-checkbox-container">
                                                <input class="input-checkbox" type="checkbox"> Paprotniki
                                                <span class="input-checkmark"></span>
                                            </label>
                                        </li>
                                        <li class="filters-list-item">
                                            <label class="input-checkbox-container">
                                                <input class="input-checkbox" type="checkbox"> Nasienne
                                                <span class="input-checkmark"></span>
                                            </label>
                                        </li>
                                        <li class="filters-list-item">
                                            <label class="input-checkbox-container">
                                                <input class="input-checkbox" type="checkbox"> Pozostałe
                                                <span class="input-checkmark"></span>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                        <div class="filters-buttons">
                            <button class="filters-button button button-danger button-inline">Resetuj</button>
                            <button class="filters-button button button-secondary button-inline">Filtruj</button>
                        </div>
                    </div>
                </div>
                <i class="expand-toggler filters-mobile-toggler" data-target="#filters"></i>
            </div>
        </div>
        <div class="section col col-9-xl">
            <div class="section-header">
                <h1 class="title title-primary">
                    <i class="title-icon icon icon-sheet"></i> Arkusze
                </h1>
            </div>
            <div class="section-body article-list">
                <div class="article">
                    <div class="article-header">
                        <h4 class="tag-filters-title">
                            Wyniki
                        </h4>
                    </div>
                    <div class="article-body">
                        <div class="pagination">
                            <span class="pagination-result">Ilość wyników: <strong>{{ $sheets->count() }}</strong></span>
                            @if($sheets->currentPage() > 1)
                            <div class="pagination-item">
                                <a class="pagination-link pagination-link-first" href="{{ $sheets->url(1) }}"></a>
                            </div>
                            @endif
                            @if($sheets->currentPage() > 1)
                            <span class="pagination-item">
                                <a class="pagination-link pagination-link-previous" href="{{ $sheets->previousPageUrl() }}"></a>
                            </span>
                            @endif
                            @if($sheets->hasMorePages())
                            <span class="pagination-item">{{ $sheets->currentPage() }}</span>
                            <span class="pagination-item">
                                <a class="pagination-link pagination-link-next" href="{{ $sheets->nextPageUrl() }}"></a>
                            </span>
                            <div class="pagination-item">
                                <a class="pagination-link pagination-link-last" href="{{ $sheets->lastPage() }}"></a>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                @foreach($sheets as $sheet)
                    <article class="article article-no-gutters-top">
                    <div class="mini-statistic-container">
                        <div class="mini-statistic">
                                <span class="mini-statistic-icon icon icon-user">
                                    <span class="mini-statistic-icon-description">
                                        Rozwiązanych arkuszy
                                    </span>
                                </span>
                            <span class="mini-statistic-value">12</span>
                        </div>
                        <div class="mini-statistic">
                                <span class="mini-statistic-icon icon icon-calendar-check">
                                    <span class="mini-statistic-icon-description">
                                        Średni wynik
                                    </span>
                                </span>
                            <span class="mini-statistic-value">76%</span>
                        </div>
                        @if($sheet->type->display_name)
                            <div class="mini-statistic mini-statistic-right">
                                    <span class="mini-statistic-icon icon icon-info-circle">
                                    </span>
                                <span class="mini-statistic-value">{{ $sheet->type->description }}</span>
                            </div>
                        @endif
                    </div>
                    <div class="article-header article-header-has-number">
                        <span class="article-number article-number-primary">1</span>
                        <h2 class="article-title title">
                            Matura {{ $sheet->month }} {{ $sheet->year }}, Poziom rozszerzony {{ $sheet->type->display_name ? $sheet->type->short_description : '' }}<br />
                            <span class="article-title-under-text">{{ $sheet->points }} pkt</span>
                        </h2>

                    </div>
                    <div class="article-body">
                        <div class="article-button-container">
                            <a class="button button-primary button-inline article-button" href="">
                                Rozwiązuj online
                            </a>
                            <button class="button button-primary article-button expand-toggler mini-question-toggler" data-target="#sheet-questions-{{ $sheet->id }}">
                                Zobacz zadania
                            </button>
                        </div>
                    </div>
                    <div id="sheet-questions-{{ $sheet->id }}" class="article-body expand-target mini-question-wrapper">
                        <div class="mini-question-container">
                            @foreach($sheet->questions as $key => $question)
                                <div class="mini-question">
                                <span class="mini-question-number">
                                    {{ $key + 1 }}
                                </span>
                                <div class="mini-question-content">
                                    <span class="mini-question-title">{{ $question->title }}</span>
                                    <sup>{{ $question->points }}pkt</sup>
                                    <div class="stats-row mini-question-stat stat-primary">
                                        <div class="stats-title mini-question-stat-title">Średni wynik</div>
                                        <div class="stats-bar mini-question-bar">
                                            <span class="stats-progress-bar active" style="width: 87%">87%</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mini-question-buttons">
                                    <a class="mini-link mini-link-go" href="#">
                                    </a>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </article>
                @endforeach
            </div>
        </div>
    </div>
@endsection