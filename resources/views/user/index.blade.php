@extends('layouts.app')

@section('content')
    <ul class="breadcrumb">
        <li class="breadcrumb-item selected">
            <i class="icon icon-home"></i>
        </li>
    </ul>

    <div class="section">
        <div class="section-header">
            <h1 class="title title-primary title-gradient"><i class="title-icon icon icon-flag"></i>Rozpocznij przygodę!</h1>
        </div>
        <div class="section-body">
            <article class="article">
                <article class="article-body">
                    <p class="article-text article-text-intended">
                        Chcesz rozwiązywać <b>arkusze</b> lub <b>zadania</b> maturalne z ostatnich lat, ale nie znalazłeś miejsca gdzie mógłbyś zrobić to online? Jeśli tak, to dobrze trafiłeś.
                        Posiadamy zadania które nie ograniczają się do klasycznego <i>wybierz A) lub B)</i>, lecz cały system, który integruje ze sobą różne wersje zadań i sposobów ich rozwiązywania!
                    </p>
                    <p>
                        Są to między innymi:
                    </p>
                    <ul>
                        <li>Zadania wielokrotnego wyboru</li>
                        <li>Zadania jednokrotnego wyboru</li>
                        <li>Podaj definicje</li>
                        <li>Uzupełnij wzór</li>
                    </ul>
                    <p>
                        Strona jest obecnie w wersji beta.
                    </p>
                </article>
            </article>
        </div>
    </div>
    <div class="section">
        <div class="section-header">
            <h1 class="title title-primary title-gradient"><i class="title-icon icon icon-sheet"></i>Arkusze</h1>
        </div>
        <div class="section-body">
            <div class="article-list">
                <article class="article">
                    <div class="article-body">
                        <p class="article-text article-text-intended">
                            Posiadamy <b>arkusze maturalne</b> sprzed kilku lat, są one na bieżąco aktualizowane i sprawdzanie. Właśnie tutaj możesz sprawdzić swoją wiedzę rozwiązując <b>interaktywne testy</b>, porównaj je z innymi i zobacz na jakim wypadasz poziomie! Możesz również wybrać losowe pytanie.
                        </p>
                    </div>
                </article>
            </div>
        </div>
    </div>
    @isset($subjects)
    <div class="section">
        <div class="section-body card-container">
            @foreach($subjects->reverse() as $subject)
            <div class="card col card-primary highlight-box">
                <div class="card-header">
                    <h2 class="title title-{{ $subject->theme }} title-inline">
                        <i class="title-icon icon {{ $subject->icon }}"></i>{{ $subject->name }}
                    </h2>
                </div>
                <div class="card-body">
                    <div class="mini-cards">
                        <div class="mini-cards-header">
                            <ul id="calendar-bar-{{ $subject->id }}" class="calendar-bar calendar-bar-{{ $subject->theme }}">
                                <li class="calendar-bar-icon icon icon-calendar"></li>
                                @foreach($subject->sheets as $sheet)
                                    <li data-sheet="{{ $sheet->id }}" data-subject="{{ $subject->id }}" id="mini-card-calendar-{{ $sheet->id }}" class="calendar-bar-item">{{ $sheet->year }}</li>
                                @endforeach
                            </ul>
                        </div>
                        <div id="mini-cards-body-list-{{ $subject->id }}" class="mini-cards-body-list">
                            @foreach($subject->sheets as $sheet)
                                <div id="mini-card-body-{{ $sheet->id }}" class="mini-cards-body">
                                    <h3 class="mini-cards-body-title">
                                        Statystyki
                                    </h3>
                                    <div class="mini-cards-stats stats stats-{{ $subject->theme }}">
                                        <div class="stats-row">
                                            <div class="stats-title">Rozwiązanych arkuszy</div>
                                            <div class="stats-bar">
                                                <span class="stats-progress-bar" style="width: {{ $sheet->percentageOfTotalResolvedSheets($subject->total_resolved_sheets_count) }}%">{{ $sheet->resolved_sheets_count }}</span>
                                            </div>
                                        </div>
                                        <div class="stats-row">
                                            <div class="stats-title">Średni wynik</div>
                                            <div class="stats-bar">
                                                <span class="stats-progress-bar" style="width: {{ $sheet->percentage_result }}%">{{ $sheet->percentage_result }}%</span>
                                            </div>
                                        </div>
                                        <div class="stats-row">
                                            <div class="stats-title">Ilość arkuszy</div>
                                            <div class="stats-bar">
                                                <span class="stats-progress-bar" style="width: {{ $sheet->percentageOfTotalSheets($subject->total_sheets_count) }}%">{{ $sheet->count }}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="mini-cards-button button button-{{ $subject->theme }} button-inline" onclick="window.location.href = `{{ route('users.subjects.sheets.index', [$subject->slug, $sheet->slug, 'years' => [$sheet->year]]) }}`">Rozwiązuj</button>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
    @endisset
@endsection

@section('scripts')
    <script type="text/javascript">
        $(function () {
            init();
        });

        function init() {
            $('.calendar-bar').each(function (index, element) {
                let item = $(element).find('.calendar-bar-item').first();

                select(item.data('subject'), item.data('sheet'))
            });
        }

        function select(subject, sheet) {
            uncheck(subject);
            $('#mini-card-calendar-' + sheet).addClass('selected');
            $('#mini-card-body-' + sheet).addClass('is-visible');
        }

        function uncheck(subject) {
            $('#mini-cards-body-list-' + subject + ' .mini-cards-body').removeClass('is-visible');
            $('#calendar-bar-' + subject + ' .calendar-bar-item').removeClass('selected');
        }

        $('.calendar-bar-item').on('click', function () {
            let element = $(this);

            console.log(element);

            select(element.data('subject'), element.data('sheet'));
        });
    </script>
@endsection

@section('footer')
    <footer class="footer-container">
        <div class="footer container">
            <div class="flex flex-row-lg flex-col">
                <div class="col col-4">
                    <div class="footer-head">
                        <h3>Kontakt</h3>
                        <p>
                            kromacie@gmail.com
                        </p>
                    </div>
                </div>
                <div class="col col-4">
                    <div class="footer-head">
                        <h3>Twórca</h3>
                        <p>
                            Maciej Król
                        </p>
                    </div>
                </div>
                <div class="col col-4">
                    <div class="footer-head">
                        <h3>Licencja</h3>
                        <p>
                            Beer-ware
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
@endsection