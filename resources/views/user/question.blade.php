@extends('layouts.app')

@section('content')
    <ul class="breadcrumb">
        <li class="breadcrumb-item selected">
            <a href="{{ route('index') }}" class="icon icon-home breadcrumb-link"></a>
        </li>
        <li class="breadcrumb-item"><a class="breadcrumb-link" href="{{ route('users.subjects.show', $subject->slug) }}"><i class="breadcrumb-icon icon {{ $subject->icon }}"></i>{{ $subject->name }}</a></li>
        <li class="breadcrumb-item"><a class="breadcrumb-link" href="{{ route('users.subjects.questions.index', $subject->slug) }}">Zadania</a></li>
        <li class="breadcrumb-item"><a class="breadcrumb-link icon icon-pencil" href="{{ route('users.subjects.questions.show', [$subject->slug, $question->slug]) }}"></a></li>
    </ul>
    <div class="flex flex-col flex-row-lg">
        <div class="section col col-3">
            <div class="section-header"></div>
            <div class="section-body">
                <div class="questions-sidebar questions-sidebar-media">
                    <div class="questions-sidebar-head">
                        <h2>Lista zadań</h2>
                    </div>
                    <ul class="questions-sidebar-list">
                        <li id="question-sidebar-{{ $question->id }}" class="questions-sidebar-item" data-question="{{ $question->id }}">{{ $question->title }}</li>
                    </ul>
                    <div class="button-container">
                        <button id="questions-button-check" class="button button-secondary button-inline questions-sidebar-button">Sprawdź</button>
                        <button id="questions-button-corrects-show" class="button button-primary button-inline questions-sidebar-button hidden">Zobacz odpowiedzi <i class="icon icon-show"></i></button>
                        <button id="questions-button-corrects-hide" class="button button-primary button-inline questions-sidebar-button hidden">Ukryj odpowiedzi <i class="icon icon-hide"></i></button>
                    </div>
                </div>
            </div>

        </div>
        <div class="section col col-9">
            <div class="section-header">
                {{--<div id="timer" class="timer">--}}
                    {{--<div class="timer-header">--}}
                        {{--Pozostały czas--}}
                    {{--</div>--}}
                    {{--<div class="timer-body">--}}
                        {{--<div class="timer-progress-bar active">12:45</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="mini-result-container">
                    <div id="mini-result" class="mini-result">
                        <div class="mini-result-title">
                            Zdobyte punkty
                        </div>
                        <div class="mini-result-bar">
                            <div class="mini-result-bar-progress-bar">
                                <span id="mini-result-points" class="mini-result-numbers">12 / 12 (56%)</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-body">
                <div class="questions-collection">
                    <article id="question-collection-{{ $question->id }}" class="questions-collection-item article article-no-gutters-top">
                    <div class="mini-statistic-container">
                        <div class="mini-statistic">
                        <span class="mini-statistic-icon icon icon-user">
                            <span class="mini-statistic-icon-description">
                                Rozwiązanych zadań
                            </span>
                        </span>
                            <span class="mini-statistic-value">{{ $question->resolved_questions_count }}</span>
                        </div>
                        <div class="mini-statistic">
                        <span class="mini-statistic-icon icon icon-calendar-check">
                            <span class="mini-statistic-icon-description">
                                Średni wynik
                            </span>
                        </span>
                            <span class="mini-statistic-value">{{ $question->percentage_result }}%</span>
                        </div>
                        @if($question->sheet->version->display_stat)
                            <div class="mini-statistic mini-statistic-right">
                                <span class="mini-statistic-icon icon icon-info-circle"></span>
                                <span class="mini-statistic-value">{{ $question->sheet->version->description }}</span>
                            </div>
                        @endif
                    </div>
                    <div class="article-header article-header-has-number">
                        <span class="article-number article-number-primary">1</span>
                        <h2 class="article-title title">
                            Matura {{ $question->sheet->release }}, {{ $question->sheet->tier->name }} {{ $question->sheet->version->display_name ? $question->sheet->version->short_description : '' }}<br />
                            <a class="primary" href="">{{ $question->title }} ({{ $question->points }} pkt)</a>
                        </h2>
                    </div>
                    <div class="article-header">
                        @if(!$question->tags->isEmpty())
                            <div class="tags">
                                @foreach($question->tags as $tag)
                                    <a class="tag tag-primary" href="#">{{ $tag->name }}</a>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="article-body">
                        @foreach($question->contents as $content)
                            @if($content->description)
                                <div class="article-text-intended">
                                    @markdown($content->description)
                                </div>
                            @endif
                            @if($content->image_id)
                                <div class="article-image-container article-image-container-half">
                                    <img class="article-image" src="{{ asset('storage/' . $content->image->path) }}" alt="">
                                </div>
                            @endif
                        @endforeach
                        @foreach($question->substitutes as $key => $substitute)
                            <div class="question">
                                <div class="question-header">
                                    <h2 class="question-title">{{ $question->title }}{{ $key + 1 }}</h2>
                                    <span class="question-points">
                                        {{ trans_choice('questions.points', $substitute->points, ['value' => $substitute->points]) }}
                                    </span>
                                </div>
                                <div class="question-body">
                                    @foreach($substitute->contents as $content)
                                        @if($content->description)
                                            @markdown($content->description)
                                        @endif
                                        @if($content->image_id)
                                            <div class="article-image-container article-image-container-half">
                                                <img class="article-image" src="{{ asset('storage/' .$content->image->path) }}" alt="">
                                            </div>
                                        @endif
                                    @endforeach
                                    <div class="answer-container">
                                        <h3 class="answer-title">
                                            Odpowiedzi
                                        </h3>
                                        <div class="answer">
                                            @foreach($substitute->inputGroups as $group)
                                                <div style="margin: 1rem 0">
                                                    @if($group->description)
                                                        @markdown($group->description)
                                                    @endif
                                                    @foreach($group->inputs as $input)
                                                        @include('components.input', ['group' => $group, 'input' => $input, 'question' => $question, 'substitute' => $substitute])
                                                    @endforeach
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </article>
                </div>
                <div id="status" class="status">
                    <div id="status-sending" class="status-item">
                        <span>Wysyłanie rządania...</span> <span class="status-item-loader"></span>
                    </div>
                    <div id="status-init" class="status-item">
                        <span>Oczekiwanie w kolejce...</span> <span class="status-item-loader"></span>
                    </div>
                    <div id="status-pending" class="status-item">
                        <span>Przetwarzanie...</span> <span class="status-item-loader"></span>
                    </div>
                    <div id="status-error" class="status-item">
                        <span>Wystąpił błąd na serwerze, spróbuj wysłać arkusz ponownie!</span>
                    </div>
                    <div id="status-done" class="status-item status-item-center status-item-expander">
                        <span>Sprawdzono!</span>
                        <span>Kliknij aby zobaczyć wynik</span>
                    </div>
                </div>
                <div id="result" class="result-container">
                    <div class="result">
                        <div class="result-title-container">
                            <h3 class="result-title">Twój wynik to:</h3>
                        </div>
                        <div class="result-points">
                            <span class="result-points-item" id="points_got">20</span> /
                            <span class="result-points-item" id="points_max">23</span>
                        </div>
                        <div class="result-buttons">
                            <button id="show_sheet" class="result-button button button-inline button-primary button-widther">Przeglądaj odpowiedzi</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="check-confirm" class="modal">
        <div class="modal-content small">
            <div class="modal-header">
                <h3 class="modal-title">Ostrzeżenie</h3>
                <button class="modal-close"></button>
            </div>
            <div class="modal-body">
                <p>Czy na pewno chcesz sprawdzić pytanie?</p>
                <p>Liczba niewypełnionych miejsc: <strong id="modal-empty-inputs">0</strong></p>
                <div class="button-container">
                    <button id="send-confirm" class="button button-secondary button-inline button-widther">Tak</button>
                    <button id="send-abort" class="button button-danger button-inline button-widther">Nie</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

        let selectedQuestion;
        let selectedQuestionId = '{{ $question->id }}';
        let pendingChecking = false;
        let uuid = "{{ $uuid }}";
        let url = "{{ route('users.subjects.questions.check', [$subject->slug, $question->id]) }}";
        let question_result_data = [];
        let finishedChecking = false;
        let subject = "{{ $subject->slug }}";

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        Echo.join(`subject.${subject}.sheet`);

        Echo.private(`question.${selectedQuestionId}.solving.${uuid}`)
            .listen('Question\\Start', function (e) {
                hideQuestions();
                hideSidefix();
                hideTimer();
                selectStatus('status-pending');
                questionsPendingAll();
                disableInputs();
            })
            .listen('Question\\Finish', function (e) {
                let question_id = e.data.question.id;
                let got_points = e.data.result.got_points;
                let max_points = e.data.result.max_points;

                question_result_data[question_id] = {
                    max_points: max_points,
                    got_points: got_points
                };

                if(got_points === max_points) {
                    setQuestionStatus(question_id, true);
                } else {
                    setQuestionStatus(question_id, false);
                }
                setResult(e.data.result);
                setMiniResultPoints(e.data.result);
                selectStatus('status-done');
                changeSidefixStateToResults();
                finishedChecking = true;
                updateSidefixPoints(selectedQuestionId);
                showAnswersWasCorrect(e.data.answers);
                parseQuestion(e.data.question);
            })
            .listen('Question\\Error', function (e) {
                pendingChecking = false;
                selectStatus('status-error');
                $('#questions-button-check').removeClass('hidden');
            });


        $('.questions-sidebar-item').on('click', function () {
            let id = $(this).data('question');
            selectQuestion(id);
        });

        function init() {
            let selected = selectedQuestionId;
            $('.questions-sidebar-item').each(function (index, item) {
                selectQuestion($(item).data('question'));
            });
            selectQuestion(selected);
        }

        function selectQuestion(id) {
            $('.questions-sidebar-item').removeClass('selected');
            $('.questions-collection-item').removeClass('selected');

            $('#question-sidebar-' + id).addClass('selected');
            $('#question-collection-' + id).addClass('selected');

            selectedQuestion = '#question-collection-' + id;
            selectedQuestionId = id;

            if(finishedChecking) {
                updateSidefixPoints(id);
            } else {
                checkEmptyInputs(id);
            }
        }

        function getEmptiesInputsCountForQuestion(id) {
            let inputs = $('#question-collection-' + id).find('input:enabled');
            let total = [];
            let has = 0;
            let checked = [];
            let checkboxes = [];
            inputs.each(function (index, element) {
                element = $(element);

                if(element.is('input:checkbox')) {
                    if($.inArray(element.data('group'), checkboxes) === -1) {
                        checkboxes.push(element.data('group'));
                    }
                } else {
                    total.push(element.attr('name'));
                }

                if(element.is('input:radio') && element.is(':checked')) {
                    has++;
                }
                if(element.is('input:checkbox') && element.is(':checked')) {
                    if($.inArray(element.data('group'), checked) === -1) {
                        checked.push(element.data('group'));
                        has++;
                    }
                }
                if(element.is('input:text') && element.val().length > 0) {
                    has++;
                }
            });

            total = $.unique(total).length + checkboxes.length;

            return total - has;
        }

        function checkEmptyInputs(id) {
            let empties = getEmptiesInputsCountForQuestion(id);

            if(empties < 1) {
                $('#question-sidebar-' + selectedQuestionId).addClass('done');
            } else {
                $('#question-sidebar-' + selectedQuestionId).removeClass('done');
            }

            $('#inputs_empty').html(empties);
        }

        function setMiniResultPoints(result)
        {
            let got_points = result.got_points;
            let max_points = result.max_points;

            let percent = Math.round(got_points / max_points * 100);

            let element = $('#mini-result-points');
            element.text(got_points + "/" + max_points + " (" + percent + "%)");
            element.parent().css('width', percent + "%");

        }

        function sendQuestion() {
            if(pendingChecking) {
                return;
            }
            hideQuestions();
            hideSidefix();
            hideTimer();
            selectStatus('status-sending');
            let toSend = {};
            let inputs = $('.questions-collection').find('input:enabled');

            inputs.each(function (index, element) {
                element = $(element);
                if(element.is('input:radio:checked')) {
                    toSend[element.prop('name') + "[" + element.data("input") + "]"] = element.val();
                } else if(element.is('input:checkbox:checked')) {
                    toSend[element.prop('name')] = element.val();
                } else if(element.is('input:text')) {
                    toSend[element.prop('name')] = element.val();
                }
            });
            console.log(toSend);
            pendingChecking = true;
            $.ajax({
                type: 'POST',
                url: url,
                data: toSend,
                success: function(msg){
                    selectStatus('status-init');
                    $('#questions-button-check').addClass('hidden');
                    //console.log(msg);
                },
                error: function (msg) {
                    $('#questions-button-check').removeClass('hidden');
                    selectStatus('status-error');
                    pendingChecking = false;
                }
            });
        }

        function selectStatus(id)
        {
            $('#status').addClass('is-visible');
            $('.status-item').removeClass('selected');
            $('#' + id).addClass('selected');
        }

        function updateSidefixPoints(question_id)
        {
            let data = question_result_data[question_id];

            $('#points_result').html(
                data.got_points + " / " + data.max_points
            );
        }

        function setQuestionStatus(id, status) {
            if(status) {
                $('#question-sidebar-' + id).addClass('question-mark').addClass('success').removeClass('pending');
            } else {
                $('#question-sidebar-' + id).addClass('question-mark').addClass('failure').removeClass('pending');
            }
        }

        function hideTimer() {
            $('#timer').addClass('hidden');
        }

        function hideQuestions()
        {
            $('.questions-collection').addClass('hidden');
        }

        function questionsPendingAll() {
            $('.questions-sidebar-item').addClass('pending');
        }

        function hideSidefix()
        {
            $('.questions-sidefix').addClass('hidden');
        }
        function showSidefix()
        {
            $('.questions-sidefix').removeClass('hidden');
        }

        $('.questions-collection-item').on('input', function() {
            checkEmptyInputs(selectedQuestionId);
        });

        $(function () {
            init();
            $(window).on("beforeunload", function(e) {
                return e.originalEvent.returnValue = "Czy na pewno chcesz opuścić tą stronę?";
            });
        });

        $('#questions-button-check').on('click', function () {
            let total = 0;
            $('.questions-sidebar-item').each(function (index, element) {
                let id = $(element).data('question');
                total += getEmptiesInputsCountForQuestion(id);
            });

            if(total > 0) {
                $('#modal-empty-inputs').html(total);
                $('#check-confirm').addClass('is-expanded');
            } else {
                sendQuestion();
            }
        });

        $('#send-abort').on('click', function () {
            $(this).closest('.modal').toggleClass('is-expanded');
        });

        $('#send-confirm').on('click', function () {
            $(this).closest('.modal').toggleClass('is-expanded');
            sendQuestion();
        });

        $('#status-done').on('click', function () {
            $(this).parent().addClass('is-expanded');
            $('#result').addClass('is-expanded');
            $('.questions-sidebar-item').removeClass('question-mark');
        });

        function changeSidefixStateToResults()
        {
            $('.questions-sidefix-description').addClass('hidden');
            $('.questions-sidefix-points').addClass('is-visible');
        }

        function setResult(result) {
            $('#points_max').text(result.max_points);
            $('#points_got').text(result.got_points);
            $('#mini-max_points').text(result.max_points);
            $('#mini-got_points').text(result.got_points);
        }

        $('#show_sheet').on('click', function () {
            $('#result').removeClass('is-expanded');
            $('.questions-collection').removeClass('hidden');
            $('#questions-button-check').addClass('hidden');
            $('#questions-button-corrects-show').removeClass('hidden');
            showSidefix();
            showMiniResult();

        });

        $('#questions-button-corrects-show').on('click', function () {
            $(this).addClass('hidden');
            $('#questions-button-corrects-hide').removeClass('hidden');
            showCorrectAnswers();
        });

        $('#questions-button-corrects-hide').on('click', function () {
            $(this).addClass('hidden');
            $('#questions-button-corrects-show').removeClass('hidden');
            hideCorrectAnswers();
        });

        function disableInputs() {
            $('.questions-collection input').prop('disabled', true);
        }

        function showCorrectAnswers()
        {
            let inputs = $('.questions-collection input:not(.success)');

            inputs.each(function (index, element) {
                element = $(element);


                if(element.data('correct')) {
                    element.addClass('proposed');

                    if(element.data('group_name') === "MULTIPLE_RADIO") {
                        element.prop('checked', true);
                    } else if (element.data('group_name') === "SINGULAR_CHECKBOX") {
                        element.prop('checked', true);
                    } else {
                        element.attr('data-user_answer', element.val());
                        element.val(element.data('correct'));
                    }
                }
            });
        }

        function showMiniResult()
        {
            $('#mini-result').addClass('is-visible');
        }

        function hideCorrectAnswers()
        {
            let inputs = $('.questions-collection input:not(.success)');

            inputs.each(function (index, element) {
                element = $(element);


                if(element.data('correct')) {

                    element.removeClass('proposed');

                    if(element.data('group_name') === "MULTIPLE_RADIO") {
                        element.prop('checked', false);
                    } else if (element.data('group_name') === "SINGULAR_CHECKBOX") {
                        element.prop('checked', false);
                    } else {
                        element.val(element.data('user_answer'));
                    }
                }
            });
        }

        function showAnswersWasCorrect(collection) {
            $.each(collection, function (i_1, question) {
                $.each(question, function (i_2, substitute) {
                    $.each(substitute, function (i_3, group) {
                        $.each(group, function (i_4, input) {
                            let element = $('#answer_' + i_1 + '_' + i_2 + '_' + i_3 + '_' + i_4);

                            if(input.correct) {
                                element.addClass('success');
                            } else {
                                element.addClass('failure');
                            }
                        })
                    })
                })
            });
        }

        function parseQuestion(question) {
            $.each(question.substitutes, function (i_2, substitute) {
                $.each(substitute.input_groups, function (i_3, group) {
                    $.each(group.inputs, function (i_4, input) {
                        if(input.corrects.length > 0) {
                            let correct = input.corrects[0];

                            let element = $('#answer_' + question.id + '_' + substitute.id + '_' + group.id + '_' + input.id);
                            element.attr('data-group_name', group.type.name);
                            element.attr('data-correct', correct.value);
                        }
                    })
                })
            })
        }
    </script>
@endsection