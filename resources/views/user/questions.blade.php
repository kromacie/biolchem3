@extends('layouts.app')

@section('content')
    <ul class="breadcrumb">
        <li class="breadcrumb-item selected">
            <a href="{{ route('index') }}" class="icon icon-home breadcrumb-link"></a>
        </li>
        <li class="breadcrumb-item"><a class="breadcrumb-link" href="{{ route('users.subjects.show', $subject->slug) }}"><i class="breadcrumb-icon icon {{ $subject->icon }}"></i>{{ $subject->name }}</a></li>
        <li class="breadcrumb-item"><a class="breadcrumb-link" href="{{ route('users.subjects.sheets.index', $subject->slug) }}">Zadania</a></li>
    </ul>
    <div class="flex">
        <div id="filters" class="filters-wrapper">
            <div class="filters">
                <div class="filters-title">
                    <h2 class="title title-inline-full title-primary title-gradient">
                        <i class="filters-icon title-icon icon icon-calendar"></i>Filtry
                    </h2>
                </div>
                <form id="filters-form" class="filters-body" method="get" action="{{ route('users.subjects.questions.index', $subject->slug) }}">
                    <div class="filters-filters">
                        <div class="filters-filters-title">
                            <h2>Daty</h2>
                        </div>
                        <div class="flex flex-col flex-row-lg">
                            <ul class="filters-list">
                                @foreach($years as $year)
                                    <li class="filters-list-item">
                                        <label class="input-checkbox-container">
                                            <input class="input-checkbox" name="years[]" value="{{ $year->year }}" {{ key_exists($year->year, $c_years) ? 'checked' : '' }} type="checkbox"> {{ $year->year }}
                                            <span class="input-checkmark"></span>
                                        </label>
                                    </li>
                                @endforeach
                            </ul>
                            <ul class="filters-list">
                                @foreach($months as $month)
                                    <li class="filters-list-item">
                                        <label class="input-checkbox-container">
                                            <input class="input-checkbox" name="months[]" value="{{ $month->month }}" {{ key_exists($month->month, $c_months) ? 'checked' : '' }} type="checkbox"> {{ $month->month }}
                                            <span class="input-checkmark"></span>
                                        </label>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="filters-filters-title">
                            <h2>Kategorie</h2>
                        </div>
                        <ul class="filters-list filters-checkbox-toggle">
                            @foreach($tags as $tag)
                                @if($tag->tags->isEmpty())
                                <li class="filters-list-item">
                                    <label class="input-checkbox-container">
                                        <input class="input-checkbox" type="checkbox" name="tags[]" value="{{ $tag->slug }}" {{ key_exists($tag->slug, $c_tags) ? 'checked' : '' }}> {{ $tag->name }}
                                        <span class="input-checkmark"></span>
                                    </label>
                                </li>
                                @else
                                    <div class="filters-toggler-container">
                                        <i class="filters-expand-toggler expand-toggler {{ key_exists($tag->slug, $c_categories) ? 'is-expanded' : '' }}" data-target="#filters-expander-{{ $tag->id }}"></i>
                                        <label class="input-checkbox-container input-checkbox-toggler" data-target="#filters-expander-{{ $tag->id }}">
                                            <input class="input-checkbox" type="checkbox" name="tags[]" value="{{ $tag->slug }}" {{ key_exists($tag->slug, $c_tags) ? 'checked' : '' }}> {{ $tag->name }}
                                            <span class="input-checkmark"></span>
                                        </label>
                                    </div>
                                    <div id="filters-expander-{{ $tag->id }}" class="filters-expand-target expand-target {{ key_exists($tag->slug, $c_categories) ? 'is-expanded' : '' }}">
                                        <ul class="filters-list">
                                            @foreach($tag->tags as $key => $_tag)
                                                <li class="filters-list-item">
                                                    <label class="input-checkbox-container">
                                                        <input class="input-checkbox" type="checkbox" name="categories[{{$tag->slug}}][{{ $_tag->id }}]" value="{{ $_tag->slug }}" {{ key_exists($tag->slug, $c_categories) && is_array($c_categories[$tag->slug]) && isset($c_categories[$tag->slug][$_tag->id]) ? 'checked' : '' }} > {{ $_tag->name }}
                                                        <span class="input-checkmark"></span>
                                                    </label>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endisset
                            @endforeach
                        </ul>
                        <div class="filters-filters-title">
                            <h2>Typ arkuszy</h2>
                        </div>
                        <ul class="filters-list filters-checkbox-toggle">
                            @foreach($types as $type)
                                <li class="filters-list-item">
                                    <label class="input-checkbox-container">
                                        <input class="input-checkbox" type="checkbox" name="types[]" value="{{ $type->slug }}" {{ key_exists($type->slug, $c_types) ? 'checked' : '' }}> {{ $type->name }}
                                        <span class="input-checkmark"></span>
                                    </label>
                                </li>
                            @endforeach
                        </ul>
                        <div class="filters-filters-title">
                            <h2>Poziom arkuszy</h2>
                        </div>
                        <ul class="filters-list filters-checkbox-toggle">
                            @foreach($tiers as $tier)
                                <li class="filters-list-item">
                                    <label class="input-checkbox-container">
                                        <input class="input-checkbox" type="checkbox" name="tiers[]" value="{{ $tier->slug }}" {{ key_exists($tier->slug, $c_tiers) ? 'checked' : '' }}> {{ $tier->name }}
                                        <span class="input-checkmark"></span>
                                    </label>
                                </li>
                            @endforeach
                        </ul>
                        <div class="filters-filters-title">
                            <h2>Wersja arkuszy</h2>
                        </div>
                        <ul class="filters-list filters-checkbox-toggle">
                            @foreach($versions as $version)
                                <li class="filters-list-item">
                                    <label class="input-checkbox-container">
                                        <input class="input-checkbox" type="checkbox" name="versions[]" value="{{ $version->slug }}" {{ key_exists($version->slug, $c_versions) ? 'checked' : '' }}> {{ $version->description }}
                                        <span class="input-checkmark"></span>
                                    </label>
                                </li>
                            @endforeach
                        </ul>
                        <div class="filters-filters-title">
                            <h2>Popularność</h2>
                        </div>
                        <ul class="filters-list filters-checkbox-toggle">
                            <li class="filters-list-item">
                                <label class="input-checkbox-container">
                                    <input class="input-checkbox" type="radio" name="popularity" value="desc" {{ $c_popularity == 'desc' ? 'checked' : '' }}> Najbardziej
                                    <span class="input-checkmark"></span>
                                </label>
                            </li>
                            <li class="filters-list-item">
                                <label class="input-checkbox-container">
                                    <input class="input-checkbox" type="radio" name="popularity" value="asc" {{ $c_popularity == 'asc' ? 'checked' : '' }}> Najmniej
                                    <span class="input-checkmark"></span>
                                </label>
                            </li>
                        </ul>
                        <div class="filters-filters-title">
                            <h2>Poziom trudności</h2>
                        </div>
                        <ul class="filters-list filters-checkbox-toggle">
                            <li class="filters-list-item">
                                <label class="input-checkbox-container">
                                    <input class="input-checkbox" type="radio" name="difficulty" value="asc" {{ $c_difficulty == 'asc' ? 'checked' : '' }}> Najtrudniejsze
                                    <span class="input-checkmark"></span>
                                </label>
                            </li>
                            <li class="filters-list-item">
                                <label class="input-checkbox-container">
                                    <input class="input-checkbox" type="radio" name="difficulty" value="desc" {{ $c_difficulty == 'desc' ? 'checked' : '' }}> Najłatwiejsze
                                    <span class="input-checkmark"></span>
                                </label>
                            </li>
                        </ul>
                        <div class="filters-buttons">
                            <button id="reset-filters" class="filters-button button button-danger button-inline">Resetuj</button>
                            <button class="filters-button button button-secondary button-inline">Filtruj</button>
                        </div>
                    </div>
                </form>
                <i class="expand-toggler filters-mobile-toggler" data-target="#filters"></i>
            </div>
        </div>
        <div class="section col col-9-xl">
            <div class="section-header">
                <h1 class="title title-primary">
                    <i class="title-icon icon icon-sheet"></i> Arkusze
                </h1>
            </div>
            <div class="section-body article-list">
                <div class="article">
                    <div class="article-header">
                        <h4 class="tag-filters-title">
                            <i class="title-icon icon icon-sliders tag-filters-icon"></i>Filtry
                            <i class="tag-filters-toggler expand-toggler" data-target="#tag-filters"></i>
                        </h4>
                    </div>
                    <div class="article-body">
                        <div class="tag-filters expand-target" id="tag-filters">
                                <span class="tag-filters-item">
                                    <span class="tag-filters-group">Ekologia i fizjologia roślin</span> - Układ oddechowy <i class="tag-filters-close-toggle"></i>
                                </span>
                            <span class="tag-filters-item">
                                    <span class="tag-filters-group">Roślony</span> - nasienne <i class="tag-filters-close-toggle"></i>
                                </span>
                            <span class="tag-filters-item">
                                    <span class="tag-filters-group">Ekologia i fizjologia roślin</span> - Układ oddechowy <i class="tag-filters-close-toggle"></i>
                                </span>
                            <span class="tag-filters-item">
                                    <span class="tag-filters-group">Ekologia i fizjologia roślin</span> - Układ krwionośny <i class="tag-filters-close-toggle"></i>
                                </span>
                            <span class="tag-filters-item">
                                Wirusy, wriony, wroidy <i class="tag-filters-close-toggle"></i>
                            </span>
                        </div>
                    </div>
                    <div class="article-body">
                        <div class="pagination">
                            <span class="pagination-result">Ilość wyników: <strong>{{ $questions->total() }}</strong></span>
                            @if($questions->currentPage() > 1)
                                <div class="pagination-item">
                                    <a class="pagination-link pagination-link-first" href="{{ $questions->url(1) }}"></a>
                                </div>
                            @endif
                            @if($questions->currentPage() > 1)
                                <span class="pagination-item">
                                <a class="pagination-link pagination-link-previous" href="{{ $questions->previousPageUrl() }}"></a>
                            </span>
                            @endif
                            <span class="pagination-item">{{ $questions->currentPage() }}</span>
                            @if($questions->hasMorePages())
                                <span class="pagination-item">
                                <a class="pagination-link pagination-link-next" href="{{ $questions->nextPageUrl() }}"></a>
                            </span>
                                <div class="pagination-item">
                                    <a class="pagination-link pagination-link-last" href="{{ $questions->url($questions->lastPage()) }}"></a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                @foreach($questions as $question)
                    <article class="article article-no-gutters-top">
                        <div class="mini-statistic-container">
                            <div class="mini-statistic">
                                <span class="mini-statistic-icon icon icon-user">
                                    <span class="mini-statistic-icon-description">
                                        Rozwiązanych arkuszy
                                    </span>
                                </span>
                                <span class="mini-statistic-value">{{ $question->resolved_questions_count }}</span>
                            </div>
                            <div class="mini-statistic">
                                <span class="mini-statistic-icon icon icon-calendar-check">
                                    <span class="mini-statistic-icon-description">
                                        Średni wynik
                                    </span>
                                </span>
                                <span class="mini-statistic-value">
                                    {{ $question->percentage_result }}%
                                </span>
                            </div>
                            @if($question->sheet->version->display_name)
                                <div class="mini-statistic mini-statistic-right">
                                    <span class="mini-statistic-icon icon icon-info-circle">
                                    </span>
                                    <span class="mini-statistic-value">{{ $question->sheet->version->description }}</span>
                                </div>
                            @endif
                        </div>
                        <div class="article-header article-header-has-number">
                            <span class="article-number article-number-primary">1</span>
                            <h2 class="article-title title">
                                Matura {{ $question->sheet->month }} {{ $question->sheet->year }}, {{ $question->sheet->tier->name }} {{ $question->sheet->version->display_name ? $question->sheet->version->short_description : '' }}<br />
                                <span class="article-title-under-text">{{ $question->points }} pkt</span>
                            </h2>
                        </div>
                        <div class="article-header">
                            <div class="tags">
                                @foreach($question->tags as $tag)
                                    <a class="tag tag-primary" href="">{{ $tag->name }}</a>
                                @endforeach
                            </div>
                        </div>
                        <div class="article-body">
                            @foreach($question->contents as $content)
                                @if($content->description)
                                    <div class="article-text-intended">
                                        @markdown($content->description)
                                    </div>
                                @endif
                                @if($content->image_id)
                                    <div class="article-image-container article-image-container-half">
                                        <img class="article-image" src="{{ asset('storage/' . $content->image->path) }}" alt="">
                                    </div>
                                @endif
                            @endforeach
                            @foreach($question->substitutes as $key => $substitute)
                                <div class="question">
                                    <div class="question-header">
                                        <h2 class="question-title">{{ $question->title }}{{ $key + 1 }}</h2>
                                        <span class="question-points">
                                                {{ trans_choice('questions.points', $substitute->points, ['value' => $substitute->points]) }}
                                            </span>
                                    </div>
                                    <div class="question-body">
                                        @foreach($substitute->contents as $content)
                                            @if($content->description)
                                                @markdown($content->description)
                                            @endif
                                            @if($content->image_id)
                                                <div class="article-image-container article-image-container-half">
                                                    <img class="article-image" src="{{ asset('storage/' .$content->image->path) }}" alt="">
                                                </div>
                                            @endif
                                        @endforeach

                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="article-body">
                            <div class="article-button-container article-button-container-no-gutters">
                                <a class="button button-primary button-inline article-button" href="{{ route('users.subjects.questions.show', [$subject->slug, $question->slug]) }}">
                                    Rozwiązuj online
                                </a>
                                <a class="button button-primary article-button" href="{{ route('users.subjects.sheets.show', [$subject->slug, $question->sheet->slug]) }}">
                                    Przejdź do arkusza
                                </a>
                            </div>
                        </div>
                    </article>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function () {
            $("#reset-filters").on('click', function (e) {
                e.preventDefault();
                $('#filters-form input:checkbox').removeAttr('checked');
                $('#filters-form input:radio').removeAttr('checked');
            })
        });
    </script>
@endsection