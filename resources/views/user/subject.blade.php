@extends('layouts.app')

@section('content')
    <ul class="breadcrumb">
        <li class="breadcrumb-item selected">
            <a href="{{ route('index') }}" class="icon icon-home breadcrumb-link"></a>
        </li>
        <li class="breadcrumb-item"><a class="breadcrumb-link" href="{{ route('users.subjects.show', $subject->slug) }}"><i class="breadcrumb-icon icon {{ $subject->icon }}"></i>{{ $subject->name }}</a></li>
    </ul>
    @isset($subject->description)
        <div class="section">
            <div class="section-header">
                <h1 class="title title-primary title-gradient">{{ $subject->name }}</h1>
            </div>
            <div class="section-body">
                <div class="article-list">
                    <article class="article highlight-box">
                        <div class="article-body">
                            <div class="article-text article-text-intended">
                                @markdown($subject->description->content)
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    @endisset
    <div class="section">
        <div class="section-body card-container flex flex-col flex-row-xl minimal-cards">
            <div class="minimal-card minimal-card-primary minimal-card-media col">
                <div class="minimal-card-header">
                    <h1 class="minimal-card-title">
                        Arkusze
                    </h1>
                </div>
                <div class="minimal-card-body">
                    <span class="minimal-card-option col">
                        <span class="minimal-card-number">{{ $sheet_count }}</span>
                        <span class="minimal-card-description">Ilość arkuszy</span>
                    </span>
                    <span class="minimal-card-option col">
                            <span class="minimal-card-number">{{ $resolved_sheets }}</span>
                            <span class="minimal-card-description">Rozwiązanych arkuszy</span>
                        </span>
                    <span class="minimal-card-option col">
                        <span id="now_solving_sheet" class="minimal-card-number">
                            {{ $sheet_users }}
                        </span>
                        <span class="minimal-card-description">Aktualnie rozwiązywanych</span>
                    </span>
                </div>
                <div class="minimal-card-footer">
                    <a href="{{ route('users.subjects.sheets.index', $subject->slug) }}" class="minimal-card-button button button-inline">Rozwiązuj</a>
                </div>
            </div>
            <div class="minimal-card minimal-card-primary col">
                <div class="minimal-card-header">
                    <h1 class="minimal-card-title">
                        Zadania
                    </h1>
                </div>
                <div class="minimal-card-body">
                        <span class="minimal-card-option col">
                            <span class="minimal-card-number">{{ $question_count }}</span>
                            <span class="minimal-card-description">Ilość zadań</span>
                        </span>
                    <span class="minimal-card-option col">
                            <span class="minimal-card-number">{{ $resolved_questions }}</span>
                            <span class="minimal-card-description">Rozwiązanych zadań</span>
                        </span>
                    <span class="minimal-card-option col">
                            <span class="minimal-card-number">
                                {{ $question_users }}
                            </span>
                            <span class="minimal-card-description">Aktualnie rozwiązywanych</span>
                        </span>
                </div>
                <div class="minimal-card-footer">
                    <a href="{{ route('users.subjects.questions.index', $subject->slug) }}" class="minimal-card-button button button-inline">Rozwiązuj</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">

    </script>
@endsection