<html lang="pl">
<head>
    <title>Biolchem</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
    <div class="container">
        <h1>Tytuły</h1>
        <h1 class="title title-inline title-primary highlight-box">
            <i class="title-icon icon icon-flag"></i>Rozpocznij przygodę!
        </h1>
        <h2 class="title title-primary">
            Rozpocznij przygodę!
        </h2>
        <h3 class="title title-inline title-secondary">
            <i class="title-icon icon icon-flag"></i>Rozpocznij przygodę!
        </h3>
        <h4 class="title title-secondary">
            <i class="title-icon icon icon-flag"></i>Rozpocznij przygodę!
        </h4>
        <h5 class="title title-inline-full title-secondary">
            <i class="title-icon icon icon-flag"></i>Rozpocznij przygodę!
        </h5>
        <h6 class="title title-inline-full title-primary">
            <i class="title-icon icon icon-flag"></i>Rozpocznij przygodę!
        </h6>
        <h5 class="title title-inline-full title-secondary">
            Rozpocznij przygodę!
        </h5>
        <h1 class="title title-inline-full title-primary highlight-box">
            Rozpocznij przygodę!
        </h1>
    </div>

    <div class="container">
        <h1>Artykuły</h1>
        <div class="article article-primary highlight-box">
            <div class="article-header">
                <h1 class="title title-primary">
                    <i class="title-icon icon icon-flag"></i> Rozpocznij przygodę
                </h1>
            </div>
            <div class="article-body article-body-indented">
                <p class="article-text article-text-intended">
                    Aliquam non aliquet nisi. Proin eu odio sed eros eleifend viverra efficitur id ex.
                    Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                    Vestibulum ornare consequat viverra. Nulla facilisi. Sed eu vestibulum purus.
                    Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                    Vivamus vitae efficitur ex. In hac habitasse platea dictumst.
                    Donec nec purus pulvinar urna semper scelerisque in eget eros.
                    Sed at efficitur nisi. Donec laoreet luctus orci nec malesuada.
                </p>
            </div>
        </div>
        <div class="article article-primary highlight-box">
            <div class="article-header">
                <h1 class="title title-primary">
                    <i class="title-icon icon icon-flag"></i> Rozpocznij przygodę
                </h1>
            </div>
            <div class="article-body article-body-indented">
                <p class="article-text article-text-intended">
                    Aliquam non aliquet nisi. Proin eu odio sed eros eleifend viverra efficitur id ex.
                    Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                    Vestibulum ornare consequat viverra. Nulla facilisi. Sed eu vestibulum purus.
                    Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                    Vivamus vitae efficitur ex. In hac habitasse platea dictumst.
                    Donec nec purus pulvinar urna semper scelerisque in eget eros.
                    Sed at efficitur nisi. Donec laoreet luctus orci nec malesuada.
                </p>
            </div>
        </div>
        <div class="flex flex-col flex-row-lg card-container">
            <div class="card card-primary">
                <div class="card-header">
                    <h1 class="title title-primary title-inline">
                        <i class="title-icon icon icon-dna"></i>Chemia
                    </h1>
                </div>
                <div class="card-body">
                    Aliquam non aliquet nisi. Proin eu odio sed eros eleifend viverra efficitur id ex.
                    Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                    Vestibulum ornare consequat viverra. Nulla facilisi. Sed eu vestibulum purus.
                    Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                    Vivamus vitae efficitur ex. In hac habitasse platea dictumst.
                    Donec nec purus pulvinar urna semper scelerisque in eget eros.
                    Sed at efficitur nisi. Donec laoreet luctus orci nec malesuada.
                </div>
            </div>
            <div class="card card-primary">
                <div class="card-header">
                    <h1 class="title title-secondary title-inline">
                        <i class="title-icon icon icon-dna"></i>Biologia
                    </h1>
                </div>
                <div class="card-body">
                    Aliquam non aliquet nisi. Proin eu odio sed eros eleifend viverra efficitur id ex.
                    Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                    Vestibulum ornare consequat viverra. Nulla facilisi. Sed eu vestibulum purus.
                    Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                    Vivamus vitae efficitur ex. In hac habitasse platea dictumst.
                    Donec nec purus pulvinar urna semper scelerisque in eget eros.
                    Sed at efficitur nisi. Donec laoreet luctus orci nec malesuada.
                </div>
            </div>
        </div>
        <div class="flex flex-col flex-row-lg card-container">
            <div class="card card-primary">
                <div class="card-header card-header-inline highlight-box">
                    <h1 class="title title-primary title-inline-full title-gradient">
                        <i class="title-icon icon icon-sheet"></i>Zadania
                    </h1>
                </div>
                <div class="card-body card-body-outline">
                    Aliquam non aliquet nisi. Proin eu odio sed eros eleifend viverra efficitur id ex.
                    Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                    Vestibulum ornare consequat viverra. Nulla facilisi. Sed eu vestibulum purus.
                    Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                    Vivamus vitae efficitur ex. In hac habitasse platea dictumst.
                    Donec nec purus pulvinar urna semper scelerisque in eget eros.
                    Sed at efficitur nisi. Donec laoreet luctus orci nec malesuada.
                </div>
                <div class="card-body card-body-no-gutters">
                    <button class="card-button button button-primary">
                        Przejdź
                    </button>
                </div>
            </div>
            <div class="card card-primary">
                <div class="card-header card-header-inline highlight-box">
                    <h1 class="title title-secondary title-inline-full title-gradient">
                        <i class="title-icon icon icon-sheet"></i>Arkusze
                    </h1>
                </div>
                <div class="card-body card-body-outline">
                    Aliquam non aliquet nisi. Proin eu odio sed eros eleifend viverra efficitur id ex.
                    Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                    Vestibulum ornare consequat viverra. Nulla facilisi. Sed eu vestibulum purus.
                    Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                    Vivamus vitae efficitur ex. In hac habitasse platea dictumst.
                    Donec nec purus pulvinar urna semper scelerisque in eget eros.
                    Sed at efficitur nisi. Donec laoreet luctus orci nec malesuada.
                </div>
                <div class="card-body card-body-no-gutters">
                    <button class="card-button button button-inline button-secondary">
                        Przejdź
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <section class="section">
            <div class="section-header">
                <h1 class="title">
                    <i class="title-icon icon icon-sheet"></i>Arkusze
                </h1>
            </div>
            <div class="section-body article-list">
                <article class="article">
                    <div class="article-body">
                        <p class="article-text article-text-intended">
                            Aliquam non aliquet nisi. Proin eu odio sed eros eleifend viverra efficitur id ex.
                            Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                            Vestibulum ornare consequat viverra. Nulla facilisi. Sed eu vestibulum purus.
                            Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                            Vivamus vitae efficitur ex. In hac habitasse platea dictumst.
                            Donec nec purus pulvinar urna semper scelerisque in eget eros.
                            Sed at efficitur nisi. Donec laoreet luctus orci nec malesuada.
                        </p>
                    </div>
                </article>
                <article class="article">
                    <div class="article-body">
                        <p class="article-text article-text-intended">
                            Aliquam non aliquet nisi. Proin eu odio sed eros eleifend viverra efficitur id ex.
                            Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                            Vestibulum ornare consequat viverra. Nulla facilisi. Sed eu vestibulum purus.
                            Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                            Vivamus vitae efficitur ex. In hac habitasse platea dictumst.
                            Donec nec purus pulvinar urna semper scelerisque in eget eros.
                            Sed at efficitur nisi. Donec laoreet luctus orci nec malesuada.
                        </p>
                    </div>
                </article>
                <article class="article">
                    <div class="article-header article-header-has-number">
                        <span class="article-number">1</span>
                        <h2 class="article-title title">
                            Matura Czerwiec 2018, Poziom rozszerzony (nowy)<br />
                            <a href="">Zadanie 1. (2 pkt)</a>
                        </h2>
                    </div>
                    <div class="article-header">
                        <div class="tags">
                            <a class="tag tag-secondary" href="">Metablizm</a>
                            <a class="tag tag-secondary" href="">Biologia</a>
                            <a class="tag tag-secondary" href="">Zwierzęta</a>
                        </div>
                    </div>
                    <div class="article-body">
                        <p class="article-text article-text-intended">
                            Aliquam non aliquet nisi. Proin eu odio sed eros eleifend viverra efficitur id ex.
                            Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                            Vestibulum ornare consequat viverra. Nulla facilisi. Sed eu vestibulum purus.
                            Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                            Vivamus vitae efficitur ex. In hac habitasse platea dictumst.
                            Donec nec purus pulvinar urna semper scelerisque in eget eros.
                            Sed at efficitur nisi. Donec laoreet luctus orci nec malesuada.
                        </p>
                    </div>
                </article>
            </div>
        </section>
    </div>
    <div class="header-main">
        <div class="header-main-image"></div>
        <h1 class="header-main-logo">
            <i class="icon icon-seedling"></i> Biolchem
        </h1>
        <p class="header-main-description">Because we love science!</p>
    </div>
    <div class="nav-main">
        <ul class="nav-main-navbar">
            <li class="nav-main-item">
                <a href="#">Strona główna</a>
            </li>
            <li class="nav-main-item">
                <a href="#">Biologia <i class="nav-main-icon icon icon-dna"></i></a>
            </li>
            <li class="nav-main-item">
                <a href="#">Chemia <i class="nav-main-icon icon icon-flask"></i></a>
            </li>
            <li class="nav-main-item">
                <a href="#">Arkusze</a>
            </li>
            <li class="nav-main-item">
                <a href="#">Kontakt</a>
            </li>
            <li class="nav-main-item">
                <a href="#">O nas</a>
            </li>
        </ul>
    </div>
    <br />
    <div class="container">
        <ul class="breadcrumb">
            <li class="breadcrumb-item selected">
                <i class="icon icon-home"></i>
            </li>
            <li class="breadcrumb-item"><i class="breadcrumb-icon icon icon-sheet"></i>Arkusze</li>
            <li class="breadcrumb-item"><i class="breadcrumb-icon icon icon-dna"></i>Biologia</li>
        </ul>
        <div class="section">
            <div class="section-header">
                <h1 class="title title-primary title-gradient"><i class="title-icon icon icon-flag"></i>Rozpocznij przygodę!</h1>
            </div>
            <div class="section-body">
                <article class="article">
                    <article class="article-body">
                        <p class="article-text article-text-intended">
                            Strona została stworzona w celu zebrania w jedno miejsce materiałów przydatnych do przygotowania się do matury na kierunku <b>biologiczno chemicznym</b>. Jeśli chcesz w przyszłości studiować <b>medycynę, farmacje</b> oraz inne podobne kierunki - znajdziesz tutaj niezbędne materiały które pozwolą uzyskać Ci przyzwoity wynik.
                        </p>
                    </article>
                </article>
            </div>
        </div>
        <div class="section">
            <div class="section-header">
                <h1 class="title title-primary title-gradient"><i class="title-icon icon icon-sheet"></i>Arkusze</h1>
            </div>
            <div class="section-body">
                <div class="article-list">
                    <article class="article">
                        <div class="article-body">
                            <p class="article-text article-text-intended">
                                Posiadamy <b>arkusze maturalne</b> sprzed kilku lat, są one na bieżąco aktualizowane i sprawdzanie. Właśnie tutaj możesz sprawdzić swoją wiedzę rozwiązując <b>interaktywne testy</b>, porównaj je z innymi i zobacz na jakim wypadasz poziomie! Możesz również wybrać losowe pytanie.
                            </p>
                        </div>
                    </article>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="section-body card-container">
                <div class="card col card-primary highlight-box">
                    <div class="card-header">
                        <h2 class="title title-primary title-inline">
                            <i class="title-icon icon icon-flask"></i>Chemia
                        </h2>
                    </div>
                    <div class="card-body">
                        <div class="mini-cards">
                            <div class="mini-cards-header">
                                <ul class="calendar-bar calendar-bar-primary">
                                    <li class="calendar-bar-icon icon icon-calendar"></li>
                                    <li class="calendar-bar-item selected">2019</li>
                                    <li class="calendar-bar-item">2018</li>
                                    <li class="calendar-bar-item">2017</li>
                                    <li class="calendar-bar-item">2016</li>
                                </ul>
                            </div>
                            <div class="mini-cards-body">
                                <h3 class="mini-cards-body-title">
                                    Statystyki
                                </h3>
                                <div class="mini-cards-stats stats stats-primary">
                                    <div class="stats-row">
                                        <div class="stats-title">Rozwiązanych arkuszy</div>
                                        <div class="stats-bar">
                                            <span class="stats-progress-bar">30%</span>
                                        </div>
                                    </div>
                                    <div class="stats-row">
                                        <div class="stats-title">Średni wynik</div>
                                        <div class="stats-bar">
                                            <span class="stats-progress-bar" style="width: 67%">67%</span>
                                        </div>
                                    </div>
                                    <div class="stats-row">
                                        <div class="stats-title">Aktualnie rozwiązywanych</div>
                                        <div class="stats-bar">
                                            <span class="stats-progress-bar active" style="width: 100%">14</span>
                                        </div>
                                    </div>
                                    <div class="stats-row">
                                        <div class="stats-title">Arkuszy</div>
                                        <div class="stats-bar">
                                            <span class="stats-progress-bar" style="width: 70%">6</span>
                                        </div>
                                    </div>
                                </div>
                                <button class="mini-cards-button button button-primary button-inline">Rozwiązuj</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card col card-primary highlight-box">
                    <div class="card-header">
                        <h2 class="title title-secondary title-inline">
                            <i class="title-icon icon icon-dna"></i>Biologia
                        </h2>
                    </div>
                    <div class="card-body">
                        <div class="mini-cards">
                            <div class="mini-cards-header">
                                <ul class="calendar-bar calendar-bar-secondary">
                                    <li class="calendar-bar-icon icon icon-calendar"></li>
                                    <li class="calendar-bar-item selected">2019</li>
                                    <li class="calendar-bar-item">2018</li>
                                    <li class="calendar-bar-item">2017</li>
                                    <li class="calendar-bar-item">2016</li>
                                </ul>
                            </div>
                            <div class="mini-cards-body">
                                <h3 class="mini-cards-body-title">
                                    Statystyki
                                </h3>
                                <div class="mini-cards-stats stats stats-secondary">
                                    <div class="stats-row">
                                        <div class="stats-title">Rozwiązanych arkuszy</div>
                                        <div class="stats-bar">
                                            <span class="stats-progress-bar" style="width: 15%">155</span>
                                        </div>
                                    </div>
                                    <div class="stats-row">
                                        <div class="stats-title">Średni wynik</div>
                                        <div class="stats-bar">
                                            <span class="stats-progress-bar" style="width: 67%">67%</span>
                                        </div>
                                    </div>
                                    <div class="stats-row">
                                        <div class="stats-title">Aktualnie rozwiązywanych</div>
                                        <div class="stats-bar">
                                            <span class="stats-progress-bar active" style="width: 100%">14</span>
                                        </div>
                                    </div>
                                    <div class="stats-row">
                                        <div class="stats-title">Arkuszy</div>
                                        <div class="stats-bar">
                                            <span class="stats-progress-bar" style="width: 15%">3</span>
                                        </div>
                                    </div>
                                </div>
                                <button class="mini-cards-button button button-secondary button-inline">Rozwiązuj</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="section-header">
                <h1 class="title title-primary">
                    <i class="icon icon-flag title-icon"></i>Wybór
                </h1>
            </div>
            <div class="section-body flex flex-col flex-row-xl minimal-cards">
                <div class="minimal-card minimal-card-primary minimal-card-media col">
                    <div class="minimal-card-header">
                        <h1 class="minimal-card-title">
                            Arkusze
                        </h1>
                    </div>
                    <div class="minimal-card-body">
                        <span class="minimal-card-option col">
                            <span class="minimal-card-number">23</span>
                            <span class="minimal-card-description">Ilość arkuszy</span>
                        </span>
                        <span class="minimal-card-option col">
                            <span class="minimal-card-number">12</span>
                            <span class="minimal-card-description">Rozwiązanych arkuszy</span>
                        </span>
                        <span class="minimal-card-option col">
                            <span class="minimal-card-number">23</span>
                            <span class="minimal-card-description">Aktualnie rozwiązywanych</span>
                        </span>
                    </div>
                    <div class="minimal-card-footer">
                        <a href="#" class="minimal-card-button button button-inline">Rozwiązuj</a>
                    </div>
                </div>
                <div class="minimal-card minimal-card-secondary col">
                    <div class="minimal-card-header">
                        <h1 class="minimal-card-title">
                            Zadania
                        </h1>
                    </div>
                    <div class="minimal-card-body">
                        <span class="minimal-card-option col">
                            <span class="minimal-card-number">23</span>
                            <span class="minimal-card-description">Ilość arkuszy</span>
                        </span>
                        <span class="minimal-card-option col">
                            <span class="minimal-card-number">12</span>
                            <span class="minimal-card-description">Rozwiązanych arkuszy</span>
                        </span>
                        <span class="minimal-card-option col">
                            <span class="minimal-card-number">23</span>
                            <span class="minimal-card-description">Aktualnie rozwiązywanych</span>
                        </span>
                    </div>
                    <div class="minimal-card-footer">
                        <a href="#" class="minimal-card-button button button-inline">Rozwiązuj</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex">
            <div id="filters" class="filters-wrapper">
                        <div class="filters">
                            <div class="filters-title">
                                <h2 class="title title-inline-full title-primary title-gradient">
                                    <i class="filters-icon title-icon icon icon-calendar"></i>Filtry
                                </h2>
                            </div>
                            <div class="filters-body">
                                <div class="filters-filters">
                                    <div class="filters-filters-title">
                                        <h2>Daty</h2>
                                    </div>
                                    <ul class="filters-list">
                                        <li class="filters-list-item">
                                            <label class="input-checkbox-container">
                                                <input class="input-checkbox" type="checkbox"> 2019
                                                <span class="input-checkmark"></span>
                                            </label>
                                        </li>
                                        <li class="filters-list-item">
                                            <label class="input-checkbox-container">
                                                <input class="input-checkbox" type="checkbox"> 2019
                                                <span class="input-checkmark"></span>
                                            </label>
                                        </li>
                                        <li class="filters-list-item">
                                            <label class="input-checkbox-container">
                                                <input class="input-checkbox" type="checkbox"> 2019
                                                <span class="input-checkmark"></span>
                                            </label>
                                        </li>
                                        <li class="filters-list-item">
                                            <label class="input-checkbox-container">
                                                <input class="input-checkbox" type="checkbox"> 2019
                                                <span class="input-checkmark"></span>
                                            </label>
                                        </li>
                                    </ul>
                                    <div class="filters-filters-title">
                                        <h2>Kategorie</h2>
                                    </div>
                                    <ul class="filters-list filters-checkbox-toggle">
                                        <li class="filters-list-item">
                                            <label class="input-checkbox-container">
                                                <input class="input-checkbox" type="checkbox"> Wirusy, wriony, wroidy
                                                <span class="input-checkmark"></span>
                                            </label>
                                        </li>
                                        <li class="filters-list-item">
                                            <label class="input-checkbox-container">
                                                <input class="input-checkbox" type="checkbox"> Ekologia i zagrożenia
                                                <span class="input-checkmark"></span>
                                            </label>
                                        </li>
                                        <li class="filters-list-item">
                                            <label class="input-checkbox-container">
                                                <input class="input-checkbox" type="checkbox"> Grzyby
                                                <span class="input-checkmark"></span>
                                            </label>
                                        </li>
                                        <li class="filters-list-item">
                                            <div class="filters-toggler-container">
                                                <i class="filters-expand-toggler expand-toggler" data-target="#filters-expander-1"></i>
                                                <label class="input-checkbox-container input-checkbox-toggler" data-target="#filters-expander-1">
                                                    <input class="input-checkbox" type="checkbox"> Ekologia i fizjologia zwierząt
                                                    <span class="input-checkmark"></span>
                                                </label>
                                            </div>
                                            <div id="filters-expander-1" class="filters-expand-target expand-target">
                                                <ul class="filters-list">
                                                    <li class="filters-list-item">
                                                        <label class="input-checkbox-container">
                                                            <input class="input-checkbox" type="checkbox"> Układ pokarmowy i żywienie
                                                            <span class="input-checkmark"></span>
                                                        </label>
                                                    </li>
                                                    <li class="filters-list-item">
                                                        <label class="input-checkbox-container">
                                                            <input class="input-checkbox" type="checkbox"> Układ hormonalny
                                                            <span class="input-checkmark"></span>
                                                        </label>
                                                    </li>
                                                    <li class="filters-list-item">
                                                        <label class="input-checkbox-container">
                                                            <input class="input-checkbox" type="checkbox"> Układ nerwowy i narządy zmysłów
                                                            <span class="input-checkmark"></span>
                                                        </label>
                                                    </li>
                                                    <li class="filters-list-item">
                                                        <label class="input-checkbox-container">
                                                            <input class="input-checkbox" type="checkbox"> Układ pokarmowy i żywienie
                                                            <span class="input-checkmark"></span>
                                                        </label>
                                                    </li>
                                                    <li class="filters-list-item">
                                                        <label class="input-checkbox-container">
                                                            <input class="input-checkbox" type="checkbox"> Układ immunologiczny
                                                            <span class="input-checkmark"></span>
                                                        </label>
                                                    </li>
                                                    <li class="filters-list-item">
                                                        <label class="input-checkbox-container">
                                                            <input class="input-checkbox" type="checkbox"> Układ krążenia
                                                            <span class="input-checkmark"></span>
                                                        </label>
                                                    </li>
                                                    <li class="filters-list-item">
                                                        <label class="input-checkbox-container">
                                                            <input class="input-checkbox" type="checkbox"> Układ oddechowy
                                                            <span class="input-checkmark"></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="filters-list-item">
                                            <div class="filters-toggler-container">
                                                <label class="input-checkbox-container input-checkbox-toggler" data-target="#filters-expander-2">
                                                    <input class="input-checkbox" type="checkbox"> Rośliny
                                                    <span class="input-checkmark"></span>
                                                </label>
                                                <i class="filters-expand-toggler expand-toggler" data-target="#filters-expander-2"></i>
                                            </div>
                                            <div id="filters-expander-2" class="filters-expand-target expand-target">
                                                <ul class="filters-list">
                                                    <li class="filters-list-item">
                                                        <label class="input-checkbox-container">
                                                            <input class="input-checkbox" type="checkbox"> Mszaki
                                                            <span class="input-checkmark"></span>
                                                        </label>
                                                    </li>
                                                    <li class="filters-list-item">
                                                        <label class="input-checkbox-container">
                                                            <input class="input-checkbox" type="checkbox"> Paprotniki
                                                            <span class="input-checkmark"></span>
                                                        </label>
                                                    </li>
                                                    <li class="filters-list-item">
                                                        <label class="input-checkbox-container">
                                                            <input class="input-checkbox" type="checkbox"> Nasienne
                                                            <span class="input-checkmark"></span>
                                                        </label>
                                                    </li>
                                                    <li class="filters-list-item">
                                                        <label class="input-checkbox-container">
                                                            <input class="input-checkbox" type="checkbox"> Pozostałe
                                                            <span class="input-checkmark"></span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="filters-buttons">
                                        <button class="filters-button button button-danger button-inline">Resetuj</button>
                                        <button class="filters-button button button-secondary button-inline">Filtruj</button>
                                    </div>
                                </div>
                            </div>
                            <i class="expand-toggler filters-mobile-toggler" data-target="#filters"></i>
                        </div>
                </div>
            <div class="section col col-9-xl">
                <div class="section-header">
                    <h1 class="title title-primary">
                        <i class="title-icon icon icon-sheet"></i> Zadania
                    </h1>
                </div>
                <div class="section-body article-list">
                    <div class="article">
                        <div class="article-header">
                            <h4 class="tag-filters-title">
                                <i class="title-icon icon icon-sliders tag-filters-icon"></i>Filtry
                                <i class="tag-filters-toggler expand-toggler" data-target="#tag-filters"></i>
                            </h4>
                        </div>
                        <div class="article-body">
                            <div class="tag-filters expand-target" id="tag-filters">
                                <span class="tag-filters-item">
                                    <span class="tag-filters-group">Ekologia i fizjologia roślin</span> - Układ oddechowy <i class="tag-filters-close-toggle"></i>
                                </span>
                                <span class="tag-filters-item">
                                    <span class="tag-filters-group">Roślony</span> - nasienne <i class="tag-filters-close-toggle"></i>
                                </span>
                                <span class="tag-filters-item">
                                    <span class="tag-filters-group">Ekologia i fizjologia roślin</span> - Układ oddechowy <i class="tag-filters-close-toggle"></i>
                                </span>
                                <span class="tag-filters-item">
                                    <span class="tag-filters-group">Ekologia i fizjologia roślin</span> - Układ krwionośny <i class="tag-filters-close-toggle"></i>
                                </span>
                                <span class="tag-filters-item">
                                    Wirusy, wriony, wroidy <i class="tag-filters-close-toggle"></i>
                                </span>
                            </div>
                        </div>
                        <div class="article-body">
                            <div class="pagination">
                                <span class="pagination-result">Ilość wyników: <strong>123</strong></span>
                                <div class="pagination-item">
                                    <a class="pagination-link pagination-link-first" href=""></a>
                                </div>
                                <span class="pagination-item">
                                    <a class="pagination-link pagination-link-previous" href=""></a>
                                </span>
                                <span class="pagination-item">3</span>
                                <span class="pagination-item">
                                    <a class="pagination-link pagination-link-next" href=""></a>
                                </span>
                                <div class="pagination-item">
                                    <a class="pagination-link pagination-link-last" href=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <article class="article article-no-gutters-top">
                        <div class="mini-statistic-container">
                            <div class="mini-statistic">
                                <span class="mini-statistic-icon icon icon-user">
                                    <span class="mini-statistic-icon-description">
                                        Rozwiązanych arkuszy
                                    </span>
                                </span>
                                <span class="mini-statistic-value">12</span>
                            </div>
                            <div class="mini-statistic">
                                <span class="mini-statistic-icon icon icon-calendar-check">
                                    <span class="mini-statistic-icon-description">
                                        Średni wynik
                                    </span>
                                </span>
                                <span class="mini-statistic-value">76%</span>
                            </div>
                            <div class="mini-statistic mini-statistic-right">
                                <span class="mini-statistic-icon icon icon-info-circle">
                                </span>
                                <span class="mini-statistic-value">Nowa matura</span>
                            </div>
                        </div>
                        <div class="article-header article-header-has-number">
                            <span class="article-number article-number-primary">1</span>
                            <h2 class="article-title title">
                                Matura Czerwiec 2018, Poziom rozszerzony (nowy)<br />
                                <a class="primary" href="">Zadanie 1. (2 pkt)</a>
                            </h2>
                        </div>
                        <div class="article-header">
                            <div class="tags">
                                <a class="tag tag-primary" href="">Metablizm</a>
                                <a class="tag tag-primary" href="">Biologia</a>
                                <a class="tag tag-primary" href="">Zwierzęta</a>
                            </div>
                        </div>
                        <div class="article-body">
                            <p class="article-text article-text-intended">
                                Aliquam non aliquet nisi. Proin eu odio sed eros eleifend viverra efficitur id ex.
                                Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                                Vestibulum ornare consequat viverra. Nulla facilisi. Sed eu vestibulum purus.
                                Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                                Vivamus vitae efficitur ex. In hac habitasse platea dictumst.
                                Donec nec purus pulvinar urna semper scelerisque in eget eros.
                                Sed at efficitur nisi. Donec laoreet luctus orci nec malesuada.
                            </p>
                            <div class="article-button-container">
                                <a class="button button-primary button-inline article-button" href="">
                                    Rozwiązuj online
                                </a>
                                <a class="button button-primary article-button" href="">
                                    Przejdź do arkusza
                                </a>
                            </div>
                        </div>
                    </article>
                    <article class="article article-no-gutters-top">
                        <div class="mini-statistic-container">
                            <div class="mini-statistic">
                                <span class="mini-statistic-icon icon icon-user">
                                    <span class="mini-statistic-icon-description">
                                        Rozwiązanych arkuszy
                                    </span>
                                </span>
                                <span class="mini-statistic-value">22</span>
                            </div>
                            <div class="mini-statistic">
                                <span class="mini-statistic-icon icon icon-calendar-check">
                                    <span class="mini-statistic-icon-description">
                                        Średni wynik
                                    </span>
                                </span>
                                <span class="mini-statistic-value">36%</span>
                            </div>
                            <div class="mini-statistic mini-statistic-right">
                                <span class="mini-statistic-icon icon icon-info-circle">
                                </span>
                                <span class="mini-statistic-value">Nowa matura</span>
                            </div>
                        </div>
                        <div class="article-header article-header-has-number">
                            <span class="article-number article-number-primary">1</span>
                            <h2 class="article-title title">
                                Matura Czerwiec 2018, Poziom rozszerzony (nowy)<br />
                                <a class="primary" href="">Zadanie 1. (2 pkt)</a>
                            </h2>
                        </div>
                        <div class="article-header">
                            <div class="tags">
                                <a class="tag tag-primary" href="">Metablizm</a>
                                <a class="tag tag-primary" href="">Biologia</a>
                                <a class="tag tag-primary" href="">Zwierzęta</a>
                            </div>
                        </div>
                        <div class="article-body">
                            <p class="article-text article-text-intended">
                                Aliquam non aliquet nisi. Proin eu odio sed eros eleifend viverra efficitur id ex.
                                Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                                Vestibulum ornare consequat viverra. Nulla facilisi. Sed eu vestibulum purus.
                                Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                                Vivamus vitae efficitur ex. In hac habitasse platea dictumst.
                                Donec nec purus pulvinar urna semper scelerisque in eget eros.
                                Sed at efficitur nisi. Donec laoreet luctus orci nec malesuada.
                            </p>
                            <div class="article-button-container">
                                <a class="button button-primary button-inline article-button" href="">
                                    Rozwiązuj online
                                </a>
                                <a class="button button-primary article-button" href="">
                                    Przejdź do arkusza
                                </a>
                            </div>
                        </div>
                    </article>
                    <article class="article article-no-gutters-top">
                        <div class="mini-statistic-container">
                            <div class="mini-statistic">
                                <span class="mini-statistic-icon icon icon-user">
                                    <span class="mini-statistic-icon-description">
                                        Rozwiązanych arkuszy
                                    </span>
                                </span>
                                <span class="mini-statistic-value">13343</span>
                            </div>
                            <div class="mini-statistic">
                                <span class="mini-statistic-icon icon icon-calendar-check">
                                    <span class="mini-statistic-icon-description">
                                        Średni wynik
                                    </span>
                                </span>
                                <span class="mini-statistic-value">98%</span>
                            </div>
                            <div class="mini-statistic">
                                <span class="mini-statistic-icon icon icon-angry">
                                    <span class="mini-statistic-icon-description">
                                        Poziom trudności
                                    </span>
                                </span>
                                <span class="mini-statistic-value">Trudny</span>
                            </div>
                            <div class="mini-statistic mini-statistic-right">
                                <span class="mini-statistic-icon icon icon-info-circle">
                                </span>
                                <span class="mini-statistic-value">Nowa matura</span>
                            </div>
                        </div>
                        <div class="article-header article-header-has-number">
                            <span class="article-number article-number-primary">1</span>
                            <h2 class="article-title title">
                                Matura Czerwiec 2018, Poziom rozszerzony (nowy)<br />
                                <a class="primary" href="">Zadanie 1. (2 pkt)</a>
                            </h2>
                        </div>
                        <div class="article-header">
                            <div class="tags">
                                <a class="tag tag-primary" href="">Metablizm</a>
                                <a class="tag tag-primary" href="">Biologia</a>
                                <a class="tag tag-primary" href="">Zwierzęta</a>
                            </div>
                        </div>
                        <div class="article-body">
                            <p class="article-text article-text-intended">
                                Aliquam non aliquet nisi. Proin eu odio sed eros eleifend viverra efficitur id ex.
                                Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                                Vestibulum ornare consequat viverra. Nulla facilisi. Sed eu vestibulum purus.
                                Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                                Vivamus vitae efficitur ex. In hac habitasse platea dictumst.
                                Donec nec purus pulvinar urna semper scelerisque in eget eros.
                                Sed at efficitur nisi. Donec laoreet luctus orci nec malesuada.
                            </p>
                            <div class="article-image-container">
                                <img class="article-image" src="{{ asset('images/question-image.png') }}" alt="">
                            </div>
                            <div class="article-button-container">
                                <a class="button button-primary button-inline article-button" href="">
                                    Rozwiązuj online
                                </a>
                                <a class="button button-primary article-button" href="">
                                    Przejdź do arkusza
                                </a>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>