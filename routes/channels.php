<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

use App\Broadcasting\QuestionSolvingChannel;
use App\Broadcasting\SheetSolvingChannel;
use App\Broadcasting\SheetYearChannel;
use App\Broadcasting\SubjectChannel;
use App\Broadcasting\SubjectSheetChannel;

Broadcast::channel('sheet.year.{year}', SheetYearChannel::class, [
    'guards' => ['web', 'guest']
]);

Broadcast::channel('sheet.{sheet}.solving.{uuid}', SheetSolvingChannel::class, [
    'guards' => ['guest']
]);

Broadcast::channel('question.{question}.solving.{uuid}', QuestionSolvingChannel::class, [
    'guards' => ['guest']
]);

Broadcast::channel('subject.{subject}', SubjectChannel::class, [
    'guards' => ['guest']
]);

Broadcast::channel('subject.{subject}.sheet', SubjectSheetChannel::class, [
    'guards' => ['guest']
]);
