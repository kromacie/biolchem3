<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', function () {
    return view('welcome');
});

Route::get('/o_nas', 'AboutUsController@index')->name('about_us');
Route::get('/pytania', 'QuestionsController@index');
Route::get('/pytania/{id}', 'QuestionsController@show');

Route::get('/', 'IndexController@index')->name('index');

Route::group(['prefix' => '/admin', 'namespace' => 'Admin', 'middleware' => ['auth.basic']], function () {
    Route::get('/', 'IndexController@index');
    Route::get('/hello', 'IndexController@hello');

    Route::resource('tags', 'TagController');
    Route::resource('types', 'SheetTypeController');
    Route::resource('tiers', 'SheetTierController');
    Route::resource('versions', 'SheetVersionController');
    Route::resource('subjects', 'SubjectController');
    Route::resource('subjects.descriptions', 'Subject\DescriptionController');

    Route::group(['prefix' => 'inputs', 'as' => 'inputs.'], function (){
        Route::resource('types', 'InputTypeController');
    });

    Route::group(['prefix' => 'inputs/groups', 'as' => 'inputs.groups.'], function (){
        Route::resource('types', 'InputGroupTypeController');
    });

    Route::resource('sheets', 'SheetController');
    Route::resource('sheets.questions', 'QuestionController');
    Route::resource('sheets.questions.tags', 'Question\\TagController');
    Route::resource('sheets.questions.contents', 'Question\\ContentController');
    Route::resource('sheets.questions.substitutes', 'Question\\SubstituteController');
    Route::resource('sheets.questions.substitutes.contents', 'Question\\Substitute\\ContentController');
    Route::resource('sheets.questions.substitutes.inputGroups', 'Question\\Substitute\\InputGroupController');
    Route::resource('sheets.questions.substitutes.inputs', 'Question\\Substitute\\InputController');
    Route::resource('sheets.questions.substitutes.rewards', 'Question\\Substitute\\RewardController');
    Route::resource('sheets.questions.substitutes.inputs.corrects', 'Question\\Substitute\\InputCorrectController');
    Route::resource('sheets.questions.substitutes.inputs.rewards', 'Question\\Substitute\\InputRewardController');

    Route::group(['prefix' => 'sheets/{sheet}/questions/{question}/editor', 'as' => 'sheets.questions.editor.'], function () {
        Route::get('/', 'QuestionEditorController@index')->name('index');
    });
});

Route::group([
    'prefix' => '{subject}',
    'as' => 'users.subjects.',
    'middleware' => [
        'subject.exists'
    ]
], function (){
    Route::get('/', 'SubjectController@show')->name('show');

    Route::group([
        'prefix' => '/arkusze',
        'as' => 'sheets.',
        'guard' => 'guest'
    ], function (){
        Route::get('/', 'SheetsController@index')->name('index');
        Route::get('/{release}', 'SheetsController@show')->name('show');
        Route::post('/{release}', 'SheetsController@check')->name('check');
    });

    Route::group([
        'prefix' => '/pytania',
        'as' => 'questions.',
        'guard' => 'guest'
    ], function (){
        Route::get('/', 'QuestionsController@index')->name('index');
        Route::get('/{slug}', 'QuestionsController@show')->name('show');
        Route::post('/{slug}', 'QuestionsController@check')->name('check');
    });
});