require('dotenv').config();

const env = process.env;

require('laravel-echo-server').run({
    authHost: env.APP_URL,
    devMode: env.APP_DEBUG,
    database: "redis",
    port: env.APP_ECHO_PORT,
    databaseConfig: {
        redis: {
            host: env.REDIS_HOST,
            port: env.REDIS_PORT,
            password: env.REDIS_PASSWORD
        }
    },
    clients: [
        {
            "appId": "Biolchem",
            "key": env.APP_ECHO_KEY
        }
    ],
    apiOriginAllow: {
        "allowCors": true,
        "allowOrigin": env.APP_URL,
        "allowMethods": "GET, POST",
        "allowHeaders": "Origin, Content-Type, X-Auth-Token, X-Requested-With, Accept, Authorization, X-CSRF-TOKEN, X-Socket-Id"
    }
});
